//*****************************************************************************
//
// Имя файла    : 'core.h'
// Описание     : Интерфейс взаимодействия с ядром
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************
#pragma once

#include <QObject>
#include <QApplication>
#include "ui/gui.h"
#include "io/driver.h"
//#include "types.h"


// ============================================================================
//                                  CCore
// ============================================================================

class CCore : public QObject
{
    Q_OBJECT

    public:
        CCore ();
        ~CCore ();

        void    Init (int& argc, char** argv);
        int     Run (void);

    private:
        QApplication*   m_app;          // Объект Qt приложения
};
