//*****************************************************************************
//
// Имя файла    : 'settings.h'
// Описание     : Управление настройками приложения
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************
#pragma once

#include <QObject>
#include <QHash>
#include <QSettings>


// ============================================================================
//                                  Константы
// ============================================================================

    // Имана групп настроек
    #define SETTINGS_GROUP_BASE_NAME                    "base"
    #define SETTINGS_GROUP_PLUGINS_NAME                 "plugins"
    #define SETTINGS_GROUP_KEYBIND_NAME                 "keybindings"

    // Префикс параметра сочетания кнопок
    #define SETTINGS_SHORTCUT_PARAM_PREFIX              "shortcut."


// ============================================================================
//                                  CSettings
// ============================================================================

class CSettings : public QObject
{
    Q_OBJECT

    public:
        CSettings (QObject* parent);
        ~CSettings ();

        // Чтение группы настроек
        int     Get (const QString* group, QHash<QString, QVariant>* pStorage);
        // Сохранение группы настроек
        int     Save (const QString* group, QHash<QString, QVariant>* pStorage);

        // Формирование имени группы параметров сесии
        static QString  GetSessGroup (uint sessId)
        {
            return QString("sess_%1").arg(sessId);
        }

        // Определение id сессии по имени группы
        static int  GetSessId (const QString* group)
        {
            if ( !group->startsWith("sess_") ) return -1;
            QString strId = group->mid( sizeof("sess_") - 1 );

            return strId.toInt();
        }

    signals:
        // Информиование об изменении группы настроек
        void    SigSettingsChanged (const QString* group, QHash<QString, QVariant>* params);

    private:
        QSettings*  m_pSettings;
};
