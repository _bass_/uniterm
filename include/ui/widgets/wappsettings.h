//*****************************************************************************
//
// Имя файла    : 'wappsettings.h'
// Описание     : Окно настроек приложения
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************
#pragma once

#include <QDialog>
#include <QStandardItemModel>
#include <QHash>
#include <QVariant>


// ============================================================================
//                                  Типы
// ============================================================================

    // Типы используемых данных для QModelIndex (элементы дерева меню)
    typedef enum
    {
        STYPE_GROUP_OBJ             = (Qt::UserRole + 1),   // Указатель на объект группы настроек (ISettingsGroup)
        STYPE_SETTINGS_GROUP_NAME   = (Qt::UserRole + 2)    // Имя группы настроек
    } TSettingsDataType;

    // Класс группы настроек
    class ISettingsGroup;

    typedef ISettingsGroup* (*TGroupCreator) (void);


// ============================================================================
//                              WAppSettings
// ============================================================================

namespace Ui
{
    class WAppSettings;
}

class WAppSettings : public QDialog
{
    Q_OBJECT

    public:
        explicit WAppSettings (QWidget* parent = 0);
        ~WAppSettings ();

    signals:
        // Сигнал закрытия окна
        void    sigClosed (bool result);

    private:
        Ui::WAppSettings*           ui;                 // Интерфейс окна настроек
        QStandardItemModel          m_groupListModel;   // Модель списка групп настроек
        QHash<QString, QVariant>    m_appSettings;      // Временное хранилище настроек приложения

        // Временное сохранение параметров (из активного виджета)
        void    saveTmpSettings (const QModelIndex& index);
        // Сохранение настроек
        void    saveSettings (void);
        // Загрузка настроек в локальное хранилище
        void    loadSettings (void);

    private slots:
        void    accept (void);
        void    reject (void);
        void    onItemSelect (const QModelIndex& current, const QModelIndex& previous);
};
