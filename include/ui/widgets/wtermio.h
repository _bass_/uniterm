//*****************************************************************************
//
// Имя файла    : 'wtermio.h'
// Описание     : Виджет ввода-вывода терминала
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************
#pragma once

#include "session.h"
#include <QWidget>
#include <QPlainTextEdit>
#include <QString>
#include <QVector>


// ============================================================================
//                              WTermIO
// ============================================================================

class WTermIO : public QPlainTextEdit
{
    Q_OBJECT

    public:
        WTermIO (QWidget* parent);

        enum ColorType
        {
            COLOR_BG,
            COLOR_TEXT_IN,
            COLOR_TEXT_OUT
        };

        void    SetColor (ColorType type, QColor color);
        void    SetLineEnding (const QString& eol) { m_eol = eol; }
        void    SetCharset (const QString& charset) { m_charset = charset; }
        void    ShowPacket (TIoPack* packet);

    signals:
        void    SignalSendRequest (char* data, uint len);

    private:
        QString             m_prompt;
        QString             m_eol;
        QString             m_charset;
        QColor              m_colorIn;
        QColor              m_colorOut;
        QVector<QString>    m_history;      // История ввода (возраст ввода увеличивается от 0 до length)
        int                 m_histIndex;

        void    keyPressEvent (QKeyEvent* ev);
        void    mouseReleaseEvent (QMouseEvent *ev);
        void    contextMenuEvent (QContextMenuEvent* ev);
        void    dragEnterEvent (QDragEnterEvent* ev);

        void    paste (void);
        void    doSend (QString& str);

        bool    isCursorValid (bool move = false);
        void    historyCtrl(Qt::Key key, QString* pStr);
};
