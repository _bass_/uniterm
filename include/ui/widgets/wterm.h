//*****************************************************************************
//
// Имя файла    : 'wterm.h'
// Описание     : Виджет терминала
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************
#pragma once

#include "ui/widgets/wtermio.h"


// ============================================================================
//                                  WTerm
// ============================================================================

class WTerm : public QWidget
{
    Q_OBJECT

    public:
        WTerm (CIoSess* session, QWidget* parent);

        CIoSess*    GetSession (void) { return m_pSession; }

    private:
        CIoSess*    m_pSession;     // Связанная с виджетом сессия
        WTermIO     m_wOut;         // Виджет вывода данных

        void    showEvent (QShowEvent *ev);
        void    initUi (void);

    private slots:
        void    onSendRequest (char* data, uint size);
        void    onPacketRecv (TIoPack* packet);
        void    onPacketSended (TIoPack* packet);
        void    onSettingsChanged (const QString* group, QHash<QString, QVariant>* params);
};
