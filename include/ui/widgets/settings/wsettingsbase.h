//*****************************************************************************
//
// Имя файла    : 'wsettingsbase.h'
// Описание     : Группа настроек Base
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************
#pragma once

#include "ui/widgets/isettingsgroup.h"


// ============================================================================
//                                      Типы
// ============================================================================

    namespace Ui
    {
        class WSettingsBase;
    }


// ============================================================================
//                              WSettingsBase
// ============================================================================

class WSettingsBase : public ISettingsGroup
{
    public:
        WSettingsBase ();

        QString GetName (void);
        QString GetTitle (void);
        void    BuildSubTree (QStandardItem* parentMenu);
        void    SettingsDraw (const QModelIndex* activeIndex, QWidget* pWidget, QHash<QString, QVariant>* pSettings);
        void    SettingsParse (const QModelIndex* activeIndex, QWidget* pWidget, QHash<QString, QVariant>* pSettings);

    private:
        Ui::WSettingsBase*   ui;

    private slots:
        // Обработчик нажатия кнопки выбора цвета
        void    onBtnSelectColor (bool checked);
};
