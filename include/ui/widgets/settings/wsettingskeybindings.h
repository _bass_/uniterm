//*****************************************************************************
//
// Имя файла    : 'wsettingskeybindings.h'
// Описание     : Группа настроек Key bindings
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************
#pragma once

#include "ui/widgets/isettingsgroup.h"
#include <QTableWidget>


// ============================================================================
//                                      Типы
// ============================================================================

    namespace Ui
    {
        class WSettingsKeyBindings;
    }


// ============================================================================
//                              WSettingsKeyBindings
// ============================================================================

class WSettingsKeyBindings : public ISettingsGroup
{
    public:
        WSettingsKeyBindings ();

        QString GetName (void);
        QString GetTitle (void);
        void    BuildSubTree (QStandardItem* parentMenu);
        void    SettingsDraw (const QModelIndex* activeIndex, QWidget* pWidget, QHash<QString, QVariant>* pSettings);
        void    SettingsParse (const QModelIndex* activeIndex, QWidget* pWidget, QHash<QString, QVariant>* pSettings);

    private:
        Ui::WSettingsKeyBindings*   ui;

        void    fillShortcutTable (QTableWidget* pTable, QHash<QString, QVariant>* pSettings);

    private slots:
        void    onItemSelect (QTableWidgetItem* item);
        void    onShortcutSave (const QString &text);
};
