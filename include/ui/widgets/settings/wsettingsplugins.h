//*****************************************************************************
//
// Имя файла    : 'wsettingsplugins.h'
// Описание     : Группа настроек Plugins
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************
#pragma once

#include "ui/widgets/isettingsgroup.h"


// ============================================================================
//                              WSettingsPlugins
// ============================================================================

class WSettingsPlugins : public ISettingsGroup
{
    public:
        WSettingsPlugins ();

        QString GetName (void);
        QString GetTitle (void);
        void    BuildSubTree (QStandardItem* parentMenu);
        void    SettingsDraw (const QModelIndex* activeIndex, QWidget* pWidget, QHash<QString, QVariant>* pSettings);
        void    SettingsParse (const QModelIndex* activeIndex, QWidget* pWidget, QHash<QString, QVariant>* pSettings);

    private:
        void    drawPluginsList (QWidget* pWidget, QHash<QString, QVariant>* pSettings);
        void    drawPluginSettings (QString& plgName, QWidget* pWidget, QHash<QString, QVariant>* pSettings);
};
