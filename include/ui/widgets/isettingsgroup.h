//*****************************************************************************
//
// Имя файла    : 'isettingsgroup.h'
// Описание     : Интерфейс группы настроек
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************
#pragma once

#include "ui/widgets/wappsettings.h"
#include "factory.h"
#include <QObject>
#include <QWidget>
#include <QStandardItem>
#include <QModelIndex>
#include <QHash>


// ============================================================================
//                                  Константы
// ============================================================================

    // Регистрация группы настроек
    #define SETTINGS_GROUP_REGISTER(className)      static ISettingsGroup* creator##className () \
                                                    { \
                                                        return new className (); \
                                                    } \
                                                    const TGroupCreator g_creator##className __attribute__ ((section("settingsGroup"), used)) = creator##className
    #define SETTINGS_GROUP_LIST_START_ADDR          (&__start_settingsGroup)
    #define SETTINGS_GROUP_LIST_END_ADDR            (&__stop_settingsGroup)
    // Объявление переменных начала и конца секции
    extern void* __start_settingsGroup;
    extern void* __stop_settingsGroup;


// ============================================================================
//                              ISettingsGroup
// ============================================================================

class ISettingsGroup : public QObject
{
    Q_OBJECT

    public:
        ISettingsGroup () : QObject( CFactory::GetCore() ) {}

        virtual QString GetName (void) = 0;
        virtual QString GetTitle (void) = 0;
        virtual void    BuildSubTree (QStandardItem* parentMenu) = 0;
        virtual void    SettingsDraw (const QModelIndex* activeIndex, QWidget* pWidget, QHash<QString, QVariant>* pSettings) = 0;
        virtual void    SettingsParse (const QModelIndex* activeIndex, QWidget* pWidget, QHash<QString, QVariant>* pSettings) = 0;
};

Q_DECLARE_METATYPE(ISettingsGroup*)
