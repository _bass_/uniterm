//*****************************************************************************
//
// Имя файла    : 'wpanel.h'
// Описание     : Виджет панели
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************
#pragma once

#include <QWidget>


// ============================================================================
//                                  WPanel
// ============================================================================

class WPanel : public QWidget
{
    Q_OBJECT

    public:
        WPanel (QWidget* parent = 0);

    private slots:
        // Обработчик нажатия на кнопку IO settings
        void    onIoSettings (bool checked);
        // Обработчик нажатия на кнопку Open / close
        void    onOpenClose (bool checked);
        // Обработчик изменения настроек
        void    onSettingsChanged (const QString* group, QHash<QString, QVariant>* params);

    private:
        void    updateShortcuts (void);
};
