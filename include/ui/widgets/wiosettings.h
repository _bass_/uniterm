//*****************************************************************************
//
// Имя файла    : 'wiosettings.h'
// Описание     : Окно настроек IO
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************
#pragma once

#include <QDialog>
#include <QStringListModel>
#include <QHash>
#include <QVariant>


// ============================================================================
//                              WIoSettings
// ============================================================================

namespace Ui {
class WIoSettings;
}

class WIoSettings : public QDialog
{
    Q_OBJECT

    public:
        explicit WIoSettings (QWidget *parent = 0);
        ~WIoSettings();

    signals:
        // Сигнал закрытия окна
        void    sigClosed (bool result);

    private:
        Ui::WIoSettings*    ui;
        QHash<QString, QVariant> m_tmpSettings;
        QString             m_prevDrvName;

        // Временное сохранение параметров драйвера (из ui->m_content)
        void    saveTmpSettings (QString& drvName);
        // Сохранение настроек сессии
        void    saveSettings (void);

    private slots:
        void    accept (void);
        void    reject (void);
        void    onDrvSelect (const QString& drvName);
};
