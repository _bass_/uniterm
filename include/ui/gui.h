//*****************************************************************************
//
// Имя файла    : 'gui.h'
// Описание     : Графический интерфейс пользователя
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************
#pragma once

#include <QObject>
#include <QMainWindow>
#include <QWidget>
#include <QTabWidget>
#include <QMenu>
#include <QVector>
#include <QEvent>
#include <QShortcut>
#include <QTranslator>
#include "session.h"
#include "ui/widgets/wterm.h"
#include "ui/widgets/wiosettings.h"
#include "ui/widgets/wappsettings.h"
#include "ui/widgets/wabout.h"


// ============================================================================
//                              Константы
// ============================================================================

    // Имя свойства панели, содержащего ID привязанной сесии
    #define GUI_PANEL_PROPETRY_SESSID           "sessId"


// ============================================================================
//                                  Типы
// ============================================================================

    // Типы событий
    typedef enum
    {
        EV_BUILD_PANEL = QEvent::User,      // Запрос создания виджета панели (параметр QWidget**)
        EV_BUILD_MENU,                      // Запрос создания основного меню (параметр QMenu**)
        EV_SHOW_IO_SETTINGS,                // Запрос отображения окна настроек IO
        EV_SHOW_IO_DRV_OPEN_ERROR           // Информирование об ошибке открытия драйвера
    } TEvType;


// ============================================================================
//                                  CEventGui
// ============================================================================

class CGuiEvent : public QEvent
{
    public:
        CGuiEvent (TEvType type) : QEvent( (QEvent::Type)type ) {}

        // Чтение дополнительных параметров события
        void*   GetParam (void) { return m_params; }
        // Установка дополнительных параметров события
        void    SetParam (void* ptr) { m_params = ptr; }

    private:
        void*   m_params;
};


// ============================================================================
//                                  CGui
// ============================================================================

class CGui : public QObject
{
    Q_OBJECT

    public:
        CGui ();
        ~CGui ();

        // Отображение GUI
        void        Show (void);
        // Запрос активной сессии
        CIoSess*    GetActiveSession (void);
        // Запрос списка панелей инструментов
        int         GetListPanels (QList<QWidget*>* pList);

    private slots:
        void    onTabChanged (int index);
        void    onTabClose (int index);
        void    onIoSettingsClose (bool result);
        void    onAppSettingsOpen (bool checked);
        void    onAppSettingsClosed (bool result);
        void    onMenuHelp (bool checked);
        void    onAboutOpen (bool checked);
        void    onAboutClosed (bool result);
        void    onBtnAddTab (bool checked);
        void    onActTabClose (void);
        void    onActTabChange (void);
        void    onActTabChangeRev (void);
        void    onSettingsChanged (const QString* group, QHash<QString, QVariant>* params);
        void    onWinClose (void);

    private:
        QMainWindow         m_window;
        QTabWidget          m_wrap;             // Обертка для основного содержимого окна (центральный виджет)
        QVector<WTerm*>     m_tabList;          // Список виджетов (вкладок)
        QMenu*              m_menu;             // Основное меню
        WIoSettings*        m_winIoSettings;    // Окно настроек IO
        WAppSettings*       m_winAppSettings;   // Окно настроек приложения
        WAbout*             m_winAbout;         // Окно "О приложении"
        QShortcut           m_scTabClose;       // Hotkey закрытия текущей вкладки
        QShortcut           m_scTabChange;      // Hotkey переключения вкладки
        QShortcut           m_scTabChangeRev;   // Hotkey переключения вкладки (в обратном направлении)
        QTranslator         m_translator;

        // Инициализация элементов окна
        void    initStatusBar (void);
        void    initTabs (void);
        void    initShortcuts (void);

        // Создание новой вкладки и сессии
        int     addTab (void);
        // Создание объекта меню
        QMenu*  buildMenu (void);

        // Обработчик событий
        void    customEvent (QEvent* event);
};
