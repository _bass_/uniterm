//*****************************************************************************
//
// Имя файла    : 'plugin_mgr.h'
// Описание     : Менеджер плагинов
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************
#pragma once

#include "plugins/plugin.h"
#include <QObject>
#include <QEvent>
#include <QPluginLoader>


// ============================================================================
//                                  Типы
// ============================================================================

    // Служебный дескриптор плагина
    typedef struct
    {
        QPluginLoader*  pLoader;
        bool            isEnabled;
        CPlugin*        plugin;
        QString         name;
        QString         title;
        QString         version;
        QString         desc;
    } TPlgDesc;

    // Информация о плагине
    typedef struct
    {
        bool        isEnabled;
        QString     name;
        QString     title;
        QString     version;
        QString     desc;
    } TPlgInfo;


// ============================================================================
//                                  CPluginMgr
// ============================================================================

class CPluginMgr : public QObject
{
    Q_OBJECT

    public:
        CPluginMgr (QObject* parent);
        ~CPluginMgr ();

        // Инициализация менеджера и плагинов
        int     Init (void);
        // Оработчик событий для плагинов
        void    Dispatch (TPlgEv code, void* data);

        // Формирование списка имен плагинов
        QStringList GetPlugins (bool onlyActivated = true);
        // Поиск плагина (активированного)
        CPlugin*    GetPlugin (const QString& name);
        // Чтение информации о плагине
        int         GetPluginInfo (const QString& name, TPlgInfo* pInfo);

    private:
        QList<TPlgDesc*>    m_listDesc;         // Список дескрипторов плагинов

        // Формирование списка дескрипторов плагинов
        void    buildList (void);
        // Поиск дескриптора плагина
        TPlgDesc* getDesc (const QString& name);
        // Инициализация плагина
        int     initPlugin (QString& name, QHash<QString, QVariant>* params = NULL);

        // Обработчик событий (запросы от плагинов)
        void    customEvent (QEvent* event);
        void    onReqSession (CPlgEvent* pEv);
        void    onReqListSessions (CPlgEvent* pEv);
        void    onReqListPanels (CPlgEvent* pEv);

    private slots:
        void    onSettingsChanged (const QString* group, QHash<QString, QVariant>* params);
};
