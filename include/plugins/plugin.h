//*****************************************************************************
//
// Имя файла    : 'plugin.h'
// Описание     : Интерфейс плагинов приложения
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************
#pragma once

#include <QObject>
#include <QWidget>
#include <QHash>
#include <QVariant>
#include <QEvent>
#include <QApplication>
#include "session.h"
#include "types.h"


// ============================================================================
//                                  Типы
// ============================================================================

    // Типы событий
    typedef enum
    {
        PLG_EV_GUI_BUILD_MENU = QEvent::User + 100,         // Данные: указатель на меню
        PLG_EV_GUI_BUILD_PANEL,                             // Данные: указатель на виджет панели
        PLG_EV_SESS_CREATE,                                 // Данные: указатель на сессию
        PLG_EV_SESS_DELETE,                                 // Данные: указатель на сессию
        PLG_EV_SESS_ACTIVATE,                               // Данные: указатель на сессию
        PLG_EV_SESS_PACKET_BEFORE_SEND,                     // Данные: указатель на пакет
        PLG_EV_SESS_PACKET_AFTER_SEND,                      // Данные: указатель на пакет
        PLG_EV_SESS_PACKET_BEFORE_RECV,                     // Данные: указатель на пакет
        PLG_EV_SESS_PACKET_AFTER_RECV                       // Данные: указатель на пакет
    } TPlgEv;

    // Коды запросов (плагин -> менеджер плагинов)
    typedef enum
    {
        PLG_REQ_SESSION = QEvent::User + 1000,              // Данные: указатель на ID сесии
        PLG_REQ_LIST_SESSIONS,                              // Данные: указатель на список сесиий
        PLG_REQ_LIST_PANELS                                 // Данные: указатель на список панелей инструментов
    } TPlgReq;

    // Стурктура запроса
    typedef struct
    {
        int     result;
        void*   arg;
    } TPlgReqParam;

    // Запрос поиска объекта сессии
    typedef union
    {
        uint        sessId;
        CIoSess*    pSession;
    } TPlgReqArgSess;


// ============================================================================
//                                  CPlgEvent
// ============================================================================

class CPlgEvent : public QEvent
{
    public:
        CPlgEvent (TPlgEv type) : QEvent( (QEvent::Type)type ) {}
        CPlgEvent (TPlgReq type) : QEvent( (QEvent::Type)type ) {}

        // Чтение дополнительных параметров события
        void*   GetParam (void) { return m_params; }
        // Установка дополнительных параметров события
        void    SetParam (void* ptr) { m_params = ptr; }

    private:
        void*   m_params;
};


// ============================================================================
//                                  CPlugin
// ============================================================================

class CPlugin : public QObject
{
    Q_OBJECT

    public:
        CPlugin (QObject* parent) : QObject(parent) {}
        virtual ~CPlugin () {}

        // Запрос имени плагина (файла)
        virtual QString GetName (void) = 0;
        // Запрос версии плагина
        virtual QString GetVersion (void) = 0;
        // Запрос названия плагина (человеческое)
        virtual QString GetTitle (void) = 0;
        // Запрос описания плагина
        virtual QString GetDesc (void) = 0;

        // Конфигурирование
        virtual int     Configure (QHash<QString, QVariant>* pSettings) = 0;
        // Отрисовка параметров плагина
        virtual void    DrawSettings (QWidget* widget, QHash<QString, QVariant>* pSettings) = 0;
        // Сохранение выбранных параметров
        virtual void    ParseSettings (QWidget* widget, QHash<QString, QVariant>* pSettings) = 0;

    protected:
        // Обработка событий приложения
        virtual void    customEvent (QEvent *event) = 0;

        // Отправка запроса
        int sendRequest (TPlgReq req, void* arg)
        {
            TPlgReqParam param = {-1, arg};
            CPlgEvent ev(req);
            ev.SetParam(&param);

            QApplication::sendEvent(parent(), &ev);

            return param.result;
        }
};
