//*****************************************************************************
//
// Имя файла    : 'factory.h'
// Описание     : Фабрика
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************
#pragma once

#include <QStringList>
#include <QHash>
#include "core.h"
#include "ui/gui.h"
#include "io/driver.h"
#include "session.h"
#include "settings.h"
#include "plugins/plugin_mgr.h"


// ============================================================================
//                                  CFactory
// ============================================================================

class CFactory
{
    public:
        static CCore*       GetCore (void);
        static CGui*        GetGui	(void);
        static CSettings*   GetSettings	(void);

        static CDriver*     GetDriver (const QString* drvName);
        static int          GetDrivers (QStringList* pList);
        static CDriver*     CreateDriver (const QString* name = NULL);

        // Управление сессиями ввода-вывода
        static CIoSess*     SessionCreate (void);
        static int          SessionDelete (CIoSess* session);
        static int          SessionList (QList<CIoSess*>* pList);

        static CPluginMgr*  GetPluginMgr (void);
};
