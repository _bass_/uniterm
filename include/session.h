//*****************************************************************************
//
// Имя файла    : 'session.h'
// Описание     : Модель для работы с сессией ввода-вывода
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************
#pragma once

#include <QObject>
#include <QVector>
#include <QTimer>
#include "io/driver.h"
#include "types.h"


// ============================================================================
//                                  Константы
// ============================================================================

    // Максимальный размер пакета (сделать через настройки!)
    #define SESS_RX_BUFF_SIZE                   4096


// ============================================================================
//                                  Типы
// ============================================================================

    // Типы (направления передачи) пакетов
    typedef enum
    {
        IO_TYPE_IN,             // Входящий пакет
        IO_TYPE_OUT             // Исходящий пакет
    } TIoType;

    // Пакет ввода-вывода
    class CIoSess;
    typedef struct
    {
        TIoType     type;       // Тип пакета
        TTime       time;       // Время формирования сообщения
        char*       data;       // Указатель на данные
        uint        size;       // Размер блока данных
        CIoSess*    session;    // Сессия (владелец пакета)
        bool        dynMem;     // Флаг динамической памяти буфера (необходимо освобождение памяти)
    } TIoPack;


// ============================================================================
//                                  CIoSess
// ============================================================================

class CIoSess : public QObject
{
    Q_OBJECT

    public:
        CIoSess (uint id);
        ~CIoSess ();

        uint        GetId (void) { return m_id; }
        void        SetDriver (CDriver* drv);
        CDriver*    GetDriver (void);

    signals:
        // Получен (сформирован) входящий пакет
        void    SignalPacketReceived (TIoPack* packet);
        // Исходящий пакет отправлен
        void    SignalPacketSended (TIoPack* packet);
        // Запуск процесса отправки
        void    privateSigDoSend (const char* data, uint size);

    public slots:
        // Отправка данных
        int     SendData (const char* data, uint size);
        // Обработчик входящих данных (парсер пакетов)
        void    OnDataRecv (const char* data, uint size);

    private:
        const uint  m_id;
        CDriver*    m_pDrv;             // Драйвер IO
        char        m_buffRx[SESS_RX_BUFF_SIZE];
        uint        m_indexRx;
        QTimer      m_timerParser;
        QVector<TIoPack> m_listTx;      // Список отправляемых пакетов

        void    configureTimer (const QHash<QString, QVariant>* params);

    private slots:
          void  onDataSended (const char* data, uint size);
          void  onParserTimeout (void);
          void  onSettingsChanged (const QString* group, QHash<QString, QVariant>* params);
};
