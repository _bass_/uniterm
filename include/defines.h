//*****************************************************************************
//
// Имя файла    : 'defines.h'
// Описание     : Глобальные константы
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************
#pragma once

#include <QtGlobal>


#define APP_TITLE                           "UniTerm"
#define APP_SHORT_TITLE                     "uniterm"
#define APP_ORGANIZATION                    "home"
#define APP_VERSION                         "0.0.3"

#ifdef Q_WS_WIN
    #define PLUGIN_EXTENSION                "dll"
#elif defined(Q_OS_LINUX)
    #define PLUGIN_EXTENSION                "so"
#endif


// Конфигурация по умолчанию
#define CFG_GEN_DEFAULT_COLOR_TEXT_IN       Qt::green
#define CFG_GEN_DEFAULT_COLOR_TEXT_OUT      Qt::white
#define CFG_GEN_DEFAULT_COLOR_BG            Qt::black
#define CFG_GEN_DEFAULT_FONT_FAMILY         "DejaVu Sans Mono"
#define CFG_GEN_DEFAULT_FONT_SIZE           11
// Сочетания клавиш
#define CFG_GEN_DEFAULT_KEY_TABNEW          "Ctrl+T"
#define CFG_GEN_DEFAULT_KEY_TABCLOSE        "Ctrl+W"
#define CFG_GEN_DEFAULT_KEY_IO_SETTINGS     "Ctrl+P"
#define CFG_GEN_DEFAULT_KEY_IO_OPENCLOSE    "Ctrl+O"

#define CFG_SESS_DEFAULT_CHARSET            "Windows-1251"
#define CFG_SESS_DEFAULT_EOL                "\n"
// Таймаут парсера пaкетов (мс)
#define CFG_SESS_DEFAULT_TIMEOUT            100
