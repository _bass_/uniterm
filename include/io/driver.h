//*****************************************************************************
//
// Имя файла    : 'driver.h'
// Описание     : Интерфейс драйверов ввода-вывода
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************
#pragma once

#include <QObject>
#include <QWidget>
#include <QHash>
#include <QVariant>
#include "types.h"


// ============================================================================
//                                  CDriver
// ============================================================================

class CDriver : public QObject
{
    Q_OBJECT

    public:
        CDriver (QObject* parent) : QObject(parent) {}
        virtual ~CDriver () {}

        // Создание нового объекта класса драйвера
        virtual CDriver*  NewInstance (void) = 0;

        // Запрос имени плагина (файла)
        virtual QString GetName (void) = 0;
        // Запрос версии плагина
        virtual QString GetVersion (void) = 0;
        // Запрос названия плагина (человеческое)
        virtual QString GetTitle (void) = 0;
        // Запрос описания плагина
        virtual QString GetDesc (void) = 0;

        // Отрисовка параметров драйвера
        virtual void    DrawSettings (QWidget* widget, QHash<QString, QVariant>* pSettings) = 0;
        // Сохранение выбранных параметров
        virtual void    SaveSettings (QWidget* widget, QHash<QString, QVariant>* pSettings) = 0;

        // Конфигурирование драйвера
        virtual int     Configure (QHash<QString, QVariant>* params) = 0;
        // Открытие устройства
        virtual int     Open (void) = 0;
        // Закрытие устройства
        virtual int     Close (void) = 0;
        // Определение состояния устройства
        virtual bool    IsOpen (void) = 0;

    public slots:
        // Запись данных в устройство
        virtual void    Send (const char* data, uint size) = 0;

    signals:
        int     Received (const char* buff, uint size);
        int     Sended (const char* data, uint size);
};
