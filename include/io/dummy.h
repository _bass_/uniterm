//*****************************************************************************
//
// Имя файла    : 'dummy.h'
// Описание     : Заглушка, драйвер ввода-вывода по умолчанию
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************
#pragma once

#include <QObject>
#include "io/driver.h"


// ============================================================================
//                                  CDrvDummy
// ============================================================================

class CDrvDummy : public CDriver
{
    Q_OBJECT

    public:
        CDrvDummy (QObject* parent);

        CDriver*  NewInstance (void);
        QString GetName (void);
        QString GetVersion (void);
        QString GetTitle (void);
        QString GetDesc (void);

        void    DrawSettings (QWidget* widget, QHash<QString, QVariant>* pSettings);
        void    SaveSettings (QWidget* widget, QHash<QString, QVariant>* pSettings);

        int     Configure (QHash<QString, QVariant>* params);
        int     Open (void);
        int     Close (void);
        bool    IsOpen (void);

    public slots:
        // Запись данных в устройство
        void    Send (const char* data, uint size);
};
