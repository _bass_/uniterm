<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>About</name>
    <message>
        <location filename="../ui/about.ui" line="26"/>
        <source>About</source>
        <translation>О приложении</translation>
    </message>
    <message>
        <location filename="../ui/about.ui" line="62"/>
        <source>UniTerm</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>CGui</name>
    <message>
        <location filename="../../src/ui/gui.cpp" line="194"/>
        <source>Add tab</source>
        <translation>Добавить вкладку</translation>
    </message>
    <message>
        <location filename="../../src/ui/gui.cpp" line="259"/>
        <source>Ready</source>
        <translation>Готов</translation>
    </message>
    <message>
        <location filename="../../src/ui/gui.cpp" line="289"/>
        <location filename="../../src/ui/gui.cpp" line="458"/>
        <source>No driver</source>
        <translation>Нет драйвера</translation>
    </message>
    <message>
        <location filename="../../src/ui/gui.cpp" line="300"/>
        <source>Close tab</source>
        <translation>Закрыть вкладку</translation>
    </message>
    <message>
        <location filename="../../src/ui/gui.cpp" line="322"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="../../src/ui/gui.cpp" line="325"/>
        <location filename="../../src/ui/gui.cpp" line="517"/>
        <source>Help</source>
        <translation>Помощь</translation>
    </message>
    <message>
        <location filename="../../src/ui/gui.cpp" line="328"/>
        <source>About</source>
        <translation>О приложении</translation>
    </message>
    <message>
        <location filename="../../src/ui/gui.cpp" line="381"/>
        <source>Driver open ERROR!</source>
        <translation>Ошибка включения драйвера!</translation>
    </message>
    <message>
        <location filename="../../src/ui/gui.cpp" line="517"/>
        <source>Help file not found</source>
        <translation>Не найден файл справки</translation>
    </message>
</context>
<context>
    <name>ISettingsGroup</name>
    <message>
        <location filename="../../src/ui/widgets/settings/wsettingsbase.cpp" line="56"/>
        <source>Base</source>
        <translation>Общие</translation>
    </message>
    <message>
        <location filename="../../src/ui/widgets/settings/wsettingskeybindings.cpp" line="77"/>
        <source>Keybindings</source>
        <translation>Горячие клавиши</translation>
    </message>
    <message>
        <location filename="../../src/ui/widgets/settings/wsettingsplugins.cpp" line="55"/>
        <source>Plugins</source>
        <translation>Плагины</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../src/ui/widgets/settings/wsettingskeybindings.cpp" line="36"/>
        <source>base/shortcut.newtab</source>
        <translation>Добавить вкладку</translation>
    </message>
    <message>
        <location filename="../../src/ui/widgets/settings/wsettingskeybindings.cpp" line="37"/>
        <source>base/shortcut.closetab</source>
        <translation>Закрыть вкладку</translation>
    </message>
    <message>
        <location filename="../../src/ui/widgets/settings/wsettingskeybindings.cpp" line="38"/>
        <source>base/shortcut.ioopenclose</source>
        <translation>Управление активацией драйвера IO</translation>
    </message>
    <message>
        <location filename="../../src/ui/widgets/settings/wsettingskeybindings.cpp" line="39"/>
        <source>base/shortcut.iosettings</source>
        <translation>Открыть параметры IO</translation>
    </message>
</context>
<context>
    <name>WAppSettings</name>
    <message>
        <location filename="../ui/wappsettings.ui" line="23"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
</context>
<context>
    <name>WIoSettings</name>
    <message>
        <location filename="../ui/wiosettings.ui" line="23"/>
        <source>IO settings</source>
        <translation>Параметры IO</translation>
    </message>
    <message>
        <location filename="../ui/wiosettings.ui" line="51"/>
        <source>Base settings</source>
        <translation>Общие</translation>
    </message>
    <message>
        <location filename="../ui/wiosettings.ui" line="78"/>
        <source>Driver</source>
        <translation>Драйвер</translation>
    </message>
    <message>
        <location filename="../ui/wiosettings.ui" line="95"/>
        <source>Charset</source>
        <translation>Кодировка</translation>
    </message>
    <message>
        <location filename="../ui/wiosettings.ui" line="112"/>
        <source>Line ending</source>
        <translation>Конец строки</translation>
    </message>
    <message>
        <location filename="../ui/wiosettings.ui" line="129"/>
        <source>Timeout (ms)</source>
        <translation>Таймаут (мс)</translation>
    </message>
</context>
<context>
    <name>WPanel</name>
    <message>
        <location filename="../../src/ui/widgets/wpanel.cpp" line="58"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="../../src/ui/widgets/wpanel.cpp" line="75"/>
        <source>IO settings</source>
        <translation>Параметры IO</translation>
    </message>
    <message>
        <location filename="../../src/ui/widgets/wpanel.cpp" line="88"/>
        <source>Open / Close</source>
        <translation>Управление активацией драйвера IO</translation>
    </message>
</context>
<context>
    <name>WSettingsBase</name>
    <message>
        <location filename="../ui/settings_base.ui" line="41"/>
        <source>Font</source>
        <translation>Шрифт</translation>
    </message>
    <message>
        <location filename="../ui/settings_base.ui" line="62"/>
        <source>Font size</source>
        <translation>Размер шрифта</translation>
    </message>
    <message>
        <location filename="../ui/settings_base.ui" line="82"/>
        <source>Background color</source>
        <translation>Цвет фона</translation>
    </message>
    <message>
        <location filename="../ui/settings_base.ui" line="119"/>
        <source>Text color (input)</source>
        <translation>Цвет текста (входящие)</translation>
    </message>
    <message>
        <location filename="../ui/settings_base.ui" line="153"/>
        <source>Text color (output)</source>
        <translation>Цвет текста (исходящие)</translation>
    </message>
    <message>
        <location filename="../ui/settings_base.ui" line="187"/>
        <source>Stay on top</source>
        <translation>Поверх всех окон</translation>
    </message>
</context>
<context>
    <name>WSettingsKeyBindings</name>
    <message>
        <location filename="../ui/settings_keybindings.ui" line="70"/>
        <source>Shortcut</source>
        <translation>Сочетание</translation>
    </message>
</context>
</TS>
