//*****************************************************************************
//
// Имя файла    : 'log.h'
// Описание     : Плагин логирования
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************
#pragma once

#include <QObject>
#include <QTranslator>
#include "plugins/plugin.h"
#include "session.h"


// ============================================================================
//                                  CPlgLog
// ============================================================================

class CPlgLog : public CPlugin
{
    Q_OBJECT

    public:
        CPlgLog (QObject* parent);
        ~CPlgLog ();

        QString GetName (void);
        QString GetVersion (void);
        QString GetTitle (void);
        QString GetDesc (void);

        // Конфигурирование
        int     Configure (QHash<QString, QVariant>* pSettings);
        // Отрисовка параметров плагина
        void    DrawSettings (QWidget* widget, QHash<QString, QVariant>* pSettings);
        // Сохранение выбранных параметров
        void    ParseSettings (QWidget* widget, QHash<QString, QVariant>* pSettings);

    private:
        QTranslator m_translator;

        // Обработка событий приложения
        void    customEvent (QEvent *event);
        // Обработчики событий
        void    onPacket (TIoPack* packet);
};
