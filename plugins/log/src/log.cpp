//*****************************************************************************
//
// Имя файла    : 'log.cpp'
// Описание     : Плагин логирования
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************

#include "log.h"
#include <QApplication>
#include <QDir>
#include <QFile>


// ============================================================================
//                              Константы
// ============================================================================

    #define PLG_NAME                    "log"
    #define PLG_VERSION                 "0.0.1"

    #define PLG_LOG_DIR                 "logs"
    #define PLG_LOG_DEFAULT_FILE        "default.log"
    #define PLG_LOG_FNAME_FORMAT        "default_%1.log"


// ============================================================================
///                              Конструктор
// ============================================================================
/// \param  parent  Родительский объект
// ============================================================================

CPlgLog::CPlgLog (QObject* parent) :
    CPlugin(parent),
    m_translator(this)
{
    // Загрузка перевода
    QString fname = ":/plg_log/lang/" + QLocale::system().name().mid(0, 2);
    m_translator.load(fname);
    qApp->installTranslator(&m_translator);

    // Проверка наличия директории с логами
    QDir dir( QApplication::applicationDirPath() );
    if ( !dir.exists(PLG_LOG_DIR) )
    {
        dir.mkdir(PLG_LOG_DIR);
    }
}


// ============================================================================
//
///                              Деструктор
//
// ============================================================================

CPlgLog::~CPlgLog ()
{
    // Удаление переводчика
    qApp->removeTranslator(&m_translator);
}


// ============================================================================
///                     Запрос имени плагина (файла)
// ============================================================================
/// \return     Строка с именем плагина
// ============================================================================

QString CPlgLog::GetName (void)
{
    return QString(PLG_NAME);
}


// ============================================================================
///                     Запрос версии плагина
// ============================================================================
/// \return     Строка с версией
// ============================================================================

QString CPlgLog::GetVersion (void)
{
    return QString(PLG_VERSION);
}


// ============================================================================
///                     Запрос названия драйвера
// ============================================================================
/// \return     Строка с названием
// ============================================================================

QString CPlgLog::GetTitle (void)
{
    return tr("Log");
}


// ============================================================================
///                     Запрос описания драйвера
// ============================================================================
/// \return     Строка с описанием
// ============================================================================

QString CPlgLog::GetDesc ()
{
    return tr("Log description");
}


// ============================================================================
///                         Конфигурирование
// ============================================================================
/// \param  pSettings   Конфигурационные параметры
// ============================================================================

int CPlgLog::Configure (QHash<QString, QVariant>* pSettings)
{
    Q_UNUSED(pSettings);
#warning "FIXME"
    return 0;
}


// ============================================================================
///                     Отрисовка параметров драйвера
// ============================================================================
/// \param  widget      Виджет для отрисовки настроек
/// \param  pSettings   Актульные значения конфигурации
// ============================================================================

void CPlgLog::DrawSettings (QWidget* widget, QHash<QString, QVariant>* pSettings)
{
    Q_UNUSED(pSettings);
    Q_UNUSED(widget);
#warning "FIXME"
}


// ============================================================================
///                     Разбор выбранных параметров
// ============================================================================
/// \param  widget      Виджет с настройками настроек
/// \param  pSettings   Хранилище настроек
// ============================================================================

void CPlgLog::ParseSettings (QWidget* widget, QHash<QString, QVariant>* pSettings)
{
    Q_UNUSED(widget);
    Q_UNUSED(pSettings);
#warning "FIXME"
}


// ============================================================================
///                     Обработка событий от приложения
// ============================================================================
/// \param  event   Объект события (CPlgEvent*)
// ============================================================================

void CPlgLog::customEvent (QEvent* event)
{
    CPlgEvent* plgEvent = static_cast<CPlgEvent*>(event);
    TPlgEv code = (TPlgEv) event->type();

    switch (code)
    {
        case PLG_EV_GUI_BUILD_MENU: break;
        case PLG_EV_GUI_BUILD_PANEL:break;
        case PLG_EV_SESS_CREATE:    break;
        case PLG_EV_SESS_DELETE:    break;
        case PLG_EV_SESS_PACKET_AFTER_SEND:
        case PLG_EV_SESS_PACKET_AFTER_RECV:
            onPacket( (TIoPack*)plgEvent->GetParam() );
        break;
        default:
            QObject::customEvent(event);
        break;
    } // switch ( event->type() )
}


// ============================================================================
///                         Обработчик пакетов
// ============================================================================
/// \param  packet  Пакет IO
// ============================================================================

void CPlgLog::onPacket (TIoPack* packet)
{
    char* buff = (char*) malloc( sizeof("[01.01.00 00:11:22.333] > ") + packet->size + sizeof("\r\n") );
    if (!buff) return;

    do
    {
        int len = sprintf(
                            buff,
                            "[%.2u.%.2u.%.2u %.2u:%.2u:%.2u.%.3u] %c ",
                            packet->time.day, packet->time.month, packet->time.year,
                            packet->time.hour, packet->time.min, packet->time.sec, packet->time.ms,
                            ( (packet->type == IO_TYPE_IN) ? '<' : '>' )
        );
        memcpy(&buff[len], packet->data, packet->size);
        len += packet->size;

        if (buff[len - 1] != '\n')
        {
            buff[len] = '\n';
            ++len;
        }

        buff[len] = '\0';

        // Запись в файл
        QString fname = (packet->session)
                        ? QString(PLG_LOG_FNAME_FORMAT).arg(packet->session->GetId())
                        : PLG_LOG_DEFAULT_FILE;
        fname.prepend("/" PLG_LOG_DIR "/");
        fname.prepend( QApplication::applicationDirPath() );

        QFile fd(fname, this);

        if ( !fd.open(QIODevice::WriteOnly|QIODevice::Append) ) break;
        fd.write(buff, len);
        fd.close();
    } while (0);

    free(buff);
}
