<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>CPlgReformator</name>
    <message>
        <location filename="../../src/reformator.cpp" line="121"/>
        <source>Reformator</source>
        <translation>Реформатор</translation>
    </message>
    <message>
        <location filename="../../src/reformator.cpp" line="133"/>
        <source>Reformator description</source>
        <translation>Плагин изменения формата входящих данных</translation>
    </message>
    <message>
        <location filename="../../src/reformator.cpp" line="293"/>
        <source>On/off reformator</source>
        <translation>Включение/выключение переформатирования</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../src/reformator.cpp" line="33"/>
        <source>plg_reformator/shortcut.activate</source>
        <translation>Активация плагина &quot;реформатор&quot;</translation>
    </message>
</context>
<context>
    <name>WReformatorSettings</name>
    <message>
        <location filename="../ui/settings.ui" line="34"/>
        <source>Show source text</source>
        <translation>Вывод исходной строки</translation>
    </message>
</context>
</TS>
