//*****************************************************************************
//
// Имя файла    : 'reformator.cpp'
// Описание     : Плагин изменения формата данных
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************

#include "reformator.h"
#include "ui/gui.h"
#include "ui_settings.h"
#include "settings.h"
#include <QToolButton>
#include <QLayout>
#include <stdio.h>


// ============================================================================
//                              Константы
// ============================================================================

    #define PLG_NAME                            "reformator"
    #define PLG_VERSION                         "0.0.2"

    #define PLG_REFORMATOR_DEFAULT_SHORTCUT     "Ctrl+R"
    #define PLG_REFORMATOR_BTN_NAME             "reformator"
    #define PLG_GRAPH_SHORTCUT_PARAM_NAME       SETTINGS_SHORTCUT_PARAM_PREFIX "activate"
    #define PLG_GRAPH_SHOWSRC_PARAM_NAME        "show_src"

    // Названия параметров горячих клавиш (контекст - QObject)
    static const char* hiddenTranslations[] =
    {
        QT_TRANSLATE_NOOP("QObject", "plg_reformator/shortcut.activate")
    };


// ============================================================================
///                              Конструктор
// ============================================================================
/// \param  parent  Родительский объект
// ============================================================================

CPlgReformator::CPlgReformator (QObject* parent) :
    CPlugin(parent),
    ui(new Ui::WReformatorSettings),
    m_shortcut(PLG_REFORMATOR_DEFAULT_SHORTCUT),
    m_translator(this),
    m_cfgShowSrc(false)
{
    Q_UNUSED(hiddenTranslations);

    // Загрузка перевода
    QString fname = ":/plg_reformator/lang/" + QLocale::system().name().mid(0, 2);
    m_translator.load(fname);
    qApp->installTranslator(&m_translator);
}


// ============================================================================
//
///                              Деструктор
//
// ============================================================================

CPlgReformator::~CPlgReformator ()
{
    // Удаление кнопок со всех панелей
    QList<QWidget*> listPanels;
    int result = sendRequest(PLG_REQ_LIST_PANELS, &listPanels);
    if (result < 0) return;

    foreach (QWidget* panel, listPanels)
    {
        QLayout* layout = panel->layout();
        if (!layout) continue;

        QToolButton* btn = panel->findChild<QToolButton*>(PLG_REFORMATOR_BTN_NAME);
        if (!btn) continue;

        layout->removeWidget(btn);
        btn->deleteLater();
    }

    // Удаление переводчика
    qApp->removeTranslator(&m_translator);
}


// ============================================================================
///                     Запрос имени плагина (файла)
// ============================================================================
/// \return     Строка с именем плагина
// ============================================================================

QString CPlgReformator::GetName (void)
{
    return QString(PLG_NAME);
}


// ============================================================================
///                     Запрос версии плагина
// ============================================================================
/// \return     Строка с версией
// ============================================================================

QString CPlgReformator::GetVersion (void)
{
    return QString(PLG_VERSION);
}


// ============================================================================
///                     Запрос названия драйвера
// ============================================================================
/// \return     Строка с названием
// ============================================================================

QString CPlgReformator::GetTitle (void)
{
    return tr("Reformator");
}


// ============================================================================
///                     Запрос описания драйвера
// ============================================================================
/// \return     Строка с описанием
// ============================================================================

QString CPlgReformator::GetDesc ()
{
    return tr("Reformator description");
}


// ============================================================================
///                         Конфигурирование
// ============================================================================
/// \param  pSettings   Конфигурационные параметры
// ============================================================================

int CPlgReformator::Configure (QHash<QString, QVariant>* pSettings)
{
    Q_UNUSED(pSettings);

    QString shortcut = pSettings->value(PLG_GRAPH_SHORTCUT_PARAM_NAME, PLG_REFORMATOR_DEFAULT_SHORTCUT).toString();
    if ( !shortcut.isEmpty() )
    {
        m_shortcut = shortcut;
    }

    m_cfgShowSrc = pSettings->value(PLG_GRAPH_SHOWSRC_PARAM_NAME, false).toBool();

    // Обновление UI в соответствии с актуальнай конфигурацией
    QList<QWidget*> listPanels;
    int result = sendRequest(PLG_REQ_LIST_PANELS, &listPanels);
    if (result < 0) return result;

    foreach (QWidget* panel, listPanels)
    {
        onBuildPanel(panel);
    }

    return 0;
}


// ============================================================================
///                     Отрисовка параметров драйвера
// ============================================================================
/// \param  widget      Виджет для отрисовки настроек
/// \param  pSettings   Актульные значения конфигурации
// ============================================================================

void CPlgReformator::DrawSettings (QWidget* widget, QHash<QString, QVariant>* pSettings)
{
    ui->setupUi(widget);

    bool valShowSrc = pSettings->value(PLG_GRAPH_SHOWSRC_PARAM_NAME, false).toBool();
    QCheckBox* checkbox = widget->findChild<QCheckBox*>("showSource");
    checkbox->setCheckState( (valShowSrc) ? Qt::Checked : Qt::Unchecked );

    if ( !pSettings->contains(PLG_GRAPH_SHORTCUT_PARAM_NAME) )
    {
        pSettings->insert(PLG_GRAPH_SHORTCUT_PARAM_NAME, PLG_REFORMATOR_DEFAULT_SHORTCUT);
    }
}


// ============================================================================
///                     Разбор выбранных параметров
// ============================================================================
/// \param  widget      Виджет с настройками настроек
/// \param  pSettings   Хранилище настроек
// ============================================================================

void CPlgReformator::ParseSettings (QWidget* widget, QHash<QString, QVariant>* pSettings)
{
    QCheckBox* checkbox = widget->findChild<QCheckBox*>("showSource");
    if (!checkbox) return;
    bool valShowSrc = (checkbox->checkState() == Qt::Checked);

    pSettings->insert(PLG_GRAPH_SHOWSRC_PARAM_NAME, valShowSrc);
}


// ============================================================================
///                     Обработчик нажатия на кнопку
// ============================================================================
/// \param  checked     Состояние фиксации кнопки
// ============================================================================

void CPlgReformator::buttonHandler (bool checked)
{
    QToolButton* btn = qobject_cast<QToolButton*>( sender() );
    if (!btn) return;

    QWidget* panel = btn->parentWidget();
    bool isOk;
    uint sessId = panel->property(GUI_PANEL_PROPETRY_SESSID).toUInt(&isOk);
    if (!isOk) return;

    if (checked)
    {
        // Добавление активированной сессии в список
        if (!m_activeList.contains(sessId) ) m_activeList.append(sessId);
    }
    else
    {
        // Удаление сессии из списка активации
        int index = m_activeList.indexOf(sessId);
        if (index >= 0)
        {
            m_activeList.remove(index);
        }
    }
}


// ============================================================================
///                     Обработка событий от приложения
// ============================================================================
/// \param  event   Объект события (CPlgEvent*)
// ============================================================================

void CPlgReformator::customEvent (QEvent* event)
{
    CPlgEvent* plgEvent = static_cast<CPlgEvent*>(event);
    TPlgEv code = (TPlgEv) event->type();

    switch (code)
    {
        case PLG_EV_GUI_BUILD_PANEL:
            onBuildPanel( (QWidget*)plgEvent->GetParam() );
        break;
        case PLG_EV_SESS_DELETE:
            onSessionDelete( (CIoSess*)plgEvent->GetParam() );
        break;
        case PLG_EV_SESS_PACKET_BEFORE_RECV:
            onPacket( (TIoPack*)plgEvent->GetParam() );
        break;
        default:
            QObject::customEvent(event);
        break;
    } // switch ( event->type() )
}


// ============================================================================
///                 Обработчик формирования панели инструментов
// ============================================================================
/// \param  panel   Виджет панели
// ============================================================================

void CPlgReformator::onBuildPanel (QWidget* panel)
{
    if (!panel) return;

    QLayout* layout = panel->layout();
    if (!layout) return;

    // Проверка наличия кнопки
    QToolButton* btn = panel->findChild<QToolButton*>(PLG_REFORMATOR_BTN_NAME);

    if (!btn)
    {
        btn = new QToolButton(panel);
        btn->setObjectName(PLG_REFORMATOR_BTN_NAME);
        btn->setAutoRaise(true);
        btn->setCheckable(true);
        btn->setIcon( QIcon(":/plg_reformator/icon.png") );
        QString tip( tr("On/off reformator") );
        tip.append(" ("+ m_shortcut +")");
        btn->setToolTip(tip);
        connect(btn, &QToolButton::clicked, this, &CPlgReformator::buttonHandler);

        layout->addWidget(btn);
    }

    btn->setShortcut( QKeySequence(m_shortcut) );
}


// ============================================================================
///                     Обработчик удаления сессии
// ============================================================================
/// \param  pSess   Созданная или активированная сессия
// ============================================================================

void CPlgReformator::onSessionDelete (CIoSess* pSess)
{
    // Удаление сессии из списка активации
    uint sessId = pSess->GetId();
    int index = m_activeList.indexOf(sessId);

    if (index >= 0)
    {
        m_activeList.remove(index);
    }
}


// ============================================================================
///                         Обработчик пакетов
// ============================================================================
/// \param  packet  Пакет IO
// ============================================================================

void CPlgReformator::onPacket (TIoPack* packet)
{
    if (!packet->size) return;

    // Пропуск пакетов если функционал не активирован в сессии пакета
    int index = m_activeList.indexOf( packet->session->GetId() );
    if (index < 0) return;

    // 3 символа на 1 исходный символ + 1 байт - null-терминатор от sprintf
    uint buffSize = packet->size * 3 + 1;
    // Исходные данные + \n
    if (m_cfgShowSrc) buffSize += packet->size + 1;

    char* buff = (char*) malloc(buffSize);
    if (!buff) return;

    char* ptr = buff;
    if (m_cfgShowSrc)
    {
        memcpy(ptr, packet->data, packet->size);
        ptr += packet->size;
        if ( *(ptr - 1) != '\n' ) *ptr++ = '\n';
    }

    uint convertSize = 0;
    for (uint i = 0; i < packet->size; ++i)
    {
        int result = sprintf(&ptr[i * 3], "%.2x ", (uchar)packet->data[i]);
        if (result < 0) break;
        convertSize += result;
    }

    if (packet->dynMem) free(packet->data);
    packet->data = buff;
    packet->size = (ptr - buff) + convertSize;
    packet->dynMem = true;
}
