//*****************************************************************************
//
// Имя файла    : 'pluginwrap.h'
// Описание     : Плагин (обертка)
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************
#pragma once

#include <QObject>
#include <QtPlugin>
#include "plugins/iplugin.h"
#include "reformator.h"


// ============================================================================
//                                  CPluginWrap
// ============================================================================

class CPluginWrap : public QObject, public IPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID PLG_PLUGIN_IFACE_ID)
    Q_INTERFACES(IPlugin)

    public:
        CPluginWrap ();
        ~CPluginWrap ();

        CPlugin* GetPlugin (void);

    private:
        CPlgReformator  m_plugin;
};
