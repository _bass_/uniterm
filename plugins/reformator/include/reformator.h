//*****************************************************************************
//
// Имя файла    : 'reformator.h'
// Описание     : Плагин изменения формата данных
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************
#pragma once

#include "plugins/plugin.h"
#include "session.h"
#include <QObject>
#include <QAction>
#include <QVector>
#include <QTranslator>


// ============================================================================
//                                  Типы
// ============================================================================

    namespace Ui {
        class WReformatorSettings;
    }


// ============================================================================
//                                  CPlgReformator
// ============================================================================

class CPlgReformator : public CPlugin
{
    Q_OBJECT

    public:
        CPlgReformator (QObject* parent);
        ~CPlgReformator ();

        QString GetName (void);
        QString GetVersion (void);
        QString GetTitle (void);
        QString GetDesc (void);

        // Конфигурирование
        int     Configure (QHash<QString, QVariant>* pSettings);
        // Отрисовка параметров плагина
        void    DrawSettings (QWidget* widget, QHash<QString, QVariant>* pSettings);
        // Сохранение выбранных параметров
        void    ParseSettings (QWidget* widget, QHash<QString, QVariant>* pSettings);

    private:
        Ui::WReformatorSettings* ui;
        QVector<uint>   m_activeList;   // Список активированных сессий
        QString         m_shortcut;
        QTranslator     m_translator;
        bool            m_cfgShowSrc;   // Управление выводом исходного пакета

        // Обработка событий приложения
        void    customEvent (QEvent *event);
        // Обработчики событий
        void    onPacket (TIoPack* packet);
        // Обработчик формирования панели инструментов
        void    onBuildPanel (QWidget* panel);
        // Обработчик создания / активации сессии
        void    onSessionActivate (CIoSess* pSess);
        // Обработчик удаления сессии
        void    onSessionDelete (CIoSess* pSess);

    public slots:
        // Обработчик кнопки панели инструментов
        void    buttonHandler (bool checked);
};
