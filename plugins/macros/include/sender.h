//*****************************************************************************
//
// Имя файла    : 'sender.h'
// Описание     : Обработчик отправки данных в сессию
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************
#pragma once

#include <QObject>
#include <QString>
#include <QByteArray>
#include "session.h"


// ============================================================================
//                              CPlgMacrosSender
// ============================================================================

class CPlgMacrosSender : public QObject
{
    Q_OBJECT

    public:
        CPlgMacrosSender (QObject* parent = 0);

        void    Send (CIoSess* pSess, const QString* str);

    signals:
        void    sigDoSend (const char* data, uint size);
};
