//*****************************************************************************
//
// Имя файла    : 'settings.h'
// Описание     : Настройки плагина
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************
#pragma once

#include <QScrollArea>
#include <QVBoxLayout>
#include <QWidget>
#include <QVector>
#include "macros.h"


// ============================================================================
//                                  Типы
// ============================================================================

    namespace Ui
    {
        class CSettingsRow;
    }


// ============================================================================
//                              CMacrosSettings
// ============================================================================

class CMacrosSettings : public QScrollArea
{
    Q_OBJECT

    public:
        explicit CMacrosSettings (QVector<TMacrosPrams>* pParams, QWidget* parent = 0);
        ~CMacrosSettings ();

        int     GetParams (QVector<TMacrosPrams>* pParams);

    private:
        QVBoxLayout*    m_pListLayout;

        void    addRow (int index = -1);
        void    updateNumbers (void);

    private slots:
        void    onBtnPlus (bool checked);
        void    onBtnMinus (bool checked);
};
