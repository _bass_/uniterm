//*****************************************************************************
//
// Имя файла    : 'macros.h'
// Описание     : Плагин макросов
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************
#pragma once

#include "plugins/plugin.h"
#include "session.h"
#include <QObject>
#include <QVector>
#include <QToolButton>
#include <QTimer>
#include <QTranslator>


// ============================================================================
//                                  Константы
// ============================================================================

    // Максимальное количество макросов
    #define PLG_MACROS_MAX_COUNT                    10


// ============================================================================
//                                  Типы
// ============================================================================

    // Параметры макроса
    typedef struct
    {
        QString     str;        // Отправляемая строка
        QString     title;      // Заголовок макроса
        uint        timeout;    // Время автоповтора
    } TMacrosPrams;


// ============================================================================
//                                  CPlgMacros
// ============================================================================

class CPlgMacros : public CPlugin
{
    Q_OBJECT

    public:
        CPlgMacros (QObject* parent);
        ~CPlgMacros ();

        QString GetName (void);
        QString GetVersion (void);
        QString GetTitle (void);
        QString GetDesc (void);

        // Конфигурирование
        int     Configure (QHash<QString, QVariant>* pSettings);
        // Отрисовка параметров плагина
        void    DrawSettings (QWidget* widget, QHash<QString, QVariant>* pSettings);
        // Сохранение выбранных параметров
        void    ParseSettings (QWidget* widget, QHash<QString, QVariant>* pSettings);

    private:
        QVector<TMacrosPrams>   m_cfg;              // Актуальная конфигурация
        QVector<QTimer*>        m_listTimers;       // Список таймеров (автоповторы)
        QTranslator             m_translator;

        // Конвертация настроек во внутреннее представление параметров
        void    convertSettings (QHash<QString, QVariant>* pSettings, QVector<TMacrosPrams>* pCfg);
        // Настройка UI панели
        void    configurePanel (QWidget* panel);
        // Конфигуррование кнопки макроса на панели инструментов
        void    configureButton (QToolButton* btn, int index);
        // Удаление неиспользуемых таймеров
        void    gcTimers (void);

        // Обработка событий приложения
        void    customEvent (QEvent *event);
        // Обработчик формирования панели инструментов
        void    onBuildPanel (QWidget* panel);
        // Обработчик удаления сессии
        void    onSessionDelete (CIoSess* pSess);

    private slots:
        // Обработчик кнопки панели инструментов
        void    buttonHandler (bool checked);
        // Обработчик таймера автоповтора
        void    onTimeout (void);
};
