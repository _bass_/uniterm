//*****************************************************************************
//
// Имя файла    : 'sender.h'
// Описание     : Обработчик отправки данных в сессию
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************

#include "sender.h"


// ============================================================================
//
///                              Конструктор
//
// ============================================================================

CPlgMacrosSender::CPlgMacrosSender (QObject* parent) :
    QObject(parent)
{
}


// ============================================================================
///                         Запуск отправки данных
// ============================================================================
/// \param  pSess   Указатель на объект сессии
/// \param  str     Отправляемые данные
// ============================================================================

void CPlgMacrosSender::Send (CIoSess* pSess, const QString* str)
{
    if ( !pSess || !str || str->isEmpty() ) return;
    QByteArray ba = str->toUtf8();

    connect( this, SIGNAL(sigDoSend(const char*, uint)), pSess, SLOT(SendData(const char*, uint)) );
    emit sigDoSend( ba.constData(), ba.size() );
    disconnect( this, SIGNAL(sigDoSend(const char*, uint)), pSess, SLOT(SendData(const char*, uint)) );
}
