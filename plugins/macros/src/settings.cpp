//*****************************************************************************
//
// Имя файла    : 'settings.cpp'
// Описание     : Настройки плагина
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************

#include "settings.h"
#include "ui_settings.h"


// ============================================================================
///                              Конструктор
// ============================================================================
/// \param  pParams Начальные параметры (список макросов)
/// \param  parent  Родительский объект
// ============================================================================

CMacrosSettings::CMacrosSettings (QVector<TMacrosPrams>* pParams, QWidget* parent) :
    QScrollArea(parent),
    m_pListLayout()
{
    // Установка компановщика для родительского виджета
    // (для заполнения его текущим виджетом)
    QVBoxLayout* pContentLayout = new QVBoxLayout(parent);
    pContentLayout->setContentsMargins(0, 0, 0, 0);
    pContentLayout->addWidget(this);

    setFrameStyle(QFrame::NoFrame);
    setWidgetResizable(true);

    QWidget* scrollContent = new QWidget(this);
    scrollContent->setObjectName("scrollContent");
    m_pListLayout = new QVBoxLayout(scrollContent);
    m_pListLayout->setAlignment(Qt::AlignTop);
    setWidget(scrollContent);

    if (pParams && pParams->size() > 0)
    {
        TMacrosPrams rowCfg;
        for (int i = 0; i < pParams->size() && i < PLG_MACROS_MAX_COUNT; ++i)
        {
            addRow();

            QLayoutItem* item = m_pListLayout->itemAt(i);
            if (!item) continue;
            QWidget* wrap = item->widget();
            if (!wrap) continue;

            QLineEdit* inputStr = wrap->findChild<QLineEdit*>("str");
            QLineEdit* inputTitle = wrap->findChild<QLineEdit*>("title");
            QSpinBox* inputTimeout = wrap->findChild<QSpinBox*>("timeout");
            if (!inputStr || !inputTitle || !inputTimeout) continue;

            rowCfg = pParams->at(i);
            inputStr->setText( rowCfg.str );
            inputTitle->setText( rowCfg.title );
            inputTimeout->setValue(rowCfg.timeout);
        }
    }
    else
    {
        addRow();
    }
}


// ============================================================================
//
///                              Деструктор
//
// ============================================================================

CMacrosSettings::~CMacrosSettings ()
{
    delete m_pListLayout;
}


// ============================================================================
///                              Добавление строки
// ============================================================================
/// \param  index   Индекс добавляемой строки (-1 - в конец списка)
// ============================================================================

void CMacrosSettings::addRow (int index)
{
    QWidget* wrap = new QWidget(m_pListLayout->widget());
    Ui::CSettingsRow* pRow = new Ui::CSettingsRow;

    pRow->setupUi(wrap);

    connect(pRow->plus, &QToolButton::clicked, this, &CMacrosSettings::onBtnPlus);
    connect(pRow->minus, &QToolButton::clicked, this, &CMacrosSettings::onBtnMinus);

    m_pListLayout->insertWidget(index, wrap);

    delete pRow;

    updateNumbers();
}


// ============================================================================
///                     Обработчик кнопки добавления строки
// ============================================================================
/// \param  checked     Флаг фиксации
// ============================================================================

void CMacrosSettings::onBtnPlus (bool checked)
{
    Q_UNUSED(checked);

    if (m_pListLayout->count() >= PLG_MACROS_MAX_COUNT) return;

    QToolButton* btn = qobject_cast<QToolButton*>( sender() );
    if (!btn) return;

    QWidget* wrap = btn->parentWidget();
    int index = m_pListLayout->indexOf(wrap);

    addRow(index + 1);
}


// ============================================================================
///                     Обработчик кнопки удаления строки
// ============================================================================
/// \param  checked     Флаг фиксации
// ============================================================================

void CMacrosSettings::onBtnMinus (bool checked)
{
    Q_UNUSED(checked);

    // Запрет удаления последней строки
    if (m_pListLayout->count() < 2) return;

    QToolButton* btn = qobject_cast<QToolButton*>( sender() );
    if (!btn) return;

    QWidget* wrap = btn->parentWidget();
    m_pListLayout->removeWidget(wrap);
    wrap->deleteLater();

    updateNumbers();
}


// ============================================================================
///                         Чтение текущих параметров
// ============================================================================
/// \param  pParams Указатель на переменную для сохранения параметров
/// \return         0 или отрицательный код ошибки
// ============================================================================

int CMacrosSettings::GetParams (QVector<TMacrosPrams>* pParams)
{
    if (!pParams || m_pListLayout->count() < 1) return -1;

    TMacrosPrams rowCfg;
    for (int i = 0; i < m_pListLayout->count(); ++i)
    {
        QLayoutItem* item = m_pListLayout->itemAt(i);
        if (!item) continue;
        QWidget* wrap = item->widget();
        if (!wrap) continue;

        QLineEdit* inputStr = wrap->findChild<QLineEdit*>("str");
        QLineEdit* inputTitle = wrap->findChild<QLineEdit*>("title");
        QSpinBox* inputTimeout = wrap->findChild<QSpinBox*>("timeout");
        if (!inputStr || !inputTitle || !inputTimeout) continue;

        rowCfg.str = inputStr->text();
        rowCfg.title = inputTitle->text();
        rowCfg.timeout = inputTimeout->value();

        pParams->append(rowCfg);
    }

    return 0;
}


// ============================================================================
///
///                     Обновление порядковых номеров
///
// ============================================================================

void CMacrosSettings::updateNumbers (void)
{
    for (int i = 0; i < m_pListLayout->count(); ++i)
    {
        QLayoutItem* item = m_pListLayout->itemAt(i);
        if (!item) continue;
        QWidget* wrap = item->widget();
        if (!wrap) continue;
        QLabel* ptr = wrap->findChild<QLabel*>("num");
        if (!ptr) continue;

        ptr->setText( QString::number(i) );
    }
}
