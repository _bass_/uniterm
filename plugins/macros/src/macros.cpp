//*****************************************************************************
//
// Имя файла    : 'graph.cpp'
// Описание     : Плагин рисования графиков
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************

#include "macros.h"
#include "settings.h"
#include "sender.h"
#include "ui/gui.h"
#include <QLayout>
#include <stdio.h>


// ============================================================================
//                              Константы
// ============================================================================

    #define PLG_NAME                    "macros"
    #define PLG_VERSION                 "0.0.1"

    #define PLG_MACROS_BTN_NAME_PREFIX  "macros_"


// ============================================================================
///                              Конструктор
// ============================================================================
/// \param  parent  Родительский объект
// ============================================================================

CPlgMacros::CPlgMacros (QObject* parent) :
    CPlugin(parent),
    m_translator(this)
{
    // Загрузка перевода
    QString fname = ":/plg_macros/lang/" + QLocale::system().name().mid(0, 2);
    m_translator.load(fname);
    qApp->installTranslator(&m_translator);

    m_cfg.clear();
    m_listTimers.clear();
}


// ============================================================================
//
///                              Деструктор
//
// ============================================================================

CPlgMacros::~CPlgMacros ()
{
    // Удаление всех кнопок со всех панелей
    QList<QWidget*> listPanels;
    int res = sendRequest(PLG_REQ_LIST_PANELS, &listPanels);

    if (!res)
    {
        foreach (QWidget* panel, listPanels)
        {
            QLayout* layout = panel->layout();
            if (!layout) continue;

            QList<QToolButton*> listButtons = panel->findChildren<QToolButton*>();
            foreach (QToolButton* btn, listButtons)
            {
                QString name = btn->objectName();
                if ( !name.startsWith(PLG_MACROS_BTN_NAME_PREFIX) ) continue;
                layout->removeWidget(btn);
                btn->deleteLater();
            }
        }
    }

    // Удаление всех таймеров
    while (m_listTimers.size() > 0)
    {
        QTimer* pTimer = m_listTimers.first();
        m_listTimers.removeFirst();
        pTimer->stop();
        pTimer->deleteLater();
    }

    // Удаление переводчика
    qApp->removeTranslator(&m_translator);
}


// ============================================================================
///                     Запрос имени плагина (файла)
// ============================================================================
/// \return     Строка с именем плагина
// ============================================================================

QString CPlgMacros::GetName (void)
{
    return QString(PLG_NAME);
}


// ============================================================================
///                     Запрос версии плагина
// ============================================================================
/// \return     Строка с версией
// ============================================================================

QString CPlgMacros::GetVersion (void)
{
    return QString(PLG_VERSION);
}


// ============================================================================
///                     Запрос названия драйвера
// ============================================================================
/// \return     Строка с названием
// ============================================================================

QString CPlgMacros::GetTitle (void)
{
    return tr("Macros");
}


// ============================================================================
///                     Запрос описания драйвера
// ============================================================================
/// \return     Строка с описанием
// ============================================================================

QString CPlgMacros::GetDesc ()
{
    return tr("Macros description");
}


// ============================================================================
///                         Конфигурирование
// ============================================================================
/// \param  pSettings   Конфигурационные параметры
// ============================================================================

int CPlgMacros::Configure (QHash<QString, QVariant>* pSettings)
{
    convertSettings(pSettings, &m_cfg);

    // Обновление UI в соответствии с актуальнай конфигурацией
    QList<QWidget*> listPanels;
    int result = sendRequest(PLG_REQ_LIST_PANELS, &listPanels);
    if (result < 0) return result;

    foreach (QWidget* panel, listPanels)
    {
        configurePanel(panel);
    }

    return 0;
}


// ============================================================================
///                     Отрисовка параметров драйвера
// ============================================================================
/// \param  widget      Виджет для отрисовки настроек
/// \param  pSettings   Актульные значения конфигурации
// ============================================================================

void CPlgMacros::DrawSettings (QWidget* widget, QHash<QString, QVariant>* pSettings)
{
    if (!pSettings) return;

    // Формирование параметров виджета настроек
    QVector<TMacrosPrams> params;
    convertSettings(pSettings, &params);

    // Создание и отрисовка настроек
    new CMacrosSettings(&params, widget);
}


// ============================================================================
///                     Разбор выбранных параметров
// ============================================================================
/// \param  widget      Виджет с настройками настроек
/// \param  pSettings   Хранилище настроек
// ============================================================================

void CPlgMacros::ParseSettings (QWidget* widget, QHash<QString, QVariant>* pSettings)
{
    if (!widget || !pSettings) return;

    CMacrosSettings* ptr = widget->findChild<CMacrosSettings*>();
    if (!ptr) return;

    QVector<TMacrosPrams> params;
    int result = ptr->GetParams(&params);
    if (result < 0 || params.size() < 1)
    {
        pSettings->insert("count", 0);
        return;
    }

    pSettings->insert("count", params.size());

    TMacrosPrams cfg;
    QString patternStr("str_%1");
    QString patternTitle("title_%1");
    QString patternTimeout("timeout_%1");

    for (int i = 0; i < params.size() && i < PLG_MACROS_MAX_COUNT; ++i)
    {
        cfg = params.at(i);

        pSettings->insert(patternStr.arg(i), cfg.str);
        pSettings->insert(patternTitle.arg(i), cfg.title);
        pSettings->insert(patternTimeout.arg(i), cfg.timeout);
    }
}


// ============================================================================
///         Конвертация настроек во внутреннее представление параметров
// ============================================================================
/// \param  pSettings   Исходные настройки
/// \param  pCfg        Переменная для сохранения конвертированных данных
// ============================================================================

void CPlgMacros::convertSettings (QHash<QString, QVariant>* pSettings, QVector<TMacrosPrams>* pCfg)
{
    if (!pSettings || !pCfg) return;

    pCfg->clear();

    bool isOk;
    QVariant defaultVal( (uint)0 );
    uint count = pSettings->value("count", defaultVal).toUInt(&isOk);

    if (isOk && count)
    {
        TMacrosPrams cfg;
        QString patternStr("str_%1");
        QString patternTitle("title_%1");
        QString patternTimeout("timeout_%1");

        for (uint i = 0; i < count && i < PLG_MACROS_MAX_COUNT; ++i)
        {
            cfg.str = pSettings->value(patternStr.arg(i)).toString();
            cfg.title = pSettings->value(patternTitle.arg(i)).toString();
            cfg.timeout = pSettings->value(patternTimeout.arg(i)).toUInt();
            pCfg->append(cfg);
        }
    }
}


// ============================================================================
///                         Настройка UI панели
// ============================================================================
/// \param  panel   Панель инструментов
// ============================================================================

void CPlgMacros::configurePanel (QWidget* panel)
{
    if (!panel) return;

    QLayout* layout = panel->layout();
    if (!layout) return;

    // Удаление лишних кнопок
    QList<QToolButton*> btnList = panel->findChildren<QToolButton*>();
    int lastIndex = -1;

    foreach (QToolButton* btn, btnList)
    {
        QString name = btn->objectName();
        if ( !name.startsWith(PLG_MACROS_BTN_NAME_PREFIX) ) continue;

        int index = name.mid( sizeof(PLG_MACROS_BTN_NAME_PREFIX) - 1 ).toInt();
        if (index < 0) continue;

        lastIndex = index;

        if ( index >= m_cfg.size() || m_cfg.at(index).str.isEmpty() )
        {
            layout->removeWidget(btn);
            btn->deleteLater();
        }
        else
        {
            configureButton(btn, index);
        }
    }

    // Создание недостающих кнопок
    for (int i = lastIndex + 1; i < m_cfg.size(); ++i)
    {
        TMacrosPrams params = m_cfg.at(i);
        if ( params.str.isEmpty() ) continue;

        QToolButton* btn = new QToolButton(panel);

        configureButton(btn, i);
        connect(btn, &QToolButton::clicked, this, &CPlgMacros::buttonHandler);

        layout->addWidget(btn);
    }

    // Обновление таймеров
    gcTimers();
}


// ============================================================================
///             Конфигуррование кнопки макроса на панели инструментов
// ============================================================================
/// \param  btn     Кнопка
/// \param  index   Индекс макроса/конфигурации
// ============================================================================

void CPlgMacros::configureButton (QToolButton* btn, int index)
{
    if (!btn || index < 0 || index >= m_cfg.size() ) return;

    TMacrosPrams params = m_cfg.at(index);
    QString name(PLG_MACROS_BTN_NAME_PREFIX);
    QString title = params.title;
    QString shortcut("Ctrl+");
    QString tip( tr("Activate macros") );

    tip.append(" #%1 (%2)");
    name.append( QString::number(index) );

    if ( title.isEmpty() ) title = QString("M%1").arg(index);

    if (index < 10) shortcut.append( QString::number(index) );
    else            shortcut.append('a' + index);

    btn->setObjectName(name);
    btn->setAutoRaise(true);
    btn->setText(title);
    btn->setShortcut( QKeySequence(shortcut) );
    btn->setToolTip( tip.arg(index).arg(shortcut) );

    if ( params.timeout > 0 && !btn->isCheckable() )
    {
        btn->setCheckable(true);
    }
    else if ( !params.timeout && btn->isCheckable() )
    {
        if ( btn->isChecked() ) btn->setChecked(false);
        btn->setCheckable(false);
    }
}



// ============================================================================
///
///                         Удаление неиспользуемых таймеров
///
// ============================================================================

void CPlgMacros::gcTimers (void)
{
    QList<CIoSess*> listSessions;
    sendRequest(PLG_REQ_LIST_SESSIONS, &listSessions);

    for (int i = 0; i < m_listTimers.size(); ++i)
    {
        QTimer* pTimer = m_listTimers.at(i);
        bool remove = true;

        do
        {
            QString objName = pTimer->objectName();
            if ( objName.isEmpty() ) break;

            // Проверка существования сесии
            uint sessId = objName.toUInt();
            bool isSessExists = false;

            foreach (CIoSess* pSess, listSessions)
            {
                if (!pSess || pSess->GetId() != sessId) continue;
                isSessExists = true;
                break;
            }

            if (!isSessExists) break;

            int macroIndex = pTimer->property("index").toInt();
            if ( macroIndex < 0 || macroIndex >= m_cfg.size() ) break;

            TMacrosPrams params = m_cfg.at(macroIndex);
            if ( !params.timeout || params.str.isEmpty() ) break;

            pTimer->setInterval(params.timeout);

            remove = false;
        } while (0);

        if (remove)
        {
            m_listTimers.remove(i);
            pTimer->stop();
            pTimer->deleteLater();
        }
    }
}


// ============================================================================
///                     Обработчик кнопки панели инструментов
// ============================================================================
/// \param  checked     Состояние кнопки
// ============================================================================

void CPlgMacros::buttonHandler (bool checked)
{
    QToolButton* btn = qobject_cast<QToolButton*>( sender() );
    if (!btn) return;

    QString name = btn->objectName();
    if ( !name.startsWith(PLG_MACROS_BTN_NAME_PREFIX) ) return;

    int index = name.mid( sizeof(PLG_MACROS_BTN_NAME_PREFIX) - 1 ).toInt();
    if ( index < 0 || index >= m_cfg.size() ) return;

    TMacrosPrams params = m_cfg.at(index);
    if ( params.str.isEmpty() ) return;

    // Поиск объекта сессии
    QWidget* panel = btn->parentWidget();
    bool isOk;
    uint sessId = panel->property(GUI_PANEL_PROPETRY_SESSID).toUInt(&isOk);
    if (!isOk) return;

    TPlgReqArgSess arg;
    arg.sessId = sessId;
    int result = sendRequest(PLG_REQ_SESSION, &arg);
    if (result < 0) return;

    CIoSess* pSess = arg.pSession;
    if (!pSess) return;

    if ( checked || !btn->isCheckable() )
    {
        // Отпрака данных
        CPlgMacrosSender* sender = new CPlgMacrosSender(this);
        sender->Send(pSess, &params.str);
        sender->deleteLater();

        // Активация таймера автоповтора
        if (params.timeout > 0)
        {
            // objectName - ID сессии
            // propetry "index" - индекс макроса
            QTimer* pTimer = new QTimer(this);
            m_listTimers.append(pTimer);
            pTimer->setObjectName( QString::number(sessId) );
            pTimer->setProperty("index", index);
            connect(pTimer, &QTimer::timeout, this, &CPlgMacros::onTimeout);
            pTimer->start(params.timeout);
        }
    }
    else
    {
        // Остановка и удаление таймера автоповтора
        QString objName = QString::number(sessId);

        for (int i = 0; i < m_listTimers.size(); ++i)
        {
            QTimer* pTimer = m_listTimers.at(i);
            if (pTimer->objectName() != objName) continue;

            int macroIndex = pTimer->property("index").toInt();
            if (macroIndex != index) continue;

            m_listTimers.remove(i);
            pTimer->stop();
            pTimer->deleteLater();
        }
    }
}


// ============================================================================
///                     Обработчик таймера автоповтора
// ============================================================================
/// \param  event   Объект события (CPlgEvent*)
// ============================================================================

void CPlgMacros::onTimeout (void)
{
    QTimer* pTimer = qobject_cast<QTimer*>( sender() );
    if (!pTimer) return;

    do
    {
        // Проверка существования привязанной сесии
        uint sessId = pTimer->objectName().toUInt();
        TPlgReqArgSess arg;
        arg.sessId = sessId;
        int result = sendRequest(PLG_REQ_SESSION, &arg);
        if (result < 0) break;

        CIoSess* pSess = arg.pSession;
        if (!pSess) break;

        int macroIndex = pTimer->property("index").toInt();
        if ( macroIndex < 0 || macroIndex >= m_cfg.size() ) break;

        TMacrosPrams params = m_cfg.at(macroIndex);
        if ( !params.timeout || params.str.isEmpty() ) break;

        // Передача данных в сессию
        CPlgMacrosSender* sender = new CPlgMacrosSender(this);
        sender->Send(pSess, &params.str);
        sender->deleteLater();

        return;
    } while (0);

    // Удаление таймера при ошибках
    m_listTimers.removeOne(pTimer);
    pTimer->stop();
    pTimer->deleteLater();
}


// ============================================================================
///                     Обработка событий от приложения
// ============================================================================
/// \param  event   Объект события (CPlgEvent*)
// ============================================================================

void CPlgMacros::customEvent (QEvent* event)
{
    CPlgEvent* plgEvent = static_cast<CPlgEvent*>(event);
    TPlgEv code = (TPlgEv) event->type();

    switch (code)
    {
        case PLG_EV_GUI_BUILD_PANEL:
            onBuildPanel( (QWidget*)plgEvent->GetParam() );
        break;
        case PLG_EV_SESS_DELETE:
            onSessionDelete( (CIoSess*)plgEvent->GetParam() );
        break;
        default:
            QObject::customEvent(event);
        break;
    } // switch (code)
}


// ============================================================================
///                 Обработчик формирования панели инструментов
// ============================================================================
/// \param  panel   Виджет панели
// ============================================================================

void CPlgMacros::onBuildPanel (QWidget* panel)
{
    if (!panel) return;
    configurePanel(panel);
}


// ============================================================================
///                     Обработчик удаления сессии
// ============================================================================
/// \param  pSess   Созданная или активированная сессия
// ============================================================================

void CPlgMacros::onSessionDelete (CIoSess* pSess)
{
    Q_UNUSED(pSess);

    // Обновление таймеров
    gcTimers();
}

