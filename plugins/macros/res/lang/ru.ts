<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>CPlgMacros</name>
    <message>
        <location filename="../../src/macros.cpp" line="123"/>
        <source>Macros</source>
        <translation>Макросы</translation>
    </message>
    <message>
        <location filename="../../src/macros.cpp" line="135"/>
        <source>Macros description</source>
        <translation>Плагин макросов отправки данных</translation>
    </message>
    <message>
        <location filename="../../src/macros.cpp" line="330"/>
        <source>Activate macros</source>
        <translation>Активация макроса</translation>
    </message>
</context>
<context>
    <name>CSettingsRow</name>
    <message>
        <location filename="../ui/settings.ui" line="57"/>
        <source>Send string</source>
        <translation>Отправляемая строка</translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="79"/>
        <source>Title</source>
        <translation>Назв.</translation>
    </message>
    <message>
        <location filename="../ui/settings.ui" line="86"/>
        <source>ms</source>
        <translation>мс</translation>
    </message>
</context>
</TS>
