//*****************************************************************************
//
// Имя файла    : 'graphparams.cpp'
// Описание     : Окно параметров графика
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************

#include "graphparams.h"
#include "ui_graphparams.h"


// ============================================================================
///                              Конструктор
// ============================================================================
/// \param  parent  Родительский объект
// ============================================================================

CGraphParams::CGraphParams (QWidget* parent) :
    QDialog(parent),
    ui(new Ui::CGraphParams)
{
    ui->setupUi(this);
    setWindowFlags(Qt::Dialog | Qt::CustomizeWindowHint | Qt::WindowTitleHint | Qt::WindowCloseButtonHint);

    ui->srcX->addItem( tr("Time"), GRAPH_SRC_TIME );
    ui->srcX->addItem( tr("Data"), GRAPH_SRC_DATA );
    ui->srcX->addItem( tr("Step"), GRAPH_SRC_STEP );

    ui->parser->setText("%y");
}


// ============================================================================
//
///                              Деструктор
//
// ============================================================================

CGraphParams::~CGraphParams()
{
    delete ui;
}


// ============================================================================
///                         Чтение текущих параметров
// ============================================================================
/// \param  params  Указатель на переменную для сохранения параметров графика
/// \return         0 или отрицательный код ошибки
// ============================================================================

int CGraphParams::GetParams (TGraphPrams* params)
{
    if (!params) return -1;

    params->srcX = (TGraphSrc) ui->srcX->currentData().toInt();
    params->pattern = ui->parser->text();
    params->title = ui->title->text();

    return 0;
}
