//*****************************************************************************
//
// Имя файла    : 'graph.cpp'
// Описание     : Плагин рисования графиков
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************

#include "graph.h"
#include "graphwin.h"
#include "graphparams.h"
#include "settings.h"
#include "ui/gui.h"
#include <QToolButton>
#include <QLayout>
#include <stdio.h>


// ============================================================================
//                              Константы
// ============================================================================

    #define PLG_NAME                        "graph"
    #define PLG_VERSION                     "0.0.1"

    #define PLG_GRAPH_BTN_NAME              "graph"
    // Shortcut'ы
    #define PLG_GRAPH_SHORTCUT_PARAM_NAME_NEWCHART      SETTINGS_SHORTCUT_PARAM_PREFIX "new_chart"
    #define PLG_GRAPH_SHORTCUT_PARAM_NAME_CHARTSAVE     SETTINGS_SHORTCUT_PARAM_PREFIX "chart_save"
    #define PLG_GRAPH_SHORTCUT_PARAM_NAME_CHARTLOAD     SETTINGS_SHORTCUT_PARAM_PREFIX "chart_load"
    #define PLG_GRAPH_DEFAULT_SHORTCUT_NEWCHART         "Ctrl+G"
    #define PLG_GRAPH_DEFAULT_SHORTCUT_SAVE             "Ctrl+S"
    #define PLG_GRAPH_DEFAULT_SHORTCUT_LOAD             "Ctrl+L"


    // Объявление типа для возможности сохранять в QVariant (передача родителя окну графика)
    Q_DECLARE_METATYPE(QWidget*)

    // Названия параметров горячих клавиш (контекст - QObject)
    static const char* hiddenTranslations[] =
    {
        QT_TRANSLATE_NOOP("QObject", "plg_graph/shortcut.new_chart"),
        QT_TRANSLATE_NOOP("QObject", "plg_graph/shortcut.chart_save"),
        QT_TRANSLATE_NOOP("QObject", "plg_graph/shortcut.chart_load")
    };


// ============================================================================
///                              Конструктор
// ============================================================================
/// \param  parent  Родительский объект
// ============================================================================

CPlgGraph::CPlgGraph (QObject* parent) :
    CPlugin(parent),
    m_pWinParams(NULL),
    m_scNewChart(PLG_GRAPH_DEFAULT_SHORTCUT_NEWCHART),
    m_translator(this)
{
    Q_UNUSED(hiddenTranslations);

    // Загрузка перевода
    QString fname = ":/plg_graph/lang/" + QLocale::system().name().mid(0, 2);
    m_translator.load(fname);
    qApp->installTranslator(&m_translator);
}


// ============================================================================
//
///                              Деструктор
//
// ============================================================================

CPlgGraph::~CPlgGraph ()
{
    if (m_pWinParams)
    {
        m_pWinParams->deleteLater();
    }

    // Удаление всех графиков
    QList<uint> keys = m_graphList.keys();
    foreach (uint sessId, keys)
    {
        QVector<CGraphWin*>* pList = m_graphList.value(sessId);
        if (!pList) continue;

        for (int i = 0; i < pList->size(); ++i)
        {
            CGraphWin* pWin = pList->at(i);
            if (!pWin) continue;
            pWin->deleteLater();
        }

        delete pList;
    }

    // Удаление кнопок со всех панелей
    QList<QWidget*> listPanels;
    int result = sendRequest(PLG_REQ_LIST_PANELS, &listPanels);
    if (result < 0) return;

    foreach (QWidget* panel, listPanels)
    {
        QLayout* layout = panel->layout();
        if (!layout) continue;

        QToolButton* btn = panel->findChild<QToolButton*>(PLG_GRAPH_BTN_NAME);
        if (!btn) continue;

        layout->removeWidget(btn);
        btn->deleteLater();
    }

    // Удаление переводчика
    qApp->removeTranslator(&m_translator);
}


// ============================================================================
///                     Запрос имени плагина (файла)
// ============================================================================
/// \return     Строка с именем плагина
// ============================================================================

QString CPlgGraph::GetName (void)
{
    return QString(PLG_NAME);
}


// ============================================================================
///                     Запрос версии плагина
// ============================================================================
/// \return     Строка с версией
// ============================================================================

QString CPlgGraph::GetVersion (void)
{
    return QString(PLG_VERSION);
}


// ============================================================================
///                     Запрос названия драйвера
// ============================================================================
/// \return     Строка с названием
// ============================================================================

QString CPlgGraph::GetTitle (void)
{
    return tr("Graphics");
}


// ============================================================================
///                     Запрос описания драйвера
// ============================================================================
/// \return     Строка с описанием
// ============================================================================

QString CPlgGraph::GetDesc ()
{
    return tr("Graphics description");
}


// ============================================================================
///                         Конфигурирование
// ============================================================================
/// \param  pSettings   Конфигурационные параметры
// ============================================================================

int CPlgGraph::Configure (QHash<QString, QVariant>* pSettings)
{
    Q_UNUSED(pSettings);

    QString shortcut = pSettings->value(PLG_GRAPH_SHORTCUT_PARAM_NAME_NEWCHART, PLG_GRAPH_DEFAULT_SHORTCUT_NEWCHART).toString();
    if ( !shortcut.isEmpty() ) m_scNewChart = shortcut;

    shortcut = pSettings->value(PLG_GRAPH_SHORTCUT_PARAM_NAME_CHARTSAVE, PLG_GRAPH_DEFAULT_SHORTCUT_SAVE).toString();
    if ( !shortcut.isEmpty() ) m_scChartSave = shortcut;

    shortcut = pSettings->value(PLG_GRAPH_SHORTCUT_PARAM_NAME_CHARTLOAD, PLG_GRAPH_DEFAULT_SHORTCUT_LOAD).toString();
    if ( !shortcut.isEmpty() ) m_scChartLoad = shortcut;


    // Обновление shortcut'ов у созданных ранее графиков
    QList<uint> keys = m_graphList.keys();
    foreach (uint sessId, keys)
    {
        QVector<CGraphWin*>* pList = m_graphList.value(sessId);
        if (!pList) continue;

        for (int i = 0; i < pList->size(); ++i)
        {
            CGraphWin* pWin = pList->at(i);
            if (!pWin) continue;
            pWin->SetShortcut(CGraphWin::SHORTCUT_TYPE_GRAPH_SAVE, m_scChartSave);
            pWin->SetShortcut(CGraphWin::SHORTCUT_TYPE_GRAPH_LOAD, m_scChartLoad);
        }
    }


    // Обновление UI в соответствии с актуальнай конфигурацией
    QList<QWidget*> listPanels;
    int result = sendRequest(PLG_REQ_LIST_PANELS, &listPanels);
    if (result < 0) return result;

    foreach (QWidget* panel, listPanels)
    {
        onBuildPanel(panel);
    }

    return 0;
}


// ============================================================================
///                     Отрисовка параметров драйвера
// ============================================================================
/// \param  widget      Виджет для отрисовки настроек
/// \param  pSettings   Актульные значения конфигурации
// ============================================================================

void CPlgGraph::DrawSettings (QWidget* widget, QHash<QString, QVariant>* pSettings)
{
    Q_UNUSED(widget);

    if ( !pSettings->contains(PLG_GRAPH_SHORTCUT_PARAM_NAME_NEWCHART) )
    {
        pSettings->insert(PLG_GRAPH_SHORTCUT_PARAM_NAME_NEWCHART, PLG_GRAPH_DEFAULT_SHORTCUT_NEWCHART);
    }

    if ( !pSettings->contains(PLG_GRAPH_SHORTCUT_PARAM_NAME_CHARTSAVE) )
    {
        pSettings->insert(PLG_GRAPH_SHORTCUT_PARAM_NAME_CHARTSAVE, PLG_GRAPH_DEFAULT_SHORTCUT_SAVE);
    }

    if ( !pSettings->contains(PLG_GRAPH_SHORTCUT_PARAM_NAME_CHARTLOAD) )
    {
        pSettings->insert(PLG_GRAPH_SHORTCUT_PARAM_NAME_CHARTLOAD, PLG_GRAPH_DEFAULT_SHORTCUT_LOAD);
    }
}


// ============================================================================
///                     Разбор выбранных параметров
// ============================================================================
/// \param  widget      Виджет с настройками настроек
/// \param  pSettings   Хранилище настроек
// ============================================================================

void CPlgGraph::ParseSettings (QWidget* widget, QHash<QString, QVariant>* pSettings)
{
    Q_UNUSED(widget);
    Q_UNUSED(pSettings);
}


// ============================================================================
///                     Обработчик нажатия на кнопку
// ============================================================================
/// \param  checked     Состояние фиксации кнопки
// ============================================================================

void CPlgGraph::buttonHandler (bool checked)
{
    Q_UNUSED(checked);

    if (!m_pWinParams)
    {
        QToolButton* btn = qobject_cast<QToolButton*>( sender() );
        QWidget* parent = btn->parentWidget();
        m_pWinParams = new CGraphParams(parent);
        connect(m_pWinParams, &CGraphParams::accepted, this, &CPlgGraph::onParamsAccepted);
        connect(m_pWinParams, &CGraphParams::rejected, this, &CPlgGraph::onParamsRejected);
    }

    m_pWinParams->show();
}


// ============================================================================
///
///                 Обработчик закрытия окна параметров графика
///
// ============================================================================

void CPlgGraph::onParamsAccepted (void)
{
    if (!m_pWinParams) return;

    do
    {
        TGraphPrams params;
        int result = m_pWinParams->GetParams(&params);
        if (result) break;

        // Создание окна с графиком
        QWidget* parent = m_pWinParams->parentWidget();
        bool isOk;
        uint sessId = parent->property(GUI_PANEL_PROPETRY_SESSID).toUInt(&isOk);
        if (!isOk) break;

        CGraphWin* chart = new CGraphWin(parent);
        chart->SetParams(&params);
        chart->SetShortcut(CGraphWin::SHORTCUT_TYPE_GRAPH_SAVE, m_scChartSave);
        chart->SetShortcut(CGraphWin::SHORTCUT_TYPE_GRAPH_LOAD, m_scChartLoad);

        // Добавление графика в общий список
        if ( !m_graphList.contains(sessId) )
        {
            m_graphList.insert(sessId, new QVector<CGraphWin*>());
        }

        QVector<CGraphWin*>* pList = m_graphList.value(sessId);
        pList->append(chart);

        chart->show();
    } while (0);

    m_pWinParams->deleteLater();
    m_pWinParams = NULL;
}


// ============================================================================
///
///                 Обработчик закрытия окна параметров графика
///
// ============================================================================

void CPlgGraph::onParamsRejected (void)
{
    if (m_pWinParams)
    {
        m_pWinParams->deleteLater();
        m_pWinParams = NULL;
    }
}


// ============================================================================
///                     Обработка событий от приложения
// ============================================================================
/// \param  event   Объект события (CPlgEvent*)
// ============================================================================

void CPlgGraph::customEvent (QEvent* event)
{
    CPlgEvent* plgEvent = static_cast<CPlgEvent*>(event);
    TPlgEv code = (TPlgEv) event->type();

    switch (code)
    {
        case PLG_EV_GUI_BUILD_PANEL:
            onBuildPanel( (QWidget*)plgEvent->GetParam() );
        break;
        case PLG_EV_SESS_DELETE:
            onSessionDelete( (CIoSess*)plgEvent->GetParam() );
        break;
        case PLG_EV_SESS_PACKET_AFTER_RECV:
            onPacket( (TIoPack*)plgEvent->GetParam() );
        break;
        default:
            QObject::customEvent(event);
        break;
    } // switch ( event->type() )
}


// ============================================================================
///                 Обработчик формирования панели инструментов
// ============================================================================
/// \param  panel   Виджет панели
// ============================================================================

void CPlgGraph::onBuildPanel (QWidget* panel)
{
    if (!panel) return;

    QLayout* layout = panel->layout();
    if (!layout) return;

    // Проверка наличия кнопки
    QToolButton* btn = panel->findChild<QToolButton*>(PLG_GRAPH_BTN_NAME);

    if (!btn)
    {
        btn = new QToolButton(panel);
        btn->setObjectName(PLG_GRAPH_BTN_NAME);
        btn->setAutoRaise(true);
        btn->setIcon( QIcon(":/plg_graph/icon.png") );
        QString tip = tr("Add chart");
        tip.append(" (" + m_scNewChart + ")");
        btn->setToolTip(tip);
        connect(btn, &QToolButton::clicked, this, &CPlgGraph::buttonHandler);

        layout->addWidget(btn);
    }

    btn->setShortcut( QKeySequence(m_scNewChart) );
}


// ============================================================================
///                     Обработчик удаления сессии
// ============================================================================
/// \param  pSess   Созданная или активированная сессия
// ============================================================================

void CPlgGraph::onSessionDelete (CIoSess* pSess)
{
    uint sessId = pSess->GetId();
    if ( !m_graphList.contains(sessId) ) return;

    // Удаление всех графиков, привязанных к удаляемой сессии
    QVector<CGraphWin*>* pList = m_graphList.value(sessId);
    for (int i = 0; i < pList->size(); ++i)
    {
        CGraphWin* pWin = pList->at(i);
        if (!pWin) continue;
        pWin->deleteLater();
    }

    m_graphList.remove(sessId);
    delete pList;
}


// ============================================================================
///                         Обработчик пакетов
// ============================================================================
/// \param  packet  Пакет IO
// ============================================================================

void CPlgGraph::onPacket (TIoPack* packet)
{
    if (!packet || !packet->size) return;

    uint sessId = packet->session->GetId();
    if ( !m_graphList.contains(sessId) ) return;

    // Передача пакета графикам с указанной сессией
    QVector<CGraphWin*>* pList = m_graphList.value(sessId);
    for (int i = 0; i < pList->size(); ++i)
    {
        CGraphWin* pWin = pList->at(i);
        if (!pWin) continue;
        pWin->onPacketRecv(packet);
    }
}
