//*****************************************************************************
//
// Имя файла    : 'graphwin.cpp'
// Описание     : Окно с графиком
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************

#include "graphwin.h"
#include "ui_graphwin.h"
#include <QVector>
#include <QSharedPointer>
#include <QFileDialog>


// ============================================================================
//                              Константы
// ============================================================================

    // Максимальное количество точек, отображаемых по умолчанию
    // При превышении график сдвигается вправо
    #define GRAPH_WIN_DEFAULT_MAX_VIEW_PIONT            100


// ============================================================================
//                          Локальные переменные
// ============================================================================

    // Директория для сохранения изображений (относительно директории приложения)
    static const char* const    g_graphDir = "charts";


// ============================================================================
///                              Конструктор
// ============================================================================
/// \param  parent  Родительский объект
// ============================================================================

CGraphWin::CGraphWin (QWidget* parent) :
    QDialog(parent),
    ui(new Ui::CGraphWin),
    m_contextMenu(this),
    m_regExp(),
    m_cursorLine(NULL),
    m_markerIndex(-1),
    m_lockUpdate(false)
{
    ui->setupUi(this);
    setWindowFlags(Qt::Tool | Qt::WindowMaximizeButtonHint | Qt::WindowCloseButtonHint);

    // Настройка полотна
    ui->plot->setInteraction(QCP::iRangeZoom, true);    // Включение взаимодействия удаления/приближения
    ui->plot->setInteraction(QCP::iRangeDrag, true);    // Включение взаимодействия перетаскивания графика
    ui->plot->axisRect()->setRangeDrag(Qt::Horizontal); // Включение перетаскивания только по горизонтальной оси
    ui->plot->axisRect()->setRangeZoom(Qt::Horizontal); // Включение удаления/приближения только по горизонтальной оси
    ui->plot->setBackground(QColor(0x1f, 0x1d, 0x1e));

    QColor axisColor(0x33, 0x33, 0x33);
    QPen axisPen(axisColor, 1, Qt::SolidLine);
    foreach(QCPAxisRect* rect, ui->plot->axisRects())
    {
        foreach(QCPAxis* axis, rect->axes())
        {
            axis->setLabelColor(Qt::lightGray);
            axis->setTickLabelColor(Qt::lightGray);
            axis->setBasePen(axisPen);
            axis->setTickPen(axisPen);
            axis->setSubTickPen(axisPen);
            axis->grid()->setPen(axisPen);
            axis->grid()->setSubGridVisible(false);
            axis->grid()->setZeroLinePen(axisPen);
        }
    }

    connect(ui->plot, &QCustomPlot::mouseMove, this, &CGraphWin::onMouseMove);
    connect(ui->plot, &QCustomPlot::mouseDoubleClick, this, &CGraphWin::onDblClick);
    connect(ui->plot, &QCustomPlot::mouseRelease, this, &CGraphWin::onMouseRelease);

    // Добавление графика и его настройка
    QCPGraph* pGraph = ui->plot->addGraph();
    pGraph->setPen(QColor(0x80, 0x90, 0x70));
    pGraph->setBrush(QColor(0x29, 0x2c, 0x25, 150));

    // Добавление маркера на график
    m_cursorLine = new QCPItemLine(ui->plot);
    m_cursorLine->setVisible(false);
    m_cursorLine->setPen( QPen(Qt::darkRed, 1, Qt::DotLine) );
    m_cursorLine->setTail(QCPLineEnding::esDisc);

    // Формирование контекстного меню
    m_contextMenu.addAction( QIcon(":/plg_graph/save.png"), tr("Save") );
    m_contextMenu.addAction( QIcon(":/plg_graph/import.png"), tr("Load") );
    connect(&m_contextMenu, &QMenu::triggered, this, &CGraphWin::onContextMenu);
}


// ============================================================================
//
///                              Деструктор
//
// ============================================================================

CGraphWin::~CGraphWin()
{
    delete ui;
}


// ============================================================================
///                         Установка параметров графика
// ============================================================================
/// \param  packet  Полученный пакет
// ============================================================================

void CGraphWin::SetParams (const TGraphPrams* params)
{
    m_params = *params;
    if ( m_params.pattern.isEmpty() ) return;

    // Очистка графика
    QCPGraph* pGraph = ui->plot->graph(0);
    if (!pGraph) return;
    pGraph->data()->clear();

    if (m_params.srcX == GRAPH_SRC_TIME)
    {
        QSharedPointer<QCPAxisTickerDateTime> pTicker(new QCPAxisTickerDateTime);
        pTicker->setDateTimeFormat("hh:mm:ss");
        ui->plot->xAxis->setTicker(pTicker);

        // Добавление точки отсчета (с текущего времени)
        QDateTime dateTime = QDateTime::currentDateTime();
        double x = dateTime.toMSecsSinceEpoch() / 1000.0;

        ui->plot->xAxis->setRange(x, x + 60);
        pGraph->rescaleAxes(false);
        ui->plot->replot();
    }
    else
    {
        QSharedPointer<QCPAxisTickerFixed> pTicker(new QCPAxisTickerFixed);
        ui->plot->xAxis->setTicker(pTicker);
    }

    QString pattern = m_params.pattern;
    pattern = pattern.replace("%x", "([-\\d\\.]+)");
    pattern = pattern.replace("%y", "([-\\d\\.]+)");
    m_regExp.setPattern(pattern);

    if ( !m_params.title.isEmpty() )
    {
        setWindowTitle(m_params.title);
    }
}


// ============================================================================
///                         Изменение shortcut'ов
// ============================================================================
/// \param  type        Тип shortcut'а
/// \param  shortcut    Сочетание клавиш
/// \return             0 или отрицательный код ошибки
// ============================================================================

void CGraphWin::SetShortcut (CGraphWin::EScType type, const QString& shortcut)
{
    switch (type)
    {
        case SHORTCUT_TYPE_GRAPH_SAVE: m_scGraphSave = shortcut; break;
        case SHORTCUT_TYPE_GRAPH_LOAD: m_scGraphLoad = shortcut; break;
    }
}


// ============================================================================
///                         Обработчик пакетов
// ============================================================================
/// \param  packet  Полученный пакет
// ============================================================================

void CGraphWin::onPacketRecv (TIoPack* packet)
{
    if (m_lockUpdate || !packet || packet->type != IO_TYPE_IN) return;

    QByteArray ba(packet->data, packet->size);
    QString data(ba);
    QRegularExpressionMatch match = m_regExp.match(data);
    if ( !match.hasMatch() ) return;

    int posY = m_params.pattern.indexOf("%y");
    int posX = m_params.pattern.indexOf("%x");
    if (posY < 0) return;

    QCPGraph* pGraph = ui->plot->graph(0);
    double y = match.captured( (posY < posX || posX < 0) ? 1 : 2 ).toDouble();
    double x;

    switch (m_params.srcX)
    {
        case GRAPH_SRC_DATA:
        {
            if (posX < 0) return;
            x = match.captured( (posX < posY) ? 1 : 2 ).toDouble();
        }
        break;

        case GRAPH_SRC_TIME:
        {
            QTime time(packet->time.hour, packet->time.min, packet->time.sec);
            QDate date(2000 + packet->time.year, packet->time.month, packet->time.day);
            QDateTime dateTime(date, time);

            x = (double)dateTime.toSecsSinceEpoch() + packet->time.ms / 1000.0;
        }
        break;

        case GRAPH_SRC_STEP:
        {
            x = (pGraph) ? pGraph->dataCount() : 0;
            if (x < 0) x = 0;
        }
        break;

        // Выход при ошибке определения значения
        default: return;
    }

    // Определение необходимости обновления отображаемого диапазона
    int dataCount = pGraph->dataCount();
    bool updateRange = false;
    double firstVisibleX = 0;

    if (dataCount > 0)
    {
        QSharedPointer<QCPGraphDataContainer> list = pGraph->data();
        const QCPGraphData* pLastPoint = list->constEnd() - 1;
        const QCPRange range = ui->plot->xAxis->range();
        updateRange = range.contains( pLastPoint->mainKey() );

        if (updateRange)
        {
            if (dataCount > GRAPH_WIN_DEFAULT_MAX_VIEW_PIONT)
            {
                firstVisibleX = (pLastPoint - GRAPH_WIN_DEFAULT_MAX_VIEW_PIONT)->mainKey();
            }
            else
            {
                firstVisibleX = list->constBegin()->mainKey();
            }
        }
    }
    else
    {
        updateRange = true;
        firstVisibleX = x + 30; // +30 сек
    }

    // Добавление новой точки
    pGraph->addData(x, y);

    // Корректировка видимого диапазона по оси Y
    {
        const QCPRange range = ui->plot->yAxis->range();
        if (y <= range.lower)
        {
            double lower = y - (range.upper - y) / height();    // Смещение на 1px ниже графика
            ui->plot->yAxis->setRange(lower, range.upper);
        }
        else if (y >= range.upper)
        {
            double upper = y + (y - range.lower) / height();    // Смещение на 1px выше графика
            ui->plot->yAxis->setRange(range.lower, upper);
        }
    }

    // Корректировка видимого диапазона по оси X
    if (updateRange)
    {
        // Добавленная точка должна попасть в отображаемый диапазон
        ui->plot->xAxis->setRange(firstVisibleX, x);
    }

    ui->plot->replot();
}


// ============================================================================
///
///                         Сохранение графика
///
// ============================================================================

void CGraphWin::save (void)
{
    // Запрос пути файла для сохранения
    QString fileName = qApp->applicationDirPath();
    QDir dir(fileName);

    if ( !dir.exists(g_graphDir) )
    {
        dir.mkdir(g_graphDir);
    }

    fileName.append("/");
    fileName.append(g_graphDir);
    fileName.append("/");
    fileName.append( (m_params.title.isEmpty()) ? "chart" : m_params.title.toLower() );
    fileName.append(".png");

    fileName = QFileDialog::getSaveFileName(
                            this, tr("Save File"),
                            fileName,
                            "*.png *.csv"
                       );

    if ( fileName.isEmpty() ) return;

    if ( fileName.endsWith(".png") )
    {
        ui->plot->savePng(fileName);
    }
    else
    {
        // Сохранние исходных данных в файл .csv
        QCPGraph* pGraph = ui->plot->graph(0);
        QFile file(fileName);
        bool res = file.open(QIODevice::WriteOnly | QIODevice::Truncate);

        do
        {
            if (!pGraph || !res) break;

            QSharedPointer<QCPGraphDataContainer> list = pGraph->data();
            if (!list) break;

            QTextStream out(&file);
            out.setRealNumberNotation(QTextStream::FixedNotation);

            for (QCPGraphDataContainer::const_iterator i = list->constBegin(); i != list->constEnd(); ++i)
            {
                // Сохранение в формате "x;y\n"
                out << i->mainKey() << ";" << i->mainValue() << "\n";
            }

            file.flush();
        } while (0);

        file.close();
    }
}


// ============================================================================
///
///                         Загрузка данных графика
///
// ============================================================================

void CGraphWin::load (void)
{
    // Запрос пути файла для сохранения
    QString baseDir = qApp->applicationDirPath();
    QDir dir(baseDir);

    if ( dir.exists(g_graphDir) )
    {
        baseDir.append("/");
        baseDir.append(g_graphDir);
    }

    QString fileName = QFileDialog::getOpenFileName(
                            this, tr("Open file"),
                            baseDir,
                            "*.csv"
                       );

    if ( fileName.isEmpty() ) return;

    QFile file(fileName);
    bool res = file.open(QIODevice::ReadOnly);
    if (!res) return;

    QCPGraph* pGraph = ui->plot->graph(0);
    if (!pGraph) return;

    // Удаление данных с графика
    QSharedPointer<QCPGraphDataContainer> data = pGraph->data();
    if (data) data->clear();

    // Блокировка обработки входящих пакетов (только отображение загруженных данных)
    m_lockUpdate = true;

    QTextStream in(&file);
    while ( !in.atEnd() )
    {
        QString line = in.readLine();
        QStringList parts = line.split(';');
        if (parts.count() < 2) continue;

        double x = parts.at(0).toDouble();
        double y = parts.at(1).toDouble();
        pGraph->addData(x, y);
    }

    ui->plot->rescaleAxes();
    ui->plot->replot();
}


// ============================================================================
///                         Обработчик нажатия кнопок
// ============================================================================
/// \param  ev  Событие клавиатуры
// ============================================================================

void CGraphWin::keyPressEvent (QKeyEvent* ev)
{
    do
    {
        if (ev->key() == Qt::Key_Shift)
        {
            m_cursorLine->setVisible(true);
            ui->plot->replot();
            return;
        }

        QKeySequence evKey( ev->key() | ev->modifiers() );
        if ( evKey.isEmpty() ) break;

        if ( !m_scGraphSave.isEmpty() && evKey == QKeySequence(m_scGraphSave) )
        {
            save();
        }
        else if ( !m_scGraphLoad.isEmpty() && evKey == QKeySequence(m_scGraphLoad) )
        {
            load();
        }

        return;
    } while (0);

    // Передача события родителю
    QDialog::keyPressEvent(ev);
}


// ============================================================================
///                         Обработчик отпускания кнопок
// ============================================================================
/// \param  ev  Событие клавиатуры
// ============================================================================

void CGraphWin::keyReleaseEvent (QKeyEvent* ev)
{
    if (ev->key() == Qt::Key_Shift)
    {
        m_cursorLine->setVisible(false);
        ui->plot->replot();
        QToolTip::hideText();
    }

    // Передача события родителю
    QDialog::keyReleaseEvent(ev);
}


// ============================================================================
///                         Обработчик двойного клика
// ============================================================================
/// \param  ev  Событие
// ============================================================================

void CGraphWin::onMouseRelease (QMouseEvent* ev)
{
    if (ev->button() != Qt::RightButton) return;

    m_contextMenu.exec( ev->globalPos() );
}


// ============================================================================
///                         Обработчик двойного клика
// ============================================================================
/// \param  ev  Событие
// ============================================================================

void CGraphWin::onDblClick (QMouseEvent* ev)
{
    Q_UNUSED(ev);

    // Сброс масштаба по оси x
    QCPGraph* pGraph = ui->plot->graph(0);
    if (!pGraph) return;
    QSharedPointer<QCPGraphDataContainer> list = pGraph->data();
    if (!list || list->size() <= 0) return;

    const QCPGraphData* pFirstPoint = list->constBegin();
    const QCPGraphData* pLastPoint = list->constEnd() - 1;

    if (!pFirstPoint || !pLastPoint) return;

    ui->plot->xAxis->setRange(pFirstPoint->mainKey(), pLastPoint->mainKey());
    ui->plot->replot();
}


// ============================================================================
///                     Обработчик выбора контекстного меню
// ============================================================================
/// \param  act     Дескриптор действия
// ============================================================================

void CGraphWin::onContextMenu (QAction* act)
{
    if ( act->text() == tr("Save") )
    {
        save();
    }
    else if ( act->text() == tr("Load") )
    {
        load();
    }
}


// ============================================================================
///                         Обработчик движения мыши
// ============================================================================
/// \param  ev  Событие мыши
// ============================================================================

void CGraphWin::onMouseMove (QMouseEvent* ev)
{
    if (!m_cursorLine) return;

    if ( !(QApplication::keyboardModifiers() & Qt::ShiftModifier) )
    {
        // Отмена обработки при скрытом маркере и не нажатой кнопке активации маркера
        // Скрытие маркера при отпускании кнопки активации
        if ( m_cursorLine->visible() )
        {
            m_cursorLine->setVisible(false);
            ui->plot->replot();
            QToolTip::hideText();
        }
        return;
    }

    // Определение значения координаты x, соответствующее позиции курсора
    double x = ui->plot->xAxis->pixelToCoord( ev->pos().x() );

    // Поиск ближайшей к курсору точки
    QCPGraph* pGraph = ui->plot->graph(0);
    QSharedPointer<QCPGraphDataContainer> list = pGraph->data();
    if (list->size() <= 0) return;

    // Для оптимизации скорости, поиск осуществляется с точки, найденной в предыдущий раз
    QCPGraphDataContainer::const_iterator startItr = ( m_markerIndex >= 0 && m_markerIndex < list->size() )
                                                     ? list->at(m_markerIndex)
                                                     : list->constBegin();
    int startIndex = (startItr == list->constBegin()) ? 0 : m_markerIndex;
    int foundIndex = -1;
    const QCPGraphData* pPoint = startItr;

    // Поиск влево
    int index = startIndex;
    for (QCPGraphDataContainer::const_iterator i = startItr; i >= list->constBegin(); --i, --index)
    {
        double key = i->mainKey();
        if ( fabs(x - key) > fabs(x - pPoint->mainKey()) ) break;
        pPoint = i;
        foundIndex = index;
    }

    // Поиск вправо
    index = startIndex;
    for (QCPGraphDataContainer::const_iterator i = startItr; i != list->constEnd(); ++i, ++index)
    {
        double key = i->mainKey();
        if ( fabs(x - key) > fabs(x - pPoint->mainKey()) ) break;
        pPoint = i;
        foundIndex = index;
    }

    // Сохранение индекса найденной точки для быстрого поиска
    if ( m_markerIndex != foundIndex || !m_cursorLine->visible() )
    {
        m_markerIndex = foundIndex;
        m_cursorLine->start->setCoords(pPoint->mainKey(), pPoint->mainValue());
        m_cursorLine->end->setCoords(pPoint->mainKey(), 0);
        m_cursorLine->setVisible(true);
        ui->plot->replot();

        // Отображенеи tooltip со значением в найденной точке
        QPoint tipPos;
        tipPos.setX( ui->plot->xAxis->coordToPixel(pPoint->mainKey()) + 10 );
        tipPos.setY( ui->plot->yAxis->coordToPixel(pPoint->mainValue()) - 40 );
        tipPos = mapToGlobal(tipPos);

        QToolTip::hideText();

        QString tip;
        if (m_params.srcX == GRAPH_SRC_TIME)
        {
            QDateTime dateTime = QCPAxisTickerDateTime::keyToDateTime(pPoint->mainKey());
            tip = QString("x=%1\ny=%2")
                  .arg(dateTime.toString("hh:mm:ss.zzz dd.MM.yy"))
                  .arg(pPoint->mainValue());
        }
        else
        {
            tip = QString("x=%1\ny=%2").arg(pPoint->mainKey()).arg(pPoint->mainValue());
        }

        QToolTip::showText(tipPos, tip);
    }
}
