//*****************************************************************************
//
// Имя файла    : 'graph.h'
// Описание     : Плагин рисования графиков
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************
#pragma once

#include "plugins/plugin.h"
#include "session.h"
#include <QObject>
#include <QAction>
#include <QVector>
#include <QTranslator>


// ============================================================================
//                                  Типы
// ============================================================================

    // Источник данных для графика
    typedef enum
    {
        GRAPH_SRC_DATA,         // Значение берется из данных пакета (парсер)
        GRAPH_SRC_TIME,         // Значение соответствует времени формирования пакета
        GRAPH_SRC_STEP          // Значение - счетчик пакетов
    } TGraphSrc;

    // Параметры графика
    typedef struct
    {
        TGraphSrc   srcX;       // Источник данных для значений по оси X
        QString     pattern;    // Регулярное выражение для поиска в пакете значения (y)
        QString     title;      // Заголовок окна графика
    } TGraphPrams;

    // Объявление склассов окон
    class CGraphWin;
    class CGraphParams;


// ============================================================================
//                                  CPlgGraph
// ============================================================================

class CPlgGraph : public CPlugin
{
    Q_OBJECT

    public:
        CPlgGraph (QObject* parent);
        ~CPlgGraph ();

        QString GetName (void);
        QString GetVersion (void);
        QString GetTitle (void);
        QString GetDesc (void);

        // Конфигурирование
        int     Configure (QHash<QString, QVariant>* pSettings);
        // Отрисовка параметров плагина
        void    DrawSettings (QWidget* widget, QHash<QString, QVariant>* pSettings);
        // Сохранение выбранных параметров
        void    ParseSettings (QWidget* widget, QHash<QString, QVariant>* pSettings);

    private:
        QHash<uint, QVector<CGraphWin*>*> m_graphList;  // Список графиков с привязкой к сессиям
        CGraphParams*   m_pWinParams;                   // Окно настроек создаваемого графика
        QString         m_scNewChart;                   // Shortcut добавления графика
        QString         m_scChartSave;                  // Shortcut сохранения графика
        QString         m_scChartLoad;                  // Shortcut загрузки данных графика
        QTranslator     m_translator;

        // Обработка событий приложения
        void    customEvent (QEvent *event);
        // Обработчики событий
        void    onPacket (TIoPack* packet);
        // Обработчик формирования панели инструментов
        void    onBuildPanel (QWidget* panel);
        // Обработчик создания / активации сессии
        void    onSessionActivate (CIoSess* pSess);
        // Обработчик удаления сессии
        void    onSessionDelete (CIoSess* pSess);

    private slots:
        // Обработчик кнопки панели инструментов
        void    buttonHandler (bool checked);
        // Обработчики закрытия окна параметров графика
        void    onParamsAccepted (void);
        void    onParamsRejected (void);
};
