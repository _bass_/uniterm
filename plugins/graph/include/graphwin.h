//*****************************************************************************
//
// Имя файла    : 'graphwin.h'
// Описание     : Окно с графиком
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************
#pragma once

#include "graph.h"
#include "session.h"
#include "qcustomplot.h"
#include <QDialog>
#include <QTime>
#include <QRegularExpression>


// ============================================================================
//                                  Типы
// ============================================================================

    namespace Ui
    {
        class CGraphWin;
    }


// ============================================================================
//                                  CGraphWin
// ============================================================================

class CGraphWin : public QDialog
{
    Q_OBJECT

    public:
        explicit CGraphWin (QWidget* parent = 0);
        ~CGraphWin();

        enum EScType
        {
            SHORTCUT_TYPE_GRAPH_SAVE,
            SHORTCUT_TYPE_GRAPH_LOAD
        };

        void    SetParams (const TGraphPrams* params);
        void    SetShortcut (EScType type, const QString& shortcut);

    public slots:
        void    onPacketRecv (TIoPack* packet);

    private:
        Ui::CGraphWin*      ui;             // Виджет графика
        QMenu               m_contextMenu;  // Контекстное меню
        TGraphPrams         m_params;       // Конфигурациионные параметры графика
        QRegularExpression  m_regExp;       // Сформированное регулярное выражение для выделения данных из пакета
        QCPItemLine*        m_cursorLine;   // Курсор, маркер положения курсора мыши
        int                 m_markerIndex;  // Индекс последней найденной точки (положение маркера)
        bool                m_lockUpdate;   // Блокировка обработчи входящих данных (отображение загруженных данных)
        QString             m_scGraphSave;  // Shortcut сохранения графика
        QString             m_scGraphLoad;  // Shortcut загрузки графика

        // Сохранение графика
        void    save (void);
        // Загрузка данных графика
        void    load (void);
        // Обработчики событий
        void    keyPressEvent (QKeyEvent* ev);
        void    keyReleaseEvent (QKeyEvent* ev);

    private slots:
        void    onMouseRelease (QMouseEvent* ev);
        void    onMouseMove (QMouseEvent* ev);
        void    onDblClick (QMouseEvent* ev);
        void    onContextMenu (QAction* act);
};
