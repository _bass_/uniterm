<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>CGraphParams</name>
    <message>
        <location filename="../ui/graphparams.ui" line="14"/>
        <source>Graph params</source>
        <translation>Параметры графика</translation>
    </message>
    <message>
        <location filename="../ui/graphparams.ui" line="56"/>
        <source>Source X axis</source>
        <translation>Источник (ось Х)</translation>
    </message>
    <message>
        <location filename="../ui/graphparams.ui" line="73"/>
        <source>Parser</source>
        <translation>Парсер</translation>
    </message>
    <message>
        <location filename="../ui/graphparams.ui" line="93"/>
        <source>Title</source>
        <translation>Заголовок</translation>
    </message>
    <message>
        <location filename="../../src/graphparams.cpp" line="26"/>
        <source>Time</source>
        <translation>Время</translation>
    </message>
    <message>
        <location filename="../../src/graphparams.cpp" line="27"/>
        <source>Data</source>
        <translation>Данные (парсер)</translation>
    </message>
    <message>
        <location filename="../../src/graphparams.cpp" line="28"/>
        <source>Step</source>
        <translation>Номер</translation>
    </message>
</context>
<context>
    <name>CGraphWin</name>
    <message>
        <location filename="../ui/graphwin.ui" line="14"/>
        <source>Chart</source>
        <translation>График</translation>
    </message>
    <message>
        <location filename="../../src/graphwin.cpp" line="91"/>
        <location filename="../../src/graphwin.cpp" line="515"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="../../src/graphwin.cpp" line="92"/>
        <location filename="../../src/graphwin.cpp" line="519"/>
        <source>Load</source>
        <translation>Загрузить</translation>
    </message>
    <message>
        <location filename="../../src/graphwin.cpp" line="311"/>
        <source>Save File</source>
        <translation>Сохранение графика</translation>
    </message>
    <message>
        <location filename="../../src/graphwin.cpp" line="372"/>
        <source>Open file</source>
        <translation>Открыть файл</translation>
    </message>
</context>
<context>
    <name>CPlgGraph</name>
    <message>
        <location filename="../../src/graph.cpp" line="153"/>
        <source>Graphics</source>
        <translation>Графики</translation>
    </message>
    <message>
        <location filename="../../src/graph.cpp" line="165"/>
        <source>Graphics description</source>
        <translation>Плагин формирования графиков на основании входящих данных</translation>
    </message>
    <message>
        <location filename="../../src/graph.cpp" line="394"/>
        <source>Add chart</source>
        <translation>Добавить график</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../src/graph.cpp" line="42"/>
        <source>plg_graph/shortcut.new_chart</source>
        <translation>Добавить график</translation>
    </message>
    <message>
        <location filename="../../src/graph.cpp" line="43"/>
        <source>plg_graph/shortcut.chart_save</source>
        <translation>Сохранить график</translation>
    </message>
    <message>
        <location filename="../../src/graph.cpp" line="44"/>
        <source>plg_graph/shortcut.chart_load</source>
        <translation>Загрузить данные графика (импорт)</translation>
    </message>
</context>
</TS>
