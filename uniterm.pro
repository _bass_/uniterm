
TARGET = uniterm
TEMPLATE = app

CONFIG  -= debug_and_release
CONFIG  -= debug_and_release_target
CONFIG  -= console

QT		+= core gui widgets

QMAKE_CXXFLAGS	+= -pipe
QMAKE_CFLAGS	+= -pipe


CONFIG(debug, debug|release) {
    BUILD_DIR = build/debug
} else {
    BUILD_DIR = build/release
}

DESTDIR = $$BUILD_DIR/bin
RCC_DIR	= $$BUILD_DIR/rcc
UI_DIR	= $$BUILD_DIR/ui
MOC_DIR	= $$BUILD_DIR/moc
OBJECTS_DIR	= $$BUILD_DIR/obj


INCLUDEPATH += include


SOURCES += \
    src/io/*.cpp \
    src/plugins/*.cpp \
    src/ui/*.cpp \
    src/ui/widgets/*.cpp \
    src/ui/widgets/settings/*.cpp \
    src/*.cpp


HEADERS += \
    include/io/*.h \
    include/plugins/*.h \
    include/ui/*.h \
    include/ui/widgets/*.h \
    include/ui/widgets/settings/*.h \
    include/*.h

FORMS += res/ui/*.ui

RESOURCES += res/res.qrc
win32:RC_FILE = res/winres.rc

TRANSLATIONS += res/lang/ru.ts

