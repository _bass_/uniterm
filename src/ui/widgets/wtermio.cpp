//*****************************************************************************
//
// Имя файла    : 'wtermio.cpp'
// Описание     : Виджет ввода-вывода терминала
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************

#include "ui/widgets/wtermio.h"
#include <QTextBlock>
#include <QScrollBar>
#include <QDateTime>
#include <QTextCodec>
#include <QClipboard>
#include <QGuiApplication>
#include <stdio.h>


// ============================================================================
//                              Константы
// ============================================================================

    #define TERM_DEFAULT_LINES_LIMIT            100000
    #define TERM_DEFAULT_PROMPT                 "> "
    #define TERM_DEFAULT_CURSOR_WIDTH           7


// ============================================================================
///
///                             Конструктор
///
// ============================================================================

WTermIO::WTermIO (QWidget* parent) :
    QPlainTextEdit(parent),
    m_prompt(TERM_DEFAULT_PROMPT),
    m_histIndex(-1)
{
    setReadOnly(false);
    setUndoRedoEnabled(false);
    setBackgroundRole(QPalette::NoRole);
    setFrameShape(QFrame::NoFrame);

    setCursorWidth(TERM_DEFAULT_CURSOR_WIDTH);
    setMaximumBlockCount(TERM_DEFAULT_LINES_LIMIT);

    m_history.clear();

    appendPlainText(m_prompt);
}


// ============================================================================
///                         Установка цвета
// ============================================================================
/// \param  type    Тип цвета
/// \param  type    Цвет
// ============================================================================

void WTermIO::SetColor(WTermIO::ColorType type, QColor color)
{
    if (type == ColorType::COLOR_TEXT_OUT)
    {
        m_colorOut = color;
    }
    else
    {
        QPalette p = palette();

        if (type == ColorType::COLOR_TEXT_IN)
        {
            m_colorIn = color;
            p.setColor(QPalette::Text, color);
        }
        else
        {
            p.setColor(QPalette::Base, color);
        }

        setPalette(p);
    }
}


// ============================================================================
///                     Вывод пакета в консоль
// ============================================================================
/// \param  packet  Пакет IO
// ============================================================================

void WTermIO::ShowPacket (TIoPack* packet)
{
    QByteArray data( (const char*)packet->data, packet->size );
    QString str;

    // Конвертация данных в соответствии с установленной кодировкой
    if (
        packet->type == IO_TYPE_IN &&
        data.count() &&
        !m_charset.isEmpty() &&
        m_charset.toLower() != "utf-8"
    ) {
        QTextCodec* codec = QTextCodec::codecForName(m_charset.toUtf8());
        if (codec)
        {
            str = codec->toUnicode(data);
        }
    }
    else
    {
        str = data;
    }

    // Удаление "\r\n" в конце строки (при наличии)
    uint len = str.size();
    if ( len > 0 && str[len - 1] == QChar('\n') )
    {
        str.chop(1);
    }

    len = str.size();
    if ( len > 0 && str[len - 1] == QChar('\r') )
    {
        str.chop(1);
    }

    // Выравнивание многострочного текста по метке времени
    str.replace(QChar('\n'), "\n               ");

    // Добавление метки времени
    char timeStr[ sizeof("[00:11:22.333] ") ] = {};
    int timeLen = sprintf(timeStr, "[%.2u:%.2u:%.2u.%.3u] ", packet->time.hour, packet->time.min, packet->time.sec, packet->time.ms);
    timeStr[timeLen] = '\0';
    str.prepend(timeStr);

    // Проверка положения прокрутки
    QScrollBar* vbar = verticalScrollBar();
    int scrollPos = vbar->value();
    bool isViewInBottom = (scrollPos == vbar->maximum());

    // Вывод в строке ввода, перенос ввода на новую строку
    QTextCursor cursor = textCursor();

    // Сохранение текущей строки ввода и позиции курсора (с выделением)
    int cursorPos = ( isCursorValid() ) ? cursor.columnNumber() : -1;
    int selection = cursor.selectionEnd() - cursor.selectionStart();
    if ( cursor.position() != cursor.selectionStart() ) selection = -selection;


    cursor.movePosition(QTextCursor::End);
    cursor.select(QTextCursor::LineUnderCursor);
    QString inStr = cursor.selectedText();

    cursor.select(QTextCursor::BlockUnderCursor);
    if (document()->lineCount() > 1) cursor.insertBlock();

    if (packet->type == IO_TYPE_IN)
    {
        cursor.insertText(str);
    }
    else
    {
        // Изменение (с восстановлением) формата строки для исходящих пакетов
        QTextCharFormat format = cursor.charFormat();
        format.setForeground(m_colorOut);
        cursor.insertText(str, format);

        format.setForeground(m_colorIn);
        setCurrentCharFormat(format);
    }

    // Восстановление поля ввода и позиции курсора
    appendPlainText(inStr);

    if (cursorPos >= 0)
    {
        cursor.movePosition(QTextCursor::End);
        cursor.movePosition(QTextCursor::StartOfLine);
        cursor.movePosition(QTextCursor::NextCharacter, QTextCursor::MoveAnchor, cursorPos + selection);

        if (selection > 0)      cursor.movePosition(QTextCursor::PreviousCharacter, QTextCursor::KeepAnchor, selection);
        else if (selection < 0) cursor.movePosition(QTextCursor::NextCharacter, QTextCursor::KeepAnchor, -selection);

        setTextCursor(cursor);
        // Восстановление позиции прокрутки после корректировки курсора
        if (!isViewInBottom)
        {
            vbar->setValue(scrollPos);
        }
    }

    // Корректировка проктутки экрана, если при добавлении данных она была нарушена
    if ( isViewInBottom && vbar->value() != vbar->maximum() )
    {
        vbar->triggerAction(QAbstractSlider::SliderToMaximum);
    }
}


// ============================================================================
///                     Вставка текста из буфера обмена
// ============================================================================
/// \param  ev  Дескриптор события
// ============================================================================

void WTermIO::paste (void)
{
    QClipboard* clipboard = QGuiApplication::clipboard();
    QString str = clipboard->text();

    if ( str.isEmpty() ) return;

    QStringList list = str.split('\n', QString::SkipEmptyParts);

    // Отправка каждой вставленной строки, кроме последней
    for (int i = 0; i < list.size(); ++i)
    {
        QString line = list.at(i);
        if ( line.endsWith('\r') ) str.chop(1);

        if ( i == list.size() - 1 && !str.endsWith('\n') )
        {
            QTextCursor cursor = textCursor();
            cursor.insertText(line);
        }
        else
        {
            if (!i)
            {
                // Добавление введенного ранее текста к первой строке
                QTextCursor cursor = textCursor();
                line.prepend( cursor.block().text().mid(m_prompt.length()) );
            }

            doSend(line);
        }
    }

    // Прокрутка до строки ввода
    QScrollBar* vbar = verticalScrollBar();
    vbar->triggerAction(QAbstractSlider::SliderToMaximum);
}


// ============================================================================
///                     Обработка и отправка введенной строки
// ============================================================================
/// \param  str     Отправляемая строка
// ============================================================================

void WTermIO::doSend (QString& str)
{
    // Добавление в историю ввода
    historyCtrl(Qt::Key_Enter, &str);

    // Добавление окончания строки
    if ( !m_eol.isEmpty() ) str.append(m_eol);

    QByteArray data = str.toUtf8();
    emit SignalSendRequest( data.data(), data.size() );
}


// ============================================================================
///                     Обработчик события клавиатуры
// ============================================================================
/// \param  ev  Дескриптор события
// ============================================================================

void WTermIO::keyPressEvent (QKeyEvent* ev)
{
    int key = ev->key();
    Qt::KeyboardModifiers modifiers = ev->modifiers();

    // Ввод символов
    if (
        (modifiers == Qt::NoModifier || modifiers == Qt::ShiftModifier || modifiers == Qt::KeypadModifier) &&
        ( (key >= 0x20 && key <= 0x7e) || (key >= 0x0401 && key <= 0x402f) )
    ) {
        isCursorValid(true);
        QPlainTextEdit::keyPressEvent(ev);
    }
    // Отправка введенной строки
    else if (key == Qt::Key_Return || key == Qt::Key_Enter)
    {
        QTextCursor cursor = textCursor();
        QString str = cursor.block().text().mid( m_prompt.length() );
        doSend(str);

        // Удаление отправленной строки
        cursor.movePosition(QTextCursor::End);
        cursor.select(QTextCursor::LineUnderCursor);
        cursor.insertText(m_prompt);
    }
    // Удаление
    else if (
             (key == Qt::Key_Backspace || key == Qt::Key_Delete) &&
             modifiers == Qt::NoModifier
    ) {
        if ( isCursorValid() )
        {
            QTextCursor cursor = textCursor();

            if ( key == Qt::Key_Backspace && !cursor.hasSelection() )
            {
                // Сброс позиции в истории
                QString str = cursor.block().text().mid( m_prompt.length() );
                historyCtrl( (Qt::Key)key, &str );
                // Курсор должен остаться в поле ввода после стирания символа
                if ( cursor.positionInBlock() <= m_prompt.length() ) return;
            }

            QPlainTextEdit::keyPressEvent(ev);
        }
    }
    // Управление историей ввода
    else if (
             modifiers == Qt::NoModifier &&
             (key == Qt::Key_PageUp || key == Qt::Key_PageDown || key == Qt::Key_Up || key == Qt::Key_Down)
    ) {
        isCursorValid(true);

        // Сброс выделения
        QTextCursor cursor = textCursor();
        if ( cursor.hasSelection() )
        {
            cursor.clearSelection();
        }

        int currentPosition = cursor.position();
        int startOfLine = cursor.block().position() + m_prompt.length();
        cursor.setPosition(startOfLine, QTextCursor::KeepAnchor);

        QString str = cursor.selectedText();
        historyCtrl( (Qt::Key)key, &str );

        cursor.clearSelection();
        cursor.movePosition(QTextCursor::End, QTextCursor::KeepAnchor);
        cursor.insertText(str);

        if (key == Qt::Key_PageUp || key == Qt::Key_PageDown)
        {
            cursor.setPosition(currentPosition, QTextCursor::MoveAnchor);
        }

        setTextCursor(cursor);
    }
    // Прокрутка экрана
    else if (
             modifiers == Qt::ShiftModifier &&
             (key == Qt::Key_PageUp || key == Qt::Key_PageDown || key == Qt::Key_Up || key == Qt::Key_Down)
    ) {
        QScrollBar* vbar = verticalScrollBar();
        QScrollBar::SliderAction action = QScrollBar::SliderNoAction;

        switch (key)
        {
            case Qt::Key_PageUp:    action = QAbstractSlider::SliderPageStepSub; break;
            case Qt::Key_PageDown:  action = QAbstractSlider::SliderPageStepAdd; break;
            case Qt::Key_Up:        action = QAbstractSlider::SliderSingleStepSub; break;
            case Qt::Key_Down:      action = QAbstractSlider::SliderSingleStepAdd; break;
        }

        vbar->triggerAction(action);
    }
    // Переход в начало / в конец
    else if ( (key == Qt::Key_Home || key == Qt::Key_End) && modifiers == Qt::ControlModifier)
    {
        QScrollBar* vbar = verticalScrollBar();
        vbar->triggerAction( (key == Qt::Key_Home) ? QAbstractSlider::SliderToMinimum : QAbstractSlider::SliderToMaximum);
    }
    // Очистка экрана
    else if (key == Qt::Key_L && modifiers == Qt::ControlModifier)
    {
        // Сохранение поля ввода и позиции курсора в нем
        QTextCursor cursor = textCursor();
        int column = ( isCursorValid() ) ? cursor.columnNumber() : -1;

        cursor.movePosition(QTextCursor::End);
        cursor.select(QTextCursor::LineUnderCursor);
        QString inStr = cursor.selectedText();

        document()->clear();

        // Восстановление поля ввода и позиции курсора
        appendPlainText(inStr);

        if (column >= 0)
        {
            cursor.movePosition(QTextCursor::StartOfLine);
            cursor.movePosition(QTextCursor::NextCharacter, QTextCursor::MoveAnchor, column);
            setTextCursor(cursor);
        }
    }
    // Выделение всего текста
    else if (modifiers == Qt::ControlModifier && key == Qt::Key_A)
    {
        QPlainTextEdit::keyPressEvent(ev);
    }
    // Копирование
    else if ( ev->matches(QKeySequence::Copy) )
    {
        QPlainTextEdit::keyPressEvent(ev);
    }
    // Вставка
    else if ( ev->matches(QKeySequence::Paste) )
    {
        isCursorValid(true);
        paste();
    }
    // Перемещенеи курсора в строке ввода (в т.ч. управление выделением)
    else if (
             ev->matches(QKeySequence::MoveToNextChar) ||
             ev->matches(QKeySequence::MoveToPreviousChar) ||
             ev->matches(QKeySequence::MoveToNextWord) ||
             ev->matches(QKeySequence::MoveToPreviousWord) ||
             ev->matches(QKeySequence::MoveToStartOfLine) ||
             ev->matches(QKeySequence::MoveToEndOfLine) ||
             ev->matches(QKeySequence::SelectNextChar) ||
             ev->matches(QKeySequence::SelectPreviousChar) ||
             ev->matches(QKeySequence::SelectNextWord) ||
             ev->matches(QKeySequence::SelectPreviousWord) ||
             ev->matches(QKeySequence::SelectStartOfLine) ||
             ev->matches(QKeySequence::SelectEndOfLine)
    ) {
        if ( isCursorValid() )
        {
            QPlainTextEdit::keyPressEvent(ev);

            if ( !isCursorValid() )
            {
                QTextCursor cursor = textCursor();
                int startPos = cursor.selectionStart();
                QTextBlock block = document()->findBlock(startPos);
                int basePosition = block.position();
                int pos = basePosition + m_prompt.length();

                cursor.setPosition(pos, (cursor.hasSelection()) ? QTextCursor::KeepAnchor : QTextCursor::MoveAnchor);
                setTextCursor(cursor);
            }
        }
    }
}


// ============================================================================
///                     Проверка положения курсора
// ============================================================================
/// \param  move    Флаг необходимости перемещения курсора при ошибке
/// \return         true - положение валидное (можно редактировать текст),
///                 false - невалидное положение
// ============================================================================

bool WTermIO::isCursorValid (bool move)
{
    QTextCursor cursor = textCursor();
    int lineCount = document()->lineCount();
    bool isValid = true;

    // Проверка строки курсора (должна быть последняя) и положения курсора в строке
    if ( cursor.hasSelection() )
    {
        // Начало выделения
        int startPos = cursor.selectionStart();
        QTextBlock block = document()->findBlock(startPos);
        int startLine = block.firstLineNumber();
        int basePosition = block.position();

        // Конец выделения
        int endPos = cursor.selectionEnd();
        block = document()->findBlock(endPos);
        int endLine = block.firstLineNumber() + block.lineCount() - 1;

        isValid = ( startLine == endLine && endLine == (lineCount - 1) );

        if (isValid)
        {
            int selectionOffset = startPos - basePosition;
            isValid = ( selectionOffset >= m_prompt.length() );
        }
    }
    else
    {
        int pos = cursor.block().firstLineNumber();
        isValid = ( (lineCount - 1) == pos );

        if (isValid)
        {
            isValid = ( cursor.positionInBlock() >= m_prompt.length() );
        }
    }

    // Перемещение курсора в конец строки ввода при ошибке
    if (move && !isValid)
    {
        cursor.clearSelection();
        cursor.movePosition(QTextCursor::End);
        setTextCursor(cursor);
    }

    return isValid;
}


// ============================================================================
///                     Обработчик события мыши
// ============================================================================
/// \param  ev  Дескриптор события
// ============================================================================

void WTermIO::mouseReleaseEvent (QMouseEvent *ev)
{
    if ( ev->button() == Qt::LeftButton && textCursor().hasSelection() ) return;

    // Возврат курсора в строку ввода
    setFocus();
    QTextCursor cursor = textCursor();
    cursor.movePosition(QTextCursor::End);
    setTextCursor(cursor);
}


// ============================================================================
///                     Обработчик события нажатия ПКМ
// ============================================================================
/// \param  ev  Дескриптор события
// ============================================================================

void WTermIO::contextMenuEvent (QContextMenuEvent* ev)
{
    Q_UNUSED(ev);
    // Создать свое контекстное меню
}


// ============================================================================
///                     Обработчик события drag and drop
// ============================================================================
/// \param  ev  Дескриптор события
// ============================================================================

void WTermIO::dragEnterEvent (QDragEnterEvent* ev)
{
    // Блокировка drag and drop
    Q_UNUSED(ev);
}


// ============================================================================
///                     Управление историей ввода
// ============================================================================
/// \param  key     Нажатая кнопка
/// \param  pStr    Строка ввода с текущим значением (доступна для модификации)
// ============================================================================

void WTermIO::historyCtrl (Qt::Key key, QString* pStr)
{
    if (!pStr) return;

    switch (key)
    {
        case Qt::Key_Enter:
        case Qt::Key_Return:
            if ( !pStr->isEmpty() )
            {
                // Добавление строки в историю с удалением всех дубликатов
                m_history.removeAll(*pStr);
                m_history.prepend(*pStr);
                m_histIndex = -1;
            }
        return;

        case Qt::Key_Up:
            if (m_histIndex < m_history.size() - 1)
            {
                ++m_histIndex;
            }
        break;

        case Qt::Key_Down:
            if (m_histIndex >= 0)
            {
                --m_histIndex;
            }
        break;

        case Qt::Key_PageUp:
        case Qt::Key_PageDown:
            if ( !pStr->isEmpty() )
            {
                // PageUp - поиск от текущей до самой старой записи
                // PageDown - поиск от текущей до самой новой
                bool goUp = (key == Qt::Key_PageUp);
                int index = (goUp) ? m_histIndex + 1 : m_histIndex - 1;

                for (; index >= 0 && index < m_history.size(); (goUp) ? ++index : --index)
                {
                    const QString item = m_history.at(index);
                    if ( !item.startsWith(*pStr) ) continue;

                    m_histIndex = index;
                    break;
                }
            }
        break;

        // Сброс индекса и выход
        default: m_histIndex = -1; return;
    }

    if ( m_histIndex >= 0 && m_histIndex < m_history.size() )
    {
        *pStr = m_history.at(m_histIndex);
    }
    else
    {
        // Сброс индекса и обнуление строки ввода
        m_histIndex = -1;
        pStr->truncate(0);
    }
}
