//*****************************************************************************
//
// Имя файла    : 'wpanel.cpp'
// Описание     : Виджет панели
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************

#include "ui/widgets/wpanel.h"
#include "factory.h"
#include <QApplication>
#include <QVBoxLayout>
#include <QMenu>
#include <QToolButton>
#include <QAction>


// ============================================================================
///                             Конструктор
// ============================================================================
/// \param  parent  Родительский виджет
// ============================================================================

WPanel::WPanel (QWidget* parent) :
    QWidget(parent)
{

#warning "FIXME"
// расположение панели из настроек

    setMinimumWidth(28);
    setMaximumWidth(28);
    setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Minimum);

    QVBoxLayout* layout = new QVBoxLayout(this);
    // Справа / слева
//    layout->setContentsMargins(3, 1, 1, 1);
    layout->setContentsMargins(1, 1, 3, 1);
    layout->setSpacing(5);
    layout->setAlignment(Qt::AlignTop);

    QToolButton* btn;

    // Меню
    CGuiEvent ev(EV_BUILD_MENU);
    QMenu* pMenu = NULL;
    ev.SetParam(&pMenu);

    CGui* pGui = CFactory::GetGui();
    QApplication::sendEvent(pGui, &ev);

    if (pMenu)
    {
        btn = new QToolButton(this);
        btn->setMenu(pMenu);
        btn->setPopupMode(QToolButton::InstantPopup);
        btn->setIcon( QIcon(":/res/tb_menu.png") );
        btn->setToolTip( tr("Settings") );
        btn->setAutoRaise(true);
        btn->setStyleSheet("QToolButton::menu-indicator { width: 0px; }");

        layout->addWidget(btn);
    }

    CSettings* pSettings = CFactory::GetSettings();
    QHash<QString, QVariant> params;
    QString groupName = SETTINGS_GROUP_BASE_NAME;
    pSettings->Get(&groupName, &params);
    QString tip;

    // IO settings
    btn = new QToolButton(this);
    btn->setObjectName("btnIo");
    btn->setIcon( QIcon(":/res/tb_io.png") );
    tip = tr("IO settings");
    tip.append(" (" + params.value(SETTINGS_SHORTCUT_PARAM_PREFIX "iosettings").toString() +")");
    btn->setToolTip(tip);
    btn->setAutoRaise(true);

    connect(btn, &QToolButton::clicked, this, &WPanel::onIoSettings);
    layout->addWidget(btn);

    // Open / close
    btn = new QToolButton(this);
    btn->setObjectName("btnOpenClose");
    btn->setCheckable(true);
    btn->setIcon( QIcon(":/res/tb_disconnect.png") );
    tip = tr("Open / Close");
    tip.append(" (" + params.value(SETTINGS_SHORTCUT_PARAM_PREFIX "ioopenclose").toString() +")");
    btn->setToolTip(tip);
    btn->setAutoRaise(true);

    connect(btn, &QToolButton::toggled, this, &WPanel::onOpenClose);
    layout->addWidget(btn);

    setLayout(layout);

    connect(pSettings, &CSettings::SigSettingsChanged, this, &WPanel::onSettingsChanged);
    updateShortcuts();
}


// ============================================================================
///             Обработчик нажатия на кнопку IO settings
// ============================================================================
/// \param  checked     Состояние кнопки
// ============================================================================

void WPanel::onIoSettings (bool checked)
{
    Q_UNUSED(checked);

    CGuiEvent* pEvent = new CGuiEvent(EV_SHOW_IO_SETTINGS);
    CGui* pGui = CFactory::GetGui();
    QApplication::postEvent(pGui, pEvent);
}


// ============================================================================
///             Обработчик нажатия на кнопку Open / close
// ============================================================================
/// \param  checked     Состояние кнопки
// ============================================================================

void WPanel::onOpenClose (bool checked)
{
    CGui* pGui = CFactory::GetGui();
    CIoSess* sess = pGui->GetActiveSession();
    CDriver* drv = sess->GetDriver();
    QToolButton* btn = findChild<QToolButton*>("btnOpenClose");

    do
    {
        if (!drv) break;

        if (!checked) break;

        int result = drv->Open();
        if (result < 0)
        {
            CGuiEvent* pEvent = new CGuiEvent(EV_SHOW_IO_DRV_OPEN_ERROR);
            CGui* pGui = CFactory::GetGui();
            QApplication::postEvent(pGui, pEvent);
            break;
        }

        btn->setIcon( QIcon(":/res/tb_connect.png") );
        return;
    } while (0);

    drv->Close();

    if (checked)
    {
        btn->setChecked(false);
    }

    btn->setIcon( QIcon(":/res/tb_disconnect.png") );
}


// ============================================================================
///                     Обработчик изменения настроек
// ============================================================================
/// \param  group   Сессия
/// \param  params  Отправленный пакет
// ============================================================================

void WPanel::onSettingsChanged (const QString* group, QHash<QString, QVariant>* params)
{
    Q_UNUSED(params);

    if (*group == SETTINGS_GROUP_BASE_NAME)
    {
        updateShortcuts();
    }
}


// ============================================================================
///
///                     Обновление shortcut'ов
///
// ============================================================================

void WPanel::updateShortcuts (void)
{
    CSettings* pSettings = CFactory::GetSettings();
    QString groupName(SETTINGS_GROUP_BASE_NAME);
    QHash<QString, QVariant> params;
    int result = pSettings->Get(&groupName, &params);
    if (result < 0) return;

    QToolButton* btn = findChild<QToolButton*>("btnIo");
    btn->setShortcut( QKeySequence(params.value(SETTINGS_SHORTCUT_PARAM_PREFIX "iosettings").toString()) );

    btn = findChild<QToolButton*>("btnOpenClose");
    btn->setShortcut( QKeySequence(params.value(SETTINGS_SHORTCUT_PARAM_PREFIX "ioopenclose").toString()) );
}
