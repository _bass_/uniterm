//*****************************************************************************
//
// Имя файла    : 'wabout.cpp'
// Описание     : Окно "О приложении"
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************

#include "ui/widgets/wabout.h"
#include "ui_about.h"
#include "defines.h"
#include <QDate>


// ============================================================================
///                             Конструктор
// ============================================================================
/// \param  parent  Родительский виджет
// ============================================================================

WAbout::WAbout (QWidget *parent) :
    QDialog(parent),
    ui(new Ui::About)
{
    ui->setupUi(this);
    setWindowFlags(Qt::Dialog | Qt::CustomizeWindowHint | Qt::WindowTitleHint | Qt::WindowCloseButtonHint);

    ui->appVersion->setText(APP_VERSION);

    QDate buildDate = QLocale("en_US").toDate( QString(__DATE__).simplified(), "MMM dd yyyy");
    if ( buildDate.isValid() )
    {
        ui->buildDate->setText( buildDate.toString("dd.MM.yyyy") );
    }
}


// ============================================================================
///
///                             Деструктор
///
// ============================================================================

WAbout::~WAbout ()
{
    delete ui;
}
