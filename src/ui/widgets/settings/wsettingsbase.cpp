//*****************************************************************************
//
// Имя файла    : 'wsettingsbase.cpp'
// Описание     : Группа настроек Base
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************

#include "ui/widgets/settings/wsettingsbase.h"
#include "ui_settings_base.h"
#include "settings.h"
#include "defines.h"
#include <QColorDialog>


// ============================================================================
//                              Локальные переменные
// ============================================================================

    SETTINGS_GROUP_REGISTER(WSettingsBase);


// ============================================================================
///                             Конструктор
// ============================================================================
/// \param  parent  Родительский виджет
// ============================================================================

WSettingsBase::WSettingsBase () :
    ISettingsGroup(),
    ui(new Ui::WSettingsBase)
{
}


// ============================================================================
///                     Чтение внутреннего имени группы
// ============================================================================
/// \return     Название группы настроек
// ============================================================================

QString WSettingsBase::GetName (void)
{
    return QString(SETTINGS_GROUP_BASE_NAME);
}


// ============================================================================
///                         Чтение заголовка группы
// ============================================================================
/// \return     Название группы настроек
// ============================================================================

QString WSettingsBase::GetTitle (void)
{
    return ISettingsGroup::tr("Base");
}


// ============================================================================
///                         Формирование подменю
// ============================================================================
/// \param  parentMenu  Родительское меню
// ============================================================================

void WSettingsBase::BuildSubTree (QStandardItem* parentMenu)
{
    Q_UNUSED(parentMenu);
}


// ============================================================================
///                         Отрисовка настроек группы
// ============================================================================
/// \param  activeIndex Выбранный элемент дерева меню
/// \param  pWidget     Виджет для формирования настроек
/// \param  pSettings   Актуальные настройки
// ============================================================================

void WSettingsBase::SettingsDraw (const QModelIndex* activeIndex, QWidget* pWidget, QHash<QString, QVariant>* pSettings)
{
    Q_UNUSED(activeIndex);

    ui->setupUi(pWidget);

    // Установка актуальных значений
    QComboBox* wFont = pWidget->findChild<QComboBox*>("fontName");
    wFont->setCurrentText( pSettings->value("font", CFG_GEN_DEFAULT_FONT_FAMILY).toString() );

    QSpinBox* wFontSize = pWidget->findChild<QSpinBox*>("fontSize");
    wFontSize->setValue( pSettings->value("fontSize", CFG_GEN_DEFAULT_FONT_SIZE).toUInt() );

    QWidget* wColor = pWidget->findChild<QWidget*>("colorBg");
    wColor->setAutoFillBackground(true);
    {
        QVariant val = pSettings->value("colorBg");
        QColor color = ( val.isValid() && !val.isNull() ) ? val.value<QColor>() : CFG_GEN_DEFAULT_COLOR_BG;
        QPalette p;
        p.setColor(QPalette::Window, color);
        wColor->setPalette(p);
    }

    wColor = pWidget->findChild<QWidget*>("colorIn");
    wColor->setAutoFillBackground(true);
    {
        QVariant val = pSettings->value("colorIn");
        QColor color = ( val.isValid() && !val.isNull() ) ? val.value<QColor>() : CFG_GEN_DEFAULT_COLOR_TEXT_IN;
        QPalette p;
        p.setColor(QPalette::Window, color);
        wColor->setPalette(p);
    }

    wColor = pWidget->findChild<QWidget*>("colorOut");
    wColor->setAutoFillBackground(true);
    {
        QVariant val = pSettings->value("colorOut");
        QColor color = ( val.isValid() && !val.isNull() ) ? val.value<QColor>() : CFG_GEN_DEFAULT_COLOR_TEXT_OUT;
        QPalette p;
        p.setColor(QPalette::Window, color);
        wColor->setPalette(p);
    }

    QCheckBox* pCheckBox = pWidget->findChild<QCheckBox*>("stayOnTop");
    bool state = pSettings->value("stayOnTop").toBool();
    pCheckBox->setCheckState( (state) ? Qt::Checked : Qt::Unchecked);

    // Обработчики нажатия кнопок
    QToolButton* wBtn = pWidget->findChild<QToolButton*>("colorBgBtn");
    connect(wBtn, &QToolButton::clicked, this, &WSettingsBase::onBtnSelectColor, Qt::UniqueConnection);

    wBtn = pWidget->findChild<QToolButton*>("colorInBtn");
    connect(wBtn, &QToolButton::clicked, this, &WSettingsBase::onBtnSelectColor, Qt::UniqueConnection);

    wBtn = pWidget->findChild<QToolButton*>("colorOutBtn");
    connect(wBtn, &QToolButton::clicked, this, &WSettingsBase::onBtnSelectColor, Qt::UniqueConnection);
}


// ============================================================================
///                         Сохранение настроек группы
// ============================================================================
/// \param  activeIndex Выбранный элемент дерева меню
/// \param  pWidget     Виджет для формирования настроек
/// \param  pSettings   Указатель на настройки для сохранения
// ============================================================================

void WSettingsBase::SettingsParse (const QModelIndex* activeIndex, QWidget* pWidget, QHash<QString, QVariant>* pSettings)
{
    Q_UNUSED(activeIndex);

    QFontComboBox* wFont = pWidget->findChild<QFontComboBox*>("fontName");
    pSettings->insert("font", wFont->currentFont().family());

    QSpinBox* wFontSize = pWidget->findChild<QSpinBox*>("fontSize");
    pSettings->insert("fontSize", wFontSize->value());

    QWidget* wColor = pWidget->findChild<QWidget*>("colorBg");
    QPalette p = wColor->palette();
    QColor color = p.color(QPalette::Window);
    if ( !color.isValid() ) color = CFG_GEN_DEFAULT_COLOR_BG;
    pSettings->insert("colorBg", color);

    wColor = pWidget->findChild<QWidget*>("colorIn");
    p = wColor->palette();
    color = p.color(QPalette::Window);
    if ( !color.isValid() ) color = CFG_GEN_DEFAULT_COLOR_TEXT_IN;
    pSettings->insert("colorIn", color);

    wColor = pWidget->findChild<QWidget*>("colorOut");
    p = wColor->palette();
    color = p.color(QPalette::Window);
    if ( !color.isValid() ) color = CFG_GEN_DEFAULT_COLOR_TEXT_OUT;
    pSettings->insert("colorOut", color);

    QCheckBox* pCheckBox = pWidget->findChild<QCheckBox*>("stayOnTop");
    bool state = (pCheckBox->checkState() == Qt::Checked);
    pSettings->insert("stayOnTop", state);
}


// ============================================================================
///                 Обработчик нажатия кнопки выбора цвета
// ============================================================================
/// \param  checked     Флаг фиксации кнопки
// ============================================================================

void WSettingsBase::onBtnSelectColor (bool checked)
{
    Q_UNUSED(checked);

    QObject* obj = sender();
    QString btnName = obj->objectName();
    QWidget* target;

    if (btnName == "colorBgBtn")        target = ui->colorBg;
    else if (btnName == "colorInBtn")   target = ui->colorIn;
    else if (btnName == "colorOutBtn")  target = ui->colorOut;
    else                                return;

    QPalette p = target->palette();
    QColor color = p.color(QPalette::Window);
    color = QColorDialog::getColor(color, target);

    if ( color.isValid() )
    {
        p.setColor(QPalette::Window, color);
        target->setPalette(p);
    }
}
