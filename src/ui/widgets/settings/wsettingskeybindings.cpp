//*****************************************************************************
//
// Имя файла    : 'wsettingskeybindings.cpp'
// Описание     : Группа настроек Key bindings
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************

#include "ui/widgets/settings/wsettingskeybindings.h"
#include "ui_settings_keybindings.h"


// ============================================================================
//                                  Типы
// ============================================================================

    // Дескриптор привязки
    typedef struct
    {
        QString     group;
        QString     param;
        QString     shortcut;
        QString     desc;
    } TShortcutDesc;


// ============================================================================
//                          Локальные переменные
// ============================================================================

    SETTINGS_GROUP_REGISTER(WSettingsKeyBindings);

    // Названия параметров горячих клавиш (контекст - QObject)
    static const char* hiddenTranslations[] =
    {
        QT_TRANSLATE_NOOP("QObject", "base/shortcut.newtab"),
        QT_TRANSLATE_NOOP("QObject", "base/shortcut.closetab"),
        QT_TRANSLATE_NOOP("QObject", "base/shortcut.ioopenclose"),
        QT_TRANSLATE_NOOP("QObject", "base/shortcut.iosettings")
    };


// ============================================================================
///                             Конструктор
// ============================================================================
/// \param  parent  Родительский виджет
// ============================================================================

WSettingsKeyBindings::WSettingsKeyBindings () :
    ISettingsGroup(),
    ui(new Ui::WSettingsKeyBindings)
{
    Q_UNUSED(hiddenTranslations);
}


// ============================================================================
///                   Чтение внутреннего имени группы
// ============================================================================
/// \return     Название группы настроек
// ============================================================================

QString WSettingsKeyBindings::GetName (void)
{
    return QString(SETTINGS_GROUP_KEYBIND_NAME);
}


// ============================================================================
///                         Чтение заголовка группы
// ============================================================================
/// \return     Название группы настроек
// ============================================================================

QString WSettingsKeyBindings::GetTitle (void)
{
    return ISettingsGroup::tr("Keybindings");
}


// ============================================================================
///                         Формирование подменю
// ============================================================================
/// \param  parentMenu  Родительское меню
// ============================================================================

void WSettingsKeyBindings::BuildSubTree (QStandardItem* parentMenu)
{
    Q_UNUSED(parentMenu);
}


// ============================================================================
///                         Отрисовка настроек группы
// ============================================================================
/// \param  activeIndex Выбранный элемент дерева меню
/// \param  pWidget     Виджет для формирования настроек
/// \param  pSettings   Актуальные настройки
// ============================================================================

void WSettingsKeyBindings::SettingsDraw (const QModelIndex* activeIndex, QWidget* pWidget, QHash<QString, QVariant>* pSettings)
{
    Q_UNUSED(activeIndex);

    ui->setupUi(pWidget);

    QTableWidget* pTable = pWidget->findChild<QTableWidget*>();
    fillShortcutTable(pTable, pSettings);

    connect(pTable, &QTableWidget::itemClicked, this, &WSettingsKeyBindings::onItemSelect);

    QLineEdit* pInput = pWidget->findChild<QLineEdit*>();
    connect(pInput, &QLineEdit::textChanged, this, &WSettingsKeyBindings::onShortcutSave);
}


// ============================================================================
///                         Сохранение настроек группы
// ============================================================================
/// \param  activeIndex Выбранный элемент дерева меню
/// \param  pWidget     Виджет для формирования настроек
/// \param  pSettings   Указатель на настройки для сохранения
// ============================================================================

void WSettingsKeyBindings::SettingsParse (const QModelIndex* activeIndex, QWidget* pWidget, QHash<QString, QVariant>* pSettings)
{
    Q_UNUSED(activeIndex);

    QTableWidget* pTable = pWidget->findChild<QTableWidget*>();
    for (int row = 0; row < pTable->rowCount(); ++row)
    {
        QTableWidgetItem* item = pTable->item(row, 1);
        if (!item) continue;

        QString param = item->data(Qt::UserRole).toString();
        QString value = item->text();

        pSettings->insert(param, value);
    }
}


// ============================================================================
///                         Заполнение таблицы привязок
// ============================================================================
/// \param  pTable  Таблица
// ============================================================================

void WSettingsKeyBindings::fillShortcutTable (QTableWidget* pTable, QHash<QString, QVariant>* pSettings)
{
    // Настрока высоты
    QHeaderView* pHdr = pTable->verticalHeader();
    pHdr->setDefaultSectionSize( pHdr->minimumSectionSize() );

    // Настройка ширины
    pHdr = pTable->horizontalHeader();
    pHdr->setVisible(false);
    pHdr->resizeSection(1, 100);
    pHdr->setSectionResizeMode(0, QHeaderView::Stretch);
    pHdr->setSectionResizeMode(1, QHeaderView::Fixed);

    // Заполнение таблицы
    const QList<QString> keys = pSettings->keys();
    int row = 0;

    foreach (QString key, keys)
    {
        QString shortcut = pSettings->value(key).toString();
        if ( shortcut.isEmpty() ) continue;

        pTable->insertRow(row);

        QString desc = QObject::tr( qPrintable(key) );
        pTable->setItem(row, 0, new QTableWidgetItem(desc));
        // Сохранение названия параметра в ячейке с shortcut'ом
        QTableWidgetItem* item = new QTableWidgetItem( shortcut.simplified() );
        item->setData(Qt::UserRole, key);
        pTable->setItem(row, 1, item);
        ++row;
    }

    pTable->sortItems(0);
}


// ============================================================================
///                     Обработка выбора ячейки таблицы
// ============================================================================
/// \param  item    Выбранный элемент
// ============================================================================

void WSettingsKeyBindings::onItemSelect (QTableWidgetItem* item)
{
    // Поиск виджета LineEdit
    QTableWidget* pTable = item->tableWidget();
    QLineEdit* pInput = ui->shortcut;

    // Проверка изменения строки
    bool isValid;
    int row = pInput->property("row").toInt(&isValid);
    if ( isValid && row >= 0 && row == item->row() ) return;

    // Вывод сочетания клавиш выбранной строки
    if (item->column() != 1)
    {
        item = pTable->item(item->row(), 1);
    }

    pInput->setText( item->text() );
    // Сохранение номера выбранной строки
    pInput->setProperty("row", item->row());
}


// ============================================================================
///                     Сохранение измененного shortcut'а
// ============================================================================
/// \param  text    Текст сочетания клавиш
// ============================================================================

void WSettingsKeyBindings::onShortcutSave (const QString& text)
{
    QTableWidget* pTable = ui->table;
    QLineEdit* pInput = ui->shortcut;

    bool isValid;
    int row = pInput->property("row").toInt(&isValid);
    if (!isValid || row < 0) return;

    QTableWidgetItem* item = pTable->item(row, 1);
    if (!item) return;

    item->setText( text.simplified() );
}
