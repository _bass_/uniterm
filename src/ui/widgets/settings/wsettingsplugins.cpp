//*****************************************************************************
//
// Имя файла    : 'wsettingsplugins.cpp'
// Описание     : Группа настроек Plugins
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************

#include "ui/widgets/settings/wsettingsplugins.h"
#include <QVBoxLayout>
#include <QTableWidget>
#include <QHeaderView>


// ============================================================================
//                              Локальные переменные
// ============================================================================

    SETTINGS_GROUP_REGISTER(WSettingsPlugins);


// ============================================================================
///                             Конструктор
// ============================================================================
/// \param  parent  Родительский виджет
// ============================================================================

WSettingsPlugins::WSettingsPlugins () :
    ISettingsGroup()
{

}


// ============================================================================
///                     Чтение внутреннего имени группы
// ============================================================================
/// \return     Название группы настроек
// ============================================================================

QString WSettingsPlugins::GetName (void)
{
    return QString(SETTINGS_GROUP_PLUGINS_NAME);
}


// ============================================================================
///                     Чтение заголовка группы
// ============================================================================
/// \return     Название группы настроек
// ============================================================================

QString WSettingsPlugins::GetTitle (void)
{
    return ISettingsGroup::tr("Plugins");
}


// ============================================================================
///                         Формирование подменю
// ============================================================================
/// \param  parentMenu  Родительское меню
// ============================================================================

void WSettingsPlugins::BuildSubTree (QStandardItem* parentMenu)
{
    CPluginMgr* manager = CFactory::GetPluginMgr();
    QStringList plgList = manager->GetPlugins();

    foreach (QString plgName, plgList)
    {
        CPlugin* plg = manager->GetPlugin(plgName);
        QStandardItem* subItem = new QStandardItem( plg->GetTitle() );
        QString name = plg->GetName();
        name = name.toLower().prepend("plg_");

        subItem->setData(parentMenu->data(STYPE_GROUP_OBJ), STYPE_GROUP_OBJ);
        subItem->setData(name, STYPE_SETTINGS_GROUP_NAME);

        parentMenu->appendRow(subItem);
    }
}


// ============================================================================
///                         Отрисовка настроек группы
// ============================================================================
/// \param  activeIndex Выбранный элемент дерева меню
/// \param  pWidget     Виджет для формирования настроек
/// \param  pSettings   Актуальные настройки
// ============================================================================

void WSettingsPlugins::SettingsDraw (const QModelIndex* activeIndex, QWidget* pWidget, QHash<QString, QVariant>* pSettings)
{
    if ( !activeIndex || !activeIndex->isValid() ) return;

    QString name = activeIndex->data(STYPE_SETTINGS_GROUP_NAME).toString();
    if ( name.isEmpty() ) return;

    if ( name == GetName() )
    {
        // Формирование списка плагинов
        drawPluginsList(pWidget, pSettings);
    }
    else if ( name.startsWith("plg_") )
    {
        // Вывод настроек плагина
        name = name.mid(sizeof("plg_") - 1);
        drawPluginSettings(name, pWidget, pSettings);
    }
}


// ============================================================================
///                         Сохранение настроек группы
// ============================================================================
/// \param  activeIndex Выбранный элемент дерева меню
/// \param  pWidget     Виджет для формирования настроек
/// \param  pSettings   Указатель на настройки для сохранения
// ============================================================================

void WSettingsPlugins::SettingsParse (const QModelIndex* activeIndex, QWidget* pWidget, QHash<QString, QVariant>* pSettings)
{
    if ( !activeIndex || !activeIndex->isValid() ) return;

    QString name = activeIndex->data(STYPE_SETTINGS_GROUP_NAME).toString();
    if ( name.isEmpty() ) return;

    if ( name == GetName() )
    {
        // Сохранение состояния активации плагинов
        QTableWidget* pTable = pWidget->findChild<QTableWidget*>();
        if (!pTable) return;

        for (int row = 0; row < pTable->rowCount(); ++row)
        {
            QTableWidgetItem* chkBox = pTable->item(row, 0);
            if (!chkBox) continue;

            QString plgName = chkBox->data(Qt::UserRole).toString();
            if ( plgName.isEmpty() ) continue;

            bool state = (chkBox->checkState() == Qt::Checked);
            pSettings->insert(plgName, state);
        }
    }
    else if ( name.startsWith("plg_") )
    {
        // Сохранение параметров плагина
        name = name.mid(sizeof("plg_") - 1);

        CPluginMgr* manager = CFactory::GetPluginMgr();
        if (!manager) return;

        CPlugin* plg = manager->GetPlugin(name);
        if (!plg) return;

        plg->ParseSettings(pWidget, pSettings);
    }
}


// ============================================================================
///                     Формирование виджета списка плагинов
// ============================================================================
/// \param  pWidget     Виджет для формирования настроек
/// \param  pSettings   Указатель на настройки для сохранения
// ============================================================================

void WSettingsPlugins::drawPluginsList (QWidget* pWidget, QHash<QString, QVariant>* pSettings)
{
    CPluginMgr* manager = CFactory::GetPluginMgr();

    QVBoxLayout* pLayout = new QVBoxLayout();
    pLayout->setContentsMargins(0, 0, 0, 0);
    pLayout->setSizeConstraint(QLayout::SetMaximumSize);
    pWidget->setLayout(pLayout);

    QTableWidget* pTable = new QTableWidget(pWidget);
    pTable->setColumnCount(4);
    pTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
    pTable->setTabKeyNavigation(false);
    pTable->setProperty("showDropIndicator", QVariant(false));
    pTable->setAlternatingRowColors(true);
    pTable->setSelectionMode(QAbstractItemView::NoSelection);
    pTable->setShowGrid(false);
    pTable->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    // Настрока высоты
    QHeaderView* pHdr = pTable->verticalHeader();
    pHdr->setVisible(false);
    pHdr->setDefaultSectionSize( pHdr->minimumSectionSize() );

    // Настройка ширины
    pHdr = pTable->horizontalHeader();
    pHdr->setVisible(false);
    pHdr->resizeSection(0, 20);
    pHdr->resizeSection(1, 120);
    pHdr->resizeSection(2, 50);
    pHdr->setSectionResizeMode(0, QHeaderView::Fixed);
    pHdr->setSectionResizeMode(1, QHeaderView::Fixed);
    pHdr->setSectionResizeMode(2, QHeaderView::Fixed);
    pHdr->setSectionResizeMode(3, QHeaderView::Stretch);

    // Список плагинов
    QStringList plgList = manager->GetPlugins(false);
    TPlgInfo info;

    for (int row = 0; row < plgList.count(); ++row)
    {
        int result = manager->GetPluginInfo(plgList.at(row), &info);
        if (result < 0) continue;

        pTable->insertRow(row);
        // Чекбокс
        QTableWidgetItem* chkBox = new QTableWidgetItem(Qt::CheckStateRole);
        bool state = pSettings->value(info.name, QVariant(true)).toBool();

        chkBox->setCheckState( (state) ? Qt::Checked : Qt::Unchecked );
        chkBox->setData(Qt::UserRole, info.name);

        pTable->setItem(row, 0, chkBox);
        pTable->setItem(row, 1, new QTableWidgetItem(info.title));
        pTable->setItem(row, 2, new QTableWidgetItem(info.version));
        pTable->setItem(row, 3, new QTableWidgetItem(info.desc));
    }

    pLayout->addWidget(pTable);
    pTable->show();
}


// ============================================================================
///                         Сохранение настроек группы
// ============================================================================
/// \param  plgName     Имя плагина
/// \param  pWidget     Виджет для формирования настроек
/// \param  pSettings   Указатель на настройки для сохранения
// ============================================================================

void WSettingsPlugins::drawPluginSettings (QString& plgName, QWidget* pWidget, QHash<QString, QVariant>* pSettings)
{
    CPluginMgr* manager = CFactory::GetPluginMgr();
    if (!manager) return;
    CPlugin* plg = manager->GetPlugin(plgName);
    if (!plg) return;

    // Удаление компановщика
    QLayout* currLayout = pWidget->layout();
    if (currLayout) delete currLayout;

    plg->DrawSettings(pWidget, pSettings);
}
