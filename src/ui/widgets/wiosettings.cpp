//*****************************************************************************
//
// Имя файла    : 'wiosettings.cpp'
// Описание     : Окно настроек IO
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************

#include "ui/widgets/wiosettings.h"
#include "ui_wiosettings.h"
#include "factory.h"
#include "defines.h"
#include <QDebug>


// ============================================================================
///                             Конструктор
// ============================================================================
/// \param  parent  Родительский виджет
// ============================================================================

WIoSettings::WIoSettings (QWidget *parent) :
    QDialog(parent),
    ui(new Ui::WIoSettings),
    m_tmpSettings(),
    m_prevDrvName()
{
    ui->setupUi(this);
    setWindowFlags(Qt::Dialog | Qt::CustomizeWindowHint | Qt::WindowTitleHint | Qt::WindowCloseButtonHint);

    // Список драйверов IO
    QStringList drvList;
    CFactory::GetDrivers(&drvList);
    ui->driverList->addItems(drvList);
    connect(ui->driverList, static_cast<void (QComboBox::*)(const QString&)>(&QComboBox::currentIndexChanged), this, &WIoSettings::onDrvSelect);

    // Список кодировок
    QStringList listCharsets;
    listCharsets << "Windows-1251" << "UTF-8" << "KOI8-R";
    ui->charsetList->addItems(listCharsets);

    // Окончание строки
    ui->lineEnding->addItem("none", "");
    ui->lineEnding->addItem("\\n", "\n");
    ui->lineEnding->addItem("\\r\\n", "\r\n");

    // Установка актуальных значений
    CGui* pGui = CFactory::GetGui();
    CIoSess* pSession = pGui->GetActiveSession();
    CSettings* pSettings = CFactory::GetSettings();
    QString groupName = CSettings::GetSessGroup( pSession->GetId() );
    int result = pSettings->Get(&groupName, &m_tmpSettings);

    if (result)
    {
        // Ручная установка и вызов обработчика для отрисовки настроек
        // (т.к. сигнал не генерится)
        ui->driverList->setCurrentIndex(0);
        onDrvSelect(drvList.first());
    }
    else
    {
        int index = ui->lineEnding->findData( m_tmpSettings.value("eol", CFG_SESS_DEFAULT_EOL) );
        ui->lineEnding->setCurrentIndex( (index < 0) ? 0 : index);

        QString charset = m_tmpSettings.value("charset", CFG_SESS_DEFAULT_CHARSET).toString();
        ui->charsetList->setCurrentText(charset);

        ui->timeout->setValue( m_tmpSettings.value("timeout", CFG_SESS_DEFAULT_TIMEOUT).toDouble() );

        QString drvName = m_tmpSettings.value("driver").toString();
        if ( drvName.isEmpty() )
        {
            ui->driverList->setCurrentIndex(0);
            onDrvSelect(drvList.first());
        }
        else
        {
            ui->driverList->setCurrentText(drvName);
        }
    }
}


// ============================================================================
///
///                             Деструктор
///
// ============================================================================

WIoSettings::~WIoSettings ()
{
    delete ui;
}


// ============================================================================
///
///                         Обработчик закрытия окна
///
// ============================================================================

void WIoSettings::accept (void)
{
    saveTmpSettings(m_prevDrvName);
    saveSettings();

    QDialog::accept();
    emit sigClosed(true);
}


// ============================================================================
///
///                         Обработчик закрытия окна
///
// ============================================================================

void WIoSettings::reject (void)
{
    QDialog::reject();
    emit sigClosed(false);
}


// ============================================================================
///                     Обработчик выбора драйвера в списке
// ============================================================================
/// \param  drvName     Имя активированного драйвера
// ============================================================================

void WIoSettings::onDrvSelect (const QString& drvName)
{
    // Сохранение выставленных параметров перед очисткой
    saveTmpSettings(m_prevDrvName);
    m_prevDrvName = drvName;

    // Очистка содержимого перед новой отрисовкой
    QObjectList childList = ui->drvContent->children();
    foreach (QObject* item, childList)
    {
        item->deleteLater();
    }

    // Удаление текущего компановщика (иначе драйвер не сможет установить новый компановщик)
    QLayout* currLayout = ui->drvContent->layout();
    if (currLayout) delete currLayout;

    CDriver* drv = CFactory::GetDriver(&drvName);
    if (!drv) return;

    ui->drvContent->setTitle( drv->GetTitle() );

    QHash<QString, QVariant> ioParams = m_tmpSettings.value(drvName).toHash();
    drv->DrawSettings(ui->drvContent, &ioParams);
}


// ============================================================================
///         Временное сохранение параметров драйвера (из ui->m_content)
// ============================================================================
/// \param  drvName     Имя драйвера
// ============================================================================

void WIoSettings::saveTmpSettings (QString& drvName)
{
    // Сохранение базовых параметров
    m_tmpSettings.insert("driver", drvName);
    m_tmpSettings.insert("charset", ui->charsetList->currentText());
    m_tmpSettings.insert("eol", ui->lineEnding->currentData());
    m_tmpSettings.insert("timeout", ui->timeout->value());

    if ( drvName.isEmpty() ) return;

    CDriver* drv = CFactory::GetDriver(&drvName);
    if (!drv) return;

    QHash<QString, QVariant> ioParams = m_tmpSettings.value(drvName).toHash();
    drv->SaveSettings(ui->drvContent, &ioParams);
    m_tmpSettings.insert(drvName, ioParams);
}


// ============================================================================
///
///                     Сохранение настроек сессии
///
// ============================================================================

void WIoSettings::saveSettings (void)
{
    CGui* pGui = CFactory::GetGui();
    CIoSess* activeSess = pGui->GetActiveSession();
    QString drvName = ui->driverList->currentText();

    // Смена и конфигурирование драйвера сессии
    CDriver* pDrv = activeSess->GetDriver();

    if (!pDrv || pDrv->GetName() != drvName)
    {
        pDrv = CFactory::CreateDriver(&drvName);
        activeSess->SetDriver(pDrv);
    }

    bool isOpen = pDrv->IsOpen();
    if (isOpen) pDrv->Close();

    QHash<QString, QVariant> ioParams = m_tmpSettings.value(drvName).toHash();
    pDrv->Configure(&ioParams);

    if (isOpen)
    {
        int result = pDrv->Open();

        if (result < 0)
        {
            CGuiEvent* pEvent = new CGuiEvent(EV_SHOW_IO_DRV_OPEN_ERROR);
            CGui* pGui = CFactory::GetGui();
            QApplication::postEvent(pGui, pEvent);
        }
    }

    // Сохранение параметров
    CSettings* pSettings = CFactory::GetSettings();
    QString group = CSettings::GetSessGroup( activeSess->GetId() );
    pSettings->Save(&group, &m_tmpSettings);
}
