//*****************************************************************************
//
// Имя файла    : 'wterm.cpp'
// Описание     : Виджет терминала
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************

#include "ui/widgets/wterm.h"
#include "factory.h"
#include "defines.h"
#include <QPalette>
#include <QHBoxLayout>
#include <QDebug>


// ============================================================================
///                             Конструктор
// ============================================================================
/// \param  session Сессия, к которой привязана вкладка
/// \param  parent  Родительский виджет
// ============================================================================

WTerm::WTerm (CIoSess* session, QWidget* parent) :
    QWidget(parent),
    m_pSession(session),
    m_wOut(this)
{
    initUi();

    // Соединение сигналов и слотов
    connect(&m_wOut, &WTermIO::SignalSendRequest, this, &WTerm::onSendRequest);
    connect(m_pSession, &CIoSess::SignalPacketReceived, this, &WTerm::onPacketRecv);
    connect(m_pSession, &CIoSess::SignalPacketSended, this, &WTerm::onPacketSended);
    connect(CFactory::GetSettings(), &CSettings::SigSettingsChanged, this, &WTerm::onSettingsChanged);

    // Расположение элементов
    QHBoxLayout* layout = new QHBoxLayout(this);
    layout->setSpacing(0);
    layout->setContentsMargins(1, 1, 1, 1);
    layout->addWidget(&m_wOut);

    // Создание панели
    CGuiEvent ev(EV_BUILD_PANEL);
    QWidget* panel = NULL;
    CGui* pGui = CFactory::GetGui();

    ev.SetParam(&panel);
    QApplication::sendEvent(pGui, &ev);

    if (panel)
    {
#warning "FIXME"
    // Расположение (слева, справа) зависит от настроек
//    layout.addWidget(&m_panel);
        layout->insertWidget(0, panel);
    }

    setLayout(layout);
}


// ============================================================================
///                     Обработчик события показа виджета
// ============================================================================
/// \param  ev  Дескриптор события
// ============================================================================

void WTerm::showEvent (QShowEvent *ev)
{
    Q_UNUSED(ev);
    m_wOut.setFocus();
}


// ============================================================================
///
///         Инициализация интерфейса в соответствии с настройками
///
// ============================================================================

void WTerm::initUi (void)
{
    CSettings* pSettings = CFactory::GetSettings();
    QString groupName(SETTINGS_GROUP_BASE_NAME);
    QHash<QString, QVariant> params;
    pSettings->Get(&groupName, &params);

    // Настройка фона
    QFont f = font();
    f.setFamily( params.value("font", CFG_GEN_DEFAULT_FONT_FAMILY).toString() );
    f.setPixelSize( params.value("fontSize", CFG_GEN_DEFAULT_FONT_SIZE).toUInt() );
    setFont(f);
    m_wOut.setFont(f);

    // Настройка цвета
    QVariant val = params.value("colorBg");
    QColor colorBg = ( val.isValid() && !val.isNull() ) ? val.value<QColor>() : CFG_GEN_DEFAULT_COLOR_BG;
    val = params.value("colorIn");
    QColor colorIn = ( val.isValid() && !val.isNull() ) ? val.value<QColor>() : CFG_GEN_DEFAULT_COLOR_TEXT_IN;
    val = params.value("colorOut");
    QColor colorOut = ( val.isValid() && !val.isNull() ) ? val.value<QColor>() : CFG_GEN_DEFAULT_COLOR_TEXT_OUT;

    QPalette p = palette();
    p.setColor(QPalette::Base, colorBg);
    p.setColor(QPalette::Text, colorIn);
    setPalette(p);

    // Output
    m_wOut.SetColor(WTermIO::COLOR_BG, colorBg);
    m_wOut.SetColor(WTermIO::COLOR_TEXT_IN, colorIn);
    m_wOut.SetColor(WTermIO::COLOR_TEXT_OUT, colorOut);

    params.clear();
    groupName = CSettings::GetSessGroup( m_pSession->GetId() );
    pSettings->Get(&groupName, &params);

    m_wOut.SetLineEnding(params.value("eol", CFG_SESS_DEFAULT_EOL).toString());
    m_wOut.SetCharset(params.value("charset", CFG_SESS_DEFAULT_CHARSET).toString());
}


// ============================================================================
///                     Обработчик запроса отправки данных
// ============================================================================
/// \param  data    Данные
/// \param  size    Количество данных
// ============================================================================

void WTerm::onSendRequest (char* data, uint size)
{
    int result = m_pSession->SendData(data, size);

    if (result)
    {
        qDebug("send %d ERROR (%d)", size, result);
    }
}


// ============================================================================
///                     Обработчик входящих пакетов
// ============================================================================
/// \param  packet  Полученный пакет
// ============================================================================

void WTerm::onPacketRecv (TIoPack* packet)
{
    m_wOut.ShowPacket(packet);
}


// ============================================================================
///                     Обработчик отправленных пакетов
// ============================================================================
/// \param  session Сессия
/// \param  packet  Отправленный пакет
// ============================================================================

void WTerm::onPacketSended (TIoPack* packet)
{
    m_wOut.ShowPacket(packet);
}


// ============================================================================
///                     Обработчик изменения настроек
// ============================================================================
/// \param  group   Сессия
/// \param  params  Отправленный пакет
// ============================================================================

void WTerm::onSettingsChanged (const QString* group, QHash<QString, QVariant>* params)
{
    Q_UNUSED(params);

    if (*group != SETTINGS_GROUP_BASE_NAME)
    {
        int sessId = CSettings::GetSessId(group);
        if (sessId < 0 || m_pSession->GetId() != (uint)sessId) return;
    }

    initUi();
}
