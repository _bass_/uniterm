//*****************************************************************************
//
// Имя файла    : 'wstatusbar.cpp'
// Описание     : Виджет строки состояния
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************

#include "ui/widgets/wstatusbar.h"


// ============================================================================
///
///                             Конструктор
///
// ============================================================================

WStatusBar::WStatusBar (QWidget* parent) :
    QStatusBar(parent)
{
}


// ============================================================================
///
///                 Обновление информации в строке состояния
///
// ============================================================================
/*
void WStatusBar::update( void )
{
    // у GUI запрашиваем активный tab
    // у таба->status()
    // таб-драйвер-имя
    // таб-драйвер-rx/tx
    // время запуска/прошло времени ...
}
*/
