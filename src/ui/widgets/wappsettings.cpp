//*****************************************************************************
//
// Имя файла    : 'wappsettings.cpp'
// Описание     : Окно настроек приложения
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************

#include "ui/widgets/wappsettings.h"
#include "ui/widgets/isettingsgroup.h"
#include "ui_wappsettings.h"
#include "factory.h"
#include <QDebug>


// ============================================================================
//                          Локальные функции
// ============================================================================

    // Загрузка настроек для элемента дерева меню
    static void loadItemSettings (QStandardItem* ptr, QHash<QString, QVariant>* pStorage);


// ============================================================================
///                             Конструктор
// ============================================================================
/// \param  parent  Родительский виджет
// ============================================================================

WAppSettings::WAppSettings (QWidget *parent) :
    QDialog(parent),
    ui(new Ui::WAppSettings),
    m_groupListModel(this),
    m_appSettings()
{
    ui->setupUi(this);
    setWindowFlags(Qt::Dialog | Qt::CustomizeWindowHint | Qt::WindowTitleHint | Qt::WindowCloseButtonHint);
    ui->contentLayout->setAlignment(Qt::AlignTop);

    ui->groupList->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->groupList->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->groupList->setHeaderHidden(true);

    // Формирование дерева меню
    for (TGroupCreator* ptr = (TGroupCreator*)SETTINGS_GROUP_LIST_START_ADDR; ptr < (TGroupCreator*)SETTINGS_GROUP_LIST_END_ADDR; ++ptr)
    {
        ISettingsGroup* group = (*ptr)();
        QString name = group->GetTitle();
        name = name[0].toUpper() + name.mid(1).toLower();
        QStandardItem* groupItem = new QStandardItem(name);

        groupItem->setData(qVariantFromValue(group), STYPE_GROUP_OBJ);
        name = group->GetName().toLower();
        groupItem->setData(name, STYPE_SETTINGS_GROUP_NAME);

        group->BuildSubTree(groupItem);

        m_groupListModel.appendRow(groupItem);
    }

    loadSettings();

    ui->groupList->setModel(&m_groupListModel);
    connect(ui->groupList->selectionModel(), &QItemSelectionModel::currentRowChanged, this, &WAppSettings::onItemSelect);
}


// ============================================================================
///
///                             Деструктор
///
// ============================================================================

WAppSettings::~WAppSettings ()
{
    delete ui;
}


// ============================================================================
///
///                         Обработчик закрытия окна
///
// ============================================================================

void WAppSettings::accept (void)
{
    // Сохранение настроек из активного виджета
    saveTmpSettings( ui->groupList->currentIndex() );

    // Сохранение настроек приложения
    saveSettings();

    QDialog::accept();
    emit sigClosed(true);
}


// ============================================================================
///
///                         Обработчик закрытия окна
///
// ============================================================================

void WAppSettings::reject (void)
{
    QDialog::reject();
    emit sigClosed(false);
}


// ============================================================================
///                     Обработчик выбора драйвера в списке
// ============================================================================
/// \param  index       Индекс активированного драйвера
/// \param  previous    Индекс деактивированного драйвера
// ============================================================================

void WAppSettings::onItemSelect (const QModelIndex& current, const QModelIndex& previous)
{
    // Сохранение параметров перед отчисткой виджета
    if ( previous.isValid() )
    {
        saveTmpSettings(previous);
    }

    // Очистка содержимого перед новой отрисовкой
    QObjectList childList = ui->content->children();
    foreach (QObject* item, childList)
    {
        item->deleteLater();
    }

    // Удаление текущего компановщика (иначе при отрисовке виджет не сможет установить новый компановщик)
    QLayout* currLayout = ui->content->layout();
    if (currLayout) delete currLayout;

    if ( !current.isValid() ) return;

    ui->wrapContent->setTitle( current.data().toString() );

    ISettingsGroup* group = current.data(STYPE_GROUP_OBJ).value<ISettingsGroup*>();
    if (!group) return;

    QString settingsGroupName = current.data(STYPE_SETTINGS_GROUP_NAME).toString();
    if ( settingsGroupName.isEmpty() ) settingsGroupName = group->GetName();

    QHash<QString, QVariant> groupSettings = m_appSettings.value(settingsGroupName).toHash();
    group->SettingsDraw(&current, ui->content, &groupSettings);
}


// ============================================================================
///         Временное сохранение параметров (из активного виджета)
// ============================================================================
/// \param  index   Индекс активного пункта меню
// ============================================================================

void WAppSettings::saveTmpSettings (const QModelIndex& index)
{
    if ( !index.isValid() ) return;

    ISettingsGroup* group = index.data(STYPE_GROUP_OBJ).value<ISettingsGroup*>();
    if (!group) return;

    QString settingsGroupName = index.data(STYPE_SETTINGS_GROUP_NAME).toString();
    if ( settingsGroupName.isEmpty() ) settingsGroupName = group->GetName();

    QHash<QString, QVariant> groupSettings;
    group->SettingsParse(&index, ui->content, &groupSettings);
    m_appSettings.insert(settingsGroupName, groupSettings);
}


// ============================================================================
///
///                     Сохранение настроек
///
// ============================================================================

void WAppSettings::saveSettings (void)
{
    CSettings* pAppSettings = CFactory::GetSettings();
    QList<QString> groupList = m_appSettings.keys();

    foreach (QString group, groupList)
    {
        QHash<QString, QVariant> params = m_appSettings.value(group).toHash();
        pAppSettings->Save(&group, &params);
    }
}


// ============================================================================
///
///                     Загрузка настроек в локальное хранилище
///
// ============================================================================

void WAppSettings::loadSettings (void)
{
    for (int row = 0; row < m_groupListModel.rowCount(); ++row)
    {
        QStandardItem* item = m_groupListModel.item(row);
        loadItemSettings(item, &m_appSettings);
    }
}


// ============================================================================
///                 Загрузка настроек для элемента дерева меню
// ============================================================================
/// \param  ptr         Элемент дерева меню
/// \param  pStorage    Хранилище настроек
// ============================================================================

void loadItemSettings (QStandardItem* ptr, QHash<QString, QVariant>* pStorage)
{
    if (!ptr || !pStorage) return;

    ISettingsGroup* group = ptr->data(STYPE_GROUP_OBJ).value<ISettingsGroup*>();
    if (!group) return;

    // Загрузка параметров для текущего пункта меню
    QString groupName = ptr->data(STYPE_SETTINGS_GROUP_NAME).toString();

    if ( groupName.isEmpty() ) groupName = group->GetName();
    if ( groupName.isEmpty() ) return;

    CSettings* pAppSettings = CFactory::GetSettings();
    if (!pAppSettings) return;

    QHash<QString, QVariant> params;

    // Формирование параметров по умолчанию
    {
        QWidget dummyWidget;
        QModelIndex index = ptr->index();
        group->SettingsDraw(&index, &dummyWidget, &params);
        group->SettingsParse(&index, &dummyWidget, &params);
    }

    // Загрузка данных из сохраненных настроек
    pAppSettings->Get(&groupName, &params);
    pStorage->insert(groupName, params);

    // Загрузка параметров для дочерних пунктов меню
    for (int row = 0; row < ptr->rowCount(); ++row)
    {
        loadItemSettings(ptr->child(row), pStorage);
    }
}
