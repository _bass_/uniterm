//*****************************************************************************
//
// Имя файла    : 'gui.cpp'
// Описание     : Грифический интерфейс приложения
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************

#include "ui/gui.h"
#include "factory.h"
#include "ui/widgets/wpanel.h"
#include "ui/widgets/wstatusbar.h"
#include "defines.h"
#include <QToolButton>
#include <QTabBar>
#include <QDesktopServices>
#include <QMessageBox>


// ============================================================================
///
///                             Конструктор
///
// ============================================================================

CGui::CGui () :
    QObject(),
    m_window(),
    m_wrap(&m_window),
    m_menu(NULL),
    m_winIoSettings(NULL),
    m_winAppSettings(NULL),
    m_winAbout(NULL),
    m_scTabClose(&m_wrap),
    m_scTabChange(&m_wrap),
    m_scTabChangeRev(&m_wrap),
    m_translator()
{
    CSettings* pSettings = CFactory::GetSettings();
    QString groupName(SETTINGS_GROUP_BASE_NAME);
    QHash<QString, QVariant> params;

    int result = pSettings->Get(&groupName, &params);
    bool stayOnTop = ( !result && params.value("stayOnTop").toBool() );
    if (stayOnTop)
    {
        m_window.setWindowFlags(m_window.windowFlags() | Qt::WindowStaysOnTopHint);
    }

    connect(pSettings, &CSettings::SigSettingsChanged, this, &CGui::onSettingsChanged);
    connect(&m_wrap, &QTabWidget::currentChanged, this, &CGui::onTabChanged);

    // Настройка внешнего вида tooltip'ов
    qApp->setStyleSheet(
                "QToolTip {"
                    "color: white;"
                    "border: 1px solid #333333;"
                    "background-color: #000000;"
                    "opacity: 200;"
                    "padding: 2;"
                "}"
    );

    // Загрузка перевода
    QString fname = ":/res/lang/" + QLocale::system().name().mid(0, 2);
    m_translator.load(fname);
    qApp->installTranslator(&m_translator);

    // Перехват закрытия приложения
    connect(qApp, &QApplication::aboutToQuit, this, &CGui::onWinClose);
}


// ============================================================================
///
///                             Деструктор
///
// ============================================================================

CGui::~CGui ()
{
}


// ============================================================================
///
///                             Отображение GUI
///
// ============================================================================

void CGui::Show ()
{
    m_window.setWindowTitle( tr(APP_TITLE) );
    m_window.setWindowIcon( QPixmap(":/res/app_logo.png") );

    buildMenu();
    initTabs();
    initStatusBar();

    // Отображение окна с восстановлением геометрии
    CSettings* pSettings = CFactory::GetSettings();
    QString groupName(SETTINGS_GROUP_BASE_NAME);
    QHash<QString, QVariant> params;
    int result = pSettings->Get(&groupName, &params);

    do
    {
        if (result < 0) break;

        int x = params.value("win_x", 0).toInt();
        int y = params.value("win_y", 0).toInt();
        int w = params.value("win_width", 0).toInt();
        int h = params.value("win_height", 0).toInt();
        bool isMaximized = params.value("win_maximized", true).toBool();

        if (w > 0 && h > 0)
        {
            m_window.resize(w, h);
        }
        else
        {
            QSize s = m_window.sizeHint();
            if ( s.isValid() ) m_window.resize(s);
        }

        if (!isMaximized)
        {
            m_window.move(x, y);
            m_window.show();
            return;
        }
    } while (0);

    m_window.showMaximized();
}


// ============================================================================
///                         Запрос активной сессии
// ============================================================================
/// \return     Указатель на активную сессию или NULL при ошибке
// ============================================================================

CIoSess* CGui::GetActiveSession (void)
{
    QWidget* widget = m_wrap.currentWidget();
    if (!widget) return NULL;

    WTerm* term = static_cast<WTerm*>(widget);
    return term->GetSession();
}


// ============================================================================
///                         Запрос активной сессии
// ============================================================================
/// \param  pList   Указатель на переменную для сохранения списка
/// \return         0 или отрицательный код ошибки
// ============================================================================

int CGui::GetListPanels (QList<QWidget*>* pList)
{
    if (!pList) return -1;

    QList<WPanel*> list = m_wrap.findChildren<WPanel*>();
    foreach (WPanel* panel, list)
    {
        pList->append( (QWidget*)panel );
    }

    return 0;
}


// ============================================================================
///
///                     Инициализация вкладок (сессий)
///
// ============================================================================

void CGui::initTabs (void)
{
    m_wrap.setTabsClosable(true);
    connect( &m_wrap, SIGNAL(tabCloseRequested(int)), this, SLOT(onTabClose(int)) );

    CSettings* pSettings = CFactory::GetSettings();
    QString groupName(SETTINGS_GROUP_BASE_NAME);
    QHash<QString, QVariant> params;
    pSettings->Get(&groupName, &params);

    // Добавление кнопки создания вкладки
    QToolButton* btn = new QToolButton(&m_wrap);
    btn->setIcon( QIcon(":/res/plus.png") );
    QString tip( tr("Add tab") );
    tip.append(" (" + params.value(SETTINGS_SHORTCUT_PARAM_PREFIX "newtab").toString() + ")");
    btn->setToolTip(tip);
    btn->setAutoRaise(true);

    m_wrap.setCornerWidget(btn, Qt::TopLeftCorner);
    connect(btn, &QToolButton::clicked, this, &CGui::onBtnAddTab);

    // Инициализация hotkey'ев
    m_scTabClose.setAutoRepeat(false);
    m_scTabChange.setAutoRepeat(false);
    m_scTabChangeRev.setAutoRepeat(false);

    connect(&m_scTabClose, &QShortcut::activated, this, &CGui::onActTabClose);
    connect(&m_scTabChange, &QShortcut::activated, this, &CGui::onActTabChange);
    connect(&m_scTabChangeRev, &QShortcut::activated, this, &CGui::onActTabChangeRev);

    initShortcuts();

    m_window.setCentralWidget(&m_wrap);

    // Создание вкладки для каждой сессии
    addTab();
}


// ============================================================================
///
///                     Инициализация shortcut'ов
///
// ============================================================================

void CGui::initShortcuts (void)
{
    CSettings* pSettings = CFactory::GetSettings();
    QString groupName(SETTINGS_GROUP_BASE_NAME);
    QHash<QString, QVariant> params;
    int result = pSettings->Get(&groupName, &params);
    if (result < 0) return;

    QWidget* widget = m_wrap.cornerWidget(Qt::TopLeftCorner);
    if (widget)
    {
        QToolButton* btn = qobject_cast<QToolButton*>(widget);
        if (btn)
        {
            btn->setShortcut( QKeySequence(params.value(SETTINGS_SHORTCUT_PARAM_PREFIX "newtab").toString()) );
        }
    }

    m_scTabClose.setKey( QKeySequence(params.value(SETTINGS_SHORTCUT_PARAM_PREFIX "closetab").toString()) );
    m_scTabChange.setKey( QKeySequence("Ctrl+Tab") );
    m_scTabChangeRev.setKey( QKeySequence("Ctrl+Shift+Tab") );
}


// ============================================================================
///
///                     Инициализация строки состояния
///
// ============================================================================

void CGui::initStatusBar (void)
{
    m_window.setStatusBar( new WStatusBar(&m_window) );
    m_window.statusBar()->showMessage(tr("Ready"), 2000);
}


// ============================================================================
///                     Создание новой вкладки и сессии
// ============================================================================
/// \return     0 или отрицательный код ошибки
// ============================================================================

int CGui::addTab (void)
{
    CIoSess* session = CFactory::SessionCreate();
    if (!session) return -1;

    // Добавление виджета вкладки
    WTerm* io = new WTerm(session, &m_wrap);

    // Информирование плагинов о создании панели
    // (с задержкой из-за проблем определения привязанной сессии в customEvent)
    WPanel* panel = io->findChild<WPanel*>();
    if (panel)
    {
        panel->setProperty(GUI_PANEL_PROPETRY_SESSID, session->GetId());

        CPluginMgr* pMgr = CFactory::GetPluginMgr();
        pMgr->Dispatch(PLG_EV_GUI_BUILD_PANEL, panel);
    }

    CDriver* drv = session->GetDriver();
    int index = m_wrap.addTab( io, (drv && drv->GetName() != "dummy") ? drv->GetTitle() : tr("No driver") );
    m_tabList.insert(index, io);

    QWidget* closeBtn = m_wrap.tabBar()->tabButton(index, QTabBar::RightSide);
    if (closeBtn)
    {
        CSettings* pSettings = CFactory::GetSettings();
        QString groupName(SETTINGS_GROUP_BASE_NAME);
        QHash<QString, QVariant> params;
        pSettings->Get(&groupName, &params);

        QString tip( tr("Close tab") );
        tip.append(" (" + params.value(SETTINGS_SHORTCUT_PARAM_PREFIX "closetab").toString() + ")");

        closeBtn->setToolTip(tip);
    }

    return 0;
}


// ============================================================================
///                     Создание основного меню
// ============================================================================
/// \return     Указатель на
// ============================================================================

QMenu* CGui::buildMenu (void)
{
    if (m_menu) return m_menu;

    m_menu = new QMenu(&m_wrap);

    QAction* act = m_menu->addAction( tr("Settings") );
    connect(act, &QAction::triggered, this, &CGui::onAppSettingsOpen);

    act = m_menu->addAction( tr("Help") );
    connect(act, &QAction::triggered, this, &CGui::onMenuHelp);

    act = m_menu->addAction( tr("About") );
    connect(act, &QAction::triggered, this, &CGui::onAboutOpen);

    // Информирование плагинов
    CPluginMgr* pMgr = CFactory::GetPluginMgr();
    pMgr->Dispatch(PLG_EV_GUI_BUILD_MENU, &m_menu);

    return m_menu;
}


// ============================================================================
///                     Обработчик событий
// ============================================================================
/// \param  event   Событие (CEventGui*)
// ============================================================================

void CGui::customEvent (QEvent* event)
{
    switch ( (TEvType)event->type() )
    {
        case EV_BUILD_PANEL:
        {
            WPanel* pPanel = new WPanel(&m_wrap);

            // Информирование плагинов
            CPluginMgr* pMgr = CFactory::GetPluginMgr();
            pMgr->Dispatch(PLG_EV_GUI_BUILD_PANEL, pPanel);

            CGuiEvent* ev = static_cast<CGuiEvent*>(event);
            QWidget** ptr = (QWidget**) ev->GetParam();
            *ptr = pPanel;
        }
        break;

        case EV_BUILD_MENU:
        {
            CGuiEvent* ev = static_cast<CGuiEvent*>(event);
            QMenu** pMenu = (QMenu**) ev->GetParam();
            *pMenu = m_menu;
        }
        break;

        case EV_SHOW_IO_SETTINGS:
            if (!m_winIoSettings)
            {
                m_winIoSettings = new WIoSettings(&m_wrap);
                connect(m_winIoSettings, &WIoSettings::sigClosed, this, &CGui::onIoSettingsClose);
            }
            m_winIoSettings->show();
        break;

        case EV_SHOW_IO_DRV_OPEN_ERROR:
            m_window.statusBar()->showMessage(tr("Driver open ERROR!"), 5000);
        break;

        default: return;
    }

    event->accept();
}


// ============================================================================
///                     Обработчик смены вкладки
// ============================================================================
/// \param  index   Индекс сессии (вкладки)
// ============================================================================

void CGui::onTabChanged (int index)
{
    if ( index < 0 || index >= m_tabList.size() ) return;

    WTerm* io = m_tabList.at(index);
    if (!io) return;

    CIoSess* pSess = io->GetSession();
    if (!pSess) return;

    CPluginMgr* pMgr = CFactory::GetPluginMgr();
    pMgr->Dispatch(PLG_EV_SESS_ACTIVATE, pSess);
}


// ============================================================================
///                     Обработчик закрытия вкладки
// ============================================================================
/// \param  index   Индекс сессии (вкладки)
// ============================================================================

void CGui::onTabClose (int index)
{
    m_wrap.removeTab(index);

    WTerm* io = m_tabList.at(index);
    if (io)
    {
        CIoSess* session = io->GetSession();
        CFactory::SessionDelete(session);
        delete io;
    }

    // Создание новой сессии при закрытии последней вкладки
    if ( !m_wrap.count() )
    {
        addTab();
    }
}


// ============================================================================
///                 Обработчик закрытия окна настроек IO
// ============================================================================
/// \param  result  true - закрытие по OK, иначе false
// ============================================================================

void CGui::onIoSettingsClose (bool result)
{
    Q_UNUSED(result);

    // Закрытие окна
    disconnect(m_winIoSettings, &WIoSettings::sigClosed, this, &CGui::onIoSettingsClose);
    delete m_winIoSettings;
    m_winIoSettings = NULL;

    // Обновление названия вкладки
    CIoSess* session = GetActiveSession();
    CDriver* drv = session->GetDriver();

    int tabIndex = m_wrap.currentIndex();
    m_wrap.setTabText( tabIndex, (drv && drv->GetName() != "dummy") ? drv->GetTitle() : tr("No driver") );
}


// ============================================================================
///                 Обработчик открытия окна настроек
// ============================================================================
/// \param  checked     Флаг типа активации
// ============================================================================

void CGui::onAppSettingsOpen (bool checked)
{
    Q_UNUSED(checked);

    if (!m_winAppSettings)
    {
        m_winAppSettings = new WAppSettings(&m_wrap);
        connect(m_winAppSettings, &WAppSettings::sigClosed, this, &CGui::onAppSettingsClosed);
    }

    m_winAppSettings->show();
}


// ============================================================================
///                 Обработчик закрытия окна настроек
// ============================================================================
/// \param  result  true - закрытие по OK, иначе false
// ============================================================================

void CGui::onAppSettingsClosed (bool result)
{
    Q_UNUSED(result);

    if (!m_winAppSettings) return;

    // Закрытие окна
    disconnect(m_winAppSettings, &WAppSettings::sigClosed, this, &CGui::onAppSettingsClosed);
    delete m_winAppSettings;
    m_winAppSettings = NULL;
}


// ============================================================================
///                 Обработчик активации меню "Помощь"
// ============================================================================
/// \param  checked     Флаг типа активации
// ============================================================================

void CGui::onMenuHelp (bool checked)
{
    Q_UNUSED(checked);

    QString file;
    file.append( QCoreApplication::applicationDirPath() );
    file.append("/docs/help.html");

    if ( !QFile::exists(file) )
    {
        QMessageBox::critical( &m_window, tr("Help"), tr("Help file not found") );
        return;
    }

    file.prepend("file:///");
    QDesktopServices::openUrl( QUrl(file) );
}


// ============================================================================
///                 Обработчик открытия окна о приложении
// ============================================================================
/// \param  checked     Флаг типа активации
// ============================================================================

void CGui::onAboutOpen (bool checked)
{
    Q_UNUSED(checked);

    if (!m_winAbout)
    {
        m_winAbout = new WAbout(&m_wrap);
        connect(m_winAbout, &WAbout::finished, this, &CGui::onAboutClosed);
    }

    m_winAbout->show();
}


// ============================================================================
///                 Обработчик закрытия окна о приложении
// ============================================================================
/// \param  result  true - закрытие по OK, иначе false
// ============================================================================

void CGui::onAboutClosed (bool result)
{
    Q_UNUSED(result);

    if (!m_winAbout) return;

    // Закрытие окна
    disconnect(m_winAbout, &WAbout::finished, this, &CGui::onAboutClosed);
    delete m_winAbout;
    m_winAbout = NULL;
}


// ============================================================================
///                 Обработчик нажатия кнопки добавления вкладки
// ============================================================================
/// \param  checked Флаг фиксации
// ============================================================================

void CGui::onBtnAddTab (bool checked)
{
    Q_UNUSED(checked);
    addTab();

    QTabBar* bar = m_wrap.tabBar();
    if (!bar) return;

    bar->setCurrentIndex( bar->count() - 1 );
}


// ============================================================================
///
///                 Обработчик hotkey закрытия вкладки
///
// ============================================================================

void CGui::onActTabClose (void)
{
    QTabBar* bar = m_wrap.tabBar();
    if (!bar) return;

    int index = bar->currentIndex();
    if (index < 0) return;

    onTabClose(index);
}


// ============================================================================
///
///                 Обработчик hotkey смены вкладки
///
// ============================================================================

void CGui::onActTabChange (void)
{
    QTabBar* bar = m_wrap.tabBar();
    if (!bar) return;

    int index = bar->currentIndex();
    if (index < 0) return;

    ++index;
    if ( index >= bar->count() ) index = 0;

    bar->setCurrentIndex(index);
}


// ============================================================================
///
///                 Обработчик hotkey смены вкладки
///
// ============================================================================

void CGui::onActTabChangeRev (void)
{
    QTabBar* bar = m_wrap.tabBar();
    if (!bar) return;

    int index = bar->currentIndex();
    if (index < 0) return;

    --index;
    if (index < 0) index = bar->count() - 1;

    if (index >= 0) bar->setCurrentIndex(index);
}


// ============================================================================
///                     Обработчик изменения настроек
// ============================================================================
/// \param  group   Группа настроек
/// \param  params  Актуальные параметры
// ============================================================================

void CGui::onSettingsChanged (const QString* group, QHash<QString, QVariant>* params)
{
    if (*group != SETTINGS_GROUP_BASE_NAME) return;

    initShortcuts();

    bool state = params->value("stayOnTop").toBool();
    Qt::WindowFlags flags = m_window.windowFlags();

    if ( (bool)(flags & Qt::WindowStaysOnTopHint) != state )
    {
        m_window.hide();

        if (state)
        {
            m_window.setWindowFlags(flags | Qt::WindowStaysOnTopHint);
        }
        else
        {
            m_window.setWindowFlags(flags & ~Qt::WindowStaysOnTopHint);
        }

        m_window.show();
    }
}


// ============================================================================
///
///                     Обработчик закрытия основного окна
///
// ============================================================================

void CGui::onWinClose (void)
{
    CSettings* pSettings = CFactory::GetSettings();
    QString groupName(SETTINGS_GROUP_BASE_NAME);
    QHash<QString, QVariant> params;

    int result = pSettings->Get(&groupName, &params);
    if (result < 0) return;

    params.insert( "win_x", m_window.x() );
    params.insert( "win_y", m_window.y() );

    QSize size = (m_window.isMaximized()) ? m_window.sizeHint() : m_window.size();
    params.insert( "win_width", (size.isValid()) ? size.width() : 0 );
    params.insert( "win_height", (size.isValid()) ? size.height() : 0 );
    params.insert( "win_maximized", m_window.isMaximized() );

    pSettings->Save(&groupName, &params);
}
