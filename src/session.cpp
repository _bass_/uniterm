//*****************************************************************************
//
// Имя файла    : 'session.cpp'
// Описание     : Модель для работы с сессией ввода-вывода
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************

#include "session.h"
#include "factory.h"
#include "defines.h"
#include <QDateTime>
#include <QDebug>


// ============================================================================
///
///                             Конструктор
///
// ============================================================================

    // Инициализация пакета (заполнение даты/времени)
    static void initPacket (CIoSess* sess, TIoPack* packet, TIoType type, char* data, uint size);
    // Конвертация строкового представления числа
    static int  charConvert (char c);


// ============================================================================
///                             Конструктор
// ============================================================================
/// \param  id  Идентификатор сесии
// ============================================================================

CIoSess::CIoSess (uint id) :
    QObject( CFactory::GetCore() ),
    m_id(id),
    m_pDrv(NULL),
    m_indexRx(0),
    m_timerParser(this)
{
    m_listTx.clear();

    CSettings* pSettings = CFactory::GetSettings();
    QString group = CSettings::GetSessGroup(m_id);
    QHash<QString, QVariant> params;
    pSettings->Get(&group, &params);

    configureTimer(&params);

    connect(pSettings, &CSettings::SigSettingsChanged, this, &CIoSess::onSettingsChanged);
    connect(&m_timerParser, &QTimer::timeout, this, &CIoSess::onParserTimeout);
}


// ============================================================================
///
///                             Деструктор
///
// ============================================================================

CIoSess::~CIoSess ()
{
    SetDriver(NULL);
}

// ============================================================================
///                 Установка используемого драйвера ввода-вывода
// ============================================================================
/// \param  drv     Драйвер
// ============================================================================

void CIoSess::SetDriver (CDriver* drv)
{
    // Удаление старых связей
    if (m_pDrv)
    {
        disconnect(m_pDrv, &CDriver::Sended, this, &CIoSess::onDataSended);
        disconnect(m_pDrv, &CDriver::Received, this, &CIoSess::OnDataRecv);
        disconnect(this, &CIoSess::privateSigDoSend, m_pDrv, &CDriver::Send);
    }

    m_pDrv = drv;
    m_listTx.clear();
    m_indexRx = 0;

    if (m_pDrv)
    {
        connect(m_pDrv, &CDriver::Sended, this, &CIoSess::onDataSended);
        connect(m_pDrv, &CDriver::Received, this, &CIoSess::OnDataRecv);
        connect(this, &CIoSess::privateSigDoSend, m_pDrv, &CDriver::Send);
    }
}


// ============================================================================
///                 Чтение указателя на используемый драйвер
// ============================================================================
/// \return         Драйвер
// ============================================================================

CDriver* CIoSess::GetDriver (void)
{
    return m_pDrv;
}


// ============================================================================
///                     Отправка данных
// ============================================================================
/// \param  data    Исходящие данные
/// \param  size    Количество данных
/// \return         0 или отрицательный код ошибки
// ============================================================================

int CIoSess::SendData (const char* data, uint size)
{
    if (!size) return -1;

    if (!m_pDrv)
    {
        qDebug("[Session] driver not set");
        return -2;
    }

    if ( !m_pDrv->IsOpen() ) return -3;

    char* buff = (char*) malloc(size);
    if (!buff)
    {
        qDebug("[Session] memory ERROR");
        return -4;
    }

    memcpy(buff, data, size);

    // Замена \00..\ff
    uint dstIndex = 0;
    for (uint i = 0; i < size; ++i, ++dstIndex)
    {
        if (buff[i] == '\\' && i + 2 < size)
        {
            char byte = 0;
            char c = buff[i + 1];

            int num = charConvert(c);
            if (num < 0)
            {
                buff[dstIndex] = buff[i];
                continue;
            }
            byte = num << 4;

            c = buff[i + 2];
            num = charConvert(c);
            if (num < 0)
            {
                buff[dstIndex] = buff[i];
                continue;
            }
            byte |= num;

            buff[dstIndex] = byte;
            i += 2;
        }
        else if (i != dstIndex)
        {
            buff[dstIndex] = buff[i];
        }
    }
    size = dstIndex;

    // Формирование пакета
    TIoPack packet;
    initPacket(this, &packet, IO_TYPE_OUT, buff, size);
    packet.dynMem = true;

    // Информирование плагинов
    CPluginMgr* pMgr = CFactory::GetPluginMgr();
    pMgr->Dispatch(PLG_EV_SESS_PACKET_BEFORE_SEND, &packet);

    m_listTx.append(packet);

    emit privateSigDoSend(packet.data, packet.size);

    return 0;
}


// ============================================================================
///                 Обработчик окончания процесса отправки данных
// ============================================================================
/// \param  drv     Драйвер ввода-вывода
/// \param  data    Отправленные данные
/// \param  size    Количество данных
// ============================================================================

void CIoSess::onDataSended (const char* data, uint size)
{
    Q_UNUSED(data);

    if (m_listTx.isEmpty() || !size) return;

    while (size)
    {
        TIoPack packet = m_listTx.first();
        m_listTx.removeFirst();

        CPluginMgr* pMgr = CFactory::GetPluginMgr();
        pMgr->Dispatch(PLG_EV_SESS_PACKET_AFTER_SEND, &packet);

        // Информирование об успешной отправке
        emit SignalPacketSended(&packet);

        // Освобождение памяти буфера
        if (packet.dynMem)
        {
            delete(packet.data);
        }

        size -= (size > packet.size) ? packet.size : size;
    }
}


// ============================================================================
///                 Обработчик входящих данных (парсер пакетов)
// ============================================================================
/// \param  data    Входящие данные
/// \param  size    Количество принятых данных
// ============================================================================

void CIoSess::OnDataRecv (const char* data, uint size)
{
    CPluginMgr* pMgr = CFactory::GetPluginMgr();
    char* ptr = (char*) data;

    while (size--)
    {
        if (m_indexRx >= sizeof(m_buffRx) - 1)
        {
            // Контроль переполнения буфера
            m_indexRx = sizeof(m_buffRx) - 1;
        }
        else
        {
            char byte = *ptr++;
            m_buffRx[m_indexRx] = byte;
            ++m_indexRx;

            // Ожидание получения строки
            if (byte != '\n') continue;
        }

        m_buffRx[m_indexRx] = '\0';

        // Формирование пакета и информирование о получении данных
        TIoPack packet;
        initPacket(this, &packet, IO_TYPE_IN, m_buffRx, m_indexRx);
        m_indexRx = 0;

        // Информирование плагинов
        pMgr->Dispatch(PLG_EV_SESS_PACKET_BEFORE_RECV, &packet);
        emit SignalPacketReceived(&packet);
        pMgr->Dispatch(PLG_EV_SESS_PACKET_AFTER_RECV, &packet);
    }

    // Управление таймером контроля входящих данных
    if (m_indexRx)
    {
        m_timerParser.start();
    }
    else if ( m_timerParser.isActive() )
    {
        m_timerParser.stop();
    }
}


// ============================================================================
///
///                     Обработчик таймаута парсера
///
// ============================================================================

void CIoSess::onParserTimeout (void)
{
    char eol = '\n';
    OnDataRecv( &eol, sizeof(eol) );
}


// ============================================================================
///                  Конфигурирование таймера парсера
// ============================================================================
/// \param  group   Сессия
/// \param  params  Отправленный пакет
// ============================================================================

void CIoSess::configureTimer(const QHash<QString, QVariant>* params)
{
    int timeout = params->value(QString("timeout"), CFG_SESS_DEFAULT_TIMEOUT).toInt();
    m_timerParser.setInterval(timeout);
}


// ============================================================================
///                     Обработчик изменения настроек
// ============================================================================
/// \param  group   Сессия
/// \param  params  Отправленный пакет
// ============================================================================

void CIoSess::onSettingsChanged (const QString* group, QHash<QString, QVariant>* params)
{
    int sessId = CSettings::GetSessId(group);
    if (sessId < 0 || m_id != (uint)sessId) return;

    configureTimer(params);
}


// ============================================================================
///                 Обработчик входящих данных (парсер пакетов)
// ============================================================================
/// \param  sess    Сессия-владелец пакета
/// \param  packet  Формируемый пакет
/// \param  type    Тип пакета
/// \param  data    Данные
/// \param  size    Количество данных
// ============================================================================

void initPacket (CIoSess* sess, TIoPack* packet, TIoType type, char* data, uint size)
{
    QDateTime dt = QDateTime::currentDateTime();
    QDate currDate = dt.date();
    QTime currTime = dt.time();

    packet->type = type;
    packet->time.year = currDate.year();
    packet->time.month = currDate.month();
    packet->time.day = currDate.day();
    packet->time.hour = currTime.hour();
    packet->time.min = currTime.minute();
    packet->time.sec = currTime.second();
    packet->time.ms = currTime.msec();
    packet->data = data;
    packet->size = size;
    packet->session = sess;
    packet->dynMem = false;
}


// ============================================================================
///                 Конвертация строкового представления числа
// ============================================================================
/// \param  c   Конвертируемый символ
/// \return     Числовое представление символа или отрицательный код ошибки
// ============================================================================

int charConvert (char c)
{
    if (c >= '0' && c <= '9')       return c - '0';
    else if (c >= 'a' && c <= 'f')  return 10 + (c - 'a');
    else if (c >= 'A' && c <= 'F')  return 10 + (c - 'A');
    else                            return -1;
}
