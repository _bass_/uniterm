//*****************************************************************************
//
// Имя файла    : 'plugin_mgr.cpp'
// Описание     : Менеджер плагинов
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************

#include "plugins/plugin_mgr.h"
#include "plugins/iplugin.h"
#include "factory.h"
#include <QApplication>
#include <QPluginLoader>
#include <QDir>
#include <QDebug>


// ============================================================================
///                              Конструктор
// ============================================================================
/// \param  parent  Родительский объект
// ============================================================================

CPluginMgr::CPluginMgr (QObject* parent) :
    QObject(parent)
{
    buildList();
}


// ============================================================================
//
///                              Деструктор
//
// ============================================================================

CPluginMgr::~CPluginMgr ()
{
    // Выгрузка плагинов и удаление памяти дескрипторов
    while (m_listDesc.count() > 0)
    {
        TPlgDesc* pDesc = m_listDesc.first();
        m_listDesc.removeFirst();

        pDesc->pLoader->unload();
        delete pDesc->pLoader;
        delete pDesc;
    }
}


// ============================================================================
///                              Деструктор
// ============================================================================
/// \return     0 или отрицательный код ошибки
// ============================================================================

int CPluginMgr::Init (void)
{
    // Чтение состояний активации плагинов
    CSettings* pSettings = CFactory::GetSettings();
    QString group("plugins");
    QHash<QString, QVariant> cfgActivation;
    pSettings->Get(&group, &cfgActivation);

    // Инициализация плагинов
    foreach (TPlgDesc* pDesc, m_listDesc)
    {
        if (!pDesc) continue;

        pDesc->isEnabled = cfgActivation.value(pDesc->name, true).toBool();

        if (pDesc->isEnabled)
        {
            initPlugin(pDesc->name);
        }
        else
        {
            // Выгрузка плагина
            pDesc->plugin = NULL;
            pDesc->pLoader->unload();
        }
    }

    connect(pSettings, &CSettings::SigSettingsChanged, this, &CPluginMgr::onSettingsChanged);

    return 0;
}


// ============================================================================
///                     Оработчик событий для плагинов
// ============================================================================
/// \param  code    Код события
/// \param  data    Дополнительные данные события
// ============================================================================

void CPluginMgr::Dispatch (TPlgEv code, void* data)
{
    CPlgEvent ev(code);
    ev.SetParam(data);

    // Передача события плагинам
    foreach (TPlgDesc* pDesc, m_listDesc)
    {
        if (!pDesc || !pDesc->isEnabled || !pDesc->plugin) continue;
        QApplication::sendEvent(pDesc->plugin, &ev);
    }
}


// ============================================================================
///                 Формирование списка имен плагинов
// ============================================================================
/// \param  onlyActivated   Управление добавлением деактивированных плагинов
/// \return                 Список имен плагинов
// ============================================================================

QStringList CPluginMgr::GetPlugins (bool onlyActivated)
{
    QStringList list;

    foreach (TPlgDesc* pDesc, m_listDesc)
    {
        if ( !pDesc || (onlyActivated && !pDesc->isEnabled) ) continue;
        list.append(pDesc->name);
    }

    return list;
}


// ============================================================================
///                     Поиск плагина (активированного)
// ============================================================================
/// \param  name    Имя плагина
/// \return         Указатель на объект плагина, или NULL
// ============================================================================

CPlugin* CPluginMgr::GetPlugin (const QString& name)
{
    TPlgDesc* pDesc = getDesc(name);
    if (!pDesc) return NULL;

    return pDesc->plugin;
}


// ============================================================================
///                     Чтение информации о плагине
// ============================================================================
/// \param  name    Имя плагина
/// \param  pInfo   Переменная для сохранения информации о плагине
/// \return         0 или отрицательный код ошибки
// ============================================================================

int CPluginMgr::GetPluginInfo (const QString& name, TPlgInfo* pInfo)
{
    if ( !pInfo || name.isEmpty() ) return -1;

    TPlgDesc* pDesc = getDesc(name);
    if (!pDesc) return -2;

    pInfo->isEnabled = pDesc->isEnabled;
    pInfo->name = name;
    pInfo->title = pDesc->title;
    pInfo->version = pDesc->version;
    pInfo->desc = pDesc->desc;

    return 0;
}


// ============================================================================
//
///                     Формирование списка плагинов
//
// ============================================================================

void CPluginMgr::buildList (void)
{
    // Список плагинов формируется только 1 раз
    if ( m_listDesc.count() ) return;

    QDir dir( QApplication::applicationDirPath() );
    if ( !dir.cd("plugins") ) return;

    foreach ( QString file, dir.entryList(QDir::Files) )
    {
        file.prepend("plugins/");
        QPluginLoader* pLoader = new QPluginLoader(file, this);

        do
        {
            QObject* plugin = pLoader->instance();
            if (!plugin)
            {
                qDebug() << "[plugins]:" << file << "plugin ERROR";
                break;
            }

            IPlugin* plgWrap = qobject_cast<IPlugin*>(plugin);
            if (!plgWrap)
            {
                qDebug() << "[plugins]:" << file << "cast ERROR";
                break;
            }

            CPlugin* plgObj = plgWrap->GetPlugin();
            if (!plgObj)
            {
                qDebug() << "[plugins]:" << file << "get base object ERROR";
                break;
            }

            QString plgName = plgObj->GetName();
            plgName = plgName.toLower();
            if ( GetPlugin(plgName) ) break;

            TPlgDesc* pDesc = new TPlgDesc;
            pDesc->pLoader = pLoader;
            pDesc->isEnabled = true;
            pDesc->plugin = plgObj;
            pDesc->name = plgName;
            pDesc->title = plgObj->GetTitle();
            pDesc->version = plgObj->GetVersion();
            pDesc->desc = plgObj->GetDesc();

            plgObj->setParent(this);
            m_listDesc.append(pDesc);

            qDebug() << "[plugins]: load" << file;

            pLoader = NULL;
        } while (0);

        // Удаление загрузчика при ошибках
        if (pLoader)
        {
            pLoader->unload();
            delete pLoader;
        }
    } // foreach ( QString file, dir.entryList(QDir::Files) )
}


// ============================================================================
///                     Поиск дескриптора плагина
// ============================================================================
/// \param  name    Имя плагина
/// \return         Указатель на дескриптор или NULL
// ============================================================================

TPlgDesc* CPluginMgr::getDesc (const QString& name)
{
    foreach (TPlgDesc* pDesc, m_listDesc)
    {
        if (!pDesc || pDesc->name != name) continue;
        return pDesc;
    }

    return NULL;
}


// ============================================================================
///                     Инициализация плагина
// ============================================================================
/// \param  group   Группа настроек
/// \param  params  Актуальные параметры
/// \return         0 или отрицательный код ошибки
// ============================================================================

int CPluginMgr::initPlugin (QString& name, QHash<QString, QVariant>* params)
{
    TPlgDesc* pDesc = getDesc(name);
    if (!pDesc || !pDesc->pLoader) return -1;

    QHash<QString, QVariant> p;

    if (!params)
    {
        CSettings* pSettings = CFactory::GetSettings();
        QString group = pDesc->name;
        group.prepend("plg_");

        int res = pSettings->Get(&group, &p);
        if (res < 0) return -2;

        params = &p;
    }

    if (!pDesc->plugin)
    {
        QObject* instance = pDesc->pLoader->instance();
        if (!instance) return -3;

        IPlugin* plgWrap = qobject_cast<IPlugin*>(instance);
        if (!plgWrap) return -4;

        pDesc->plugin = plgWrap->GetPlugin();
        if (!pDesc->plugin) return -5;

        pDesc->plugin->setParent(this);
    }

    pDesc->isEnabled = true;

    return pDesc->plugin->Configure(params);
}


// ============================================================================
///                     Обработчик изменения настроек
// ============================================================================
/// \param  group   Группа настроек
/// \param  params  Актуальные параметры
// ============================================================================

void CPluginMgr::onSettingsChanged (const QString* group, QHash<QString, QVariant>* params)
{
    if (*group == "plugins")
    {
        QList<QString> names = params->keys();
        foreach (QString name, names)
        {
            bool isEnabled = params->value(name).toBool();
            TPlgDesc* pDesc = getDesc(name);

            if (!pDesc || isEnabled == pDesc->isEnabled) continue;

            if (isEnabled)
            {
                // Активация отключенного ранее плагина
                qDebug() << "[plugins]: enable" << name;
                initPlugin(name);
            }
            else
            {
                // Деактивация плагина
                pDesc->isEnabled = false;
                pDesc->plugin = NULL;
                bool result = pDesc->pLoader->unload();

                if (result)
                {
                    qDebug() << "[plugins]: disable" << name;
                }
                else
                {
                    qDebug() << "[plugins]: unload ERROR" << name;
                }
            }
        }
    }
    else if ( group->startsWith("plg_") )
    {
        QString plgName = group->mid(sizeof("plg_") - 1);
        TPlgDesc* pDesc = getDesc(plgName);

        // Переконфигурирование активного плагина
        if (pDesc && pDesc->isEnabled)
        {
            initPlugin(plgName, params);
        }
    }
}


// ============================================================================
///                 Обработчик событий (запросы от плагинов)
// ============================================================================
/// \param  event   Объект события (CPlgEvent*)
// ============================================================================

void CPluginMgr::customEvent (QEvent* event)
{
    CPlgEvent* plgEvent = static_cast<CPlgEvent*>(event);
    TPlgReq code = (TPlgReq) plgEvent->type();

    switch (code)
    {
        case PLG_REQ_SESSION:       onReqSession(plgEvent); break;
        case PLG_REQ_LIST_SESSIONS: onReqListSessions(plgEvent); break;
        case PLG_REQ_LIST_PANELS:   onReqListPanels(plgEvent); break;
        default:                    QObject::customEvent(event); break;
    } // switch (code)
}


// ============================================================================
///                 Обработчик запроса PLG_REQ_SESSION
// ============================================================================
/// \param  pEv     Объект события (CPlgEvent*)
// ============================================================================

void CPluginMgr::onReqSession (CPlgEvent* pEv)
{
    TPlgReqParam* pParam = (TPlgReqParam*) pEv->GetParam();
    if (!pParam) return;

    do
    {
        TPlgReqArgSess* pArg = (TPlgReqArgSess*) pParam->arg;
        if (!pArg) break;

        QList<CIoSess*> sessList;
        int res = CFactory::SessionList(&sessList);
        if (res < 0) break;

        uint sessId = pArg->sessId;
        pArg->pSession = NULL;

        foreach (CIoSess* pSess, sessList)
        {
            if (pSess->GetId() != sessId) continue;
            pArg->pSession = pSess;
            break;
        }

        pParam->result = 0;
        return;
    } while (0);

    pParam->result = -1;
}


// ============================================================================
///                 Обработчик запроса PLG_REQ_LIST_SESSIONS
// ============================================================================
/// \param  pEv     Объект события (CPlgEvent*)
// ============================================================================

void CPluginMgr::onReqListSessions (CPlgEvent* pEv)
{
    TPlgReqParam* pParam = (TPlgReqParam*) pEv->GetParam();
    if (!pParam) return;

    do
    {
        if (!pParam->arg) break;

        pParam->result = CFactory::SessionList( (QList<CIoSess*>*)pParam->arg );
        return;
    } while (0);

    pParam->result = -1;

}


// ============================================================================
///                 Обработчик запроса PLG_REQ_LIST_PANELS
// ============================================================================
/// \param  pEv     Объект события (CPlgEvent*)
// ============================================================================

void CPluginMgr::onReqListPanels (CPlgEvent* pEv)
{
    TPlgReqParam* pParam = (TPlgReqParam*) pEv->GetParam();
    if (!pParam) return;

    do
    {
        if (!pParam->arg) break;

        CGui* pGui = CFactory::GetGui();
        if (!pGui) break;

        pParam->result = pGui->GetListPanels( (QList<QWidget*>*)pParam->arg );
        return;
    } while (0);

    pParam->result = -1;
}
