//*****************************************************************************
//
// Имя файла    : 'factory.cpp'
// Описание     : Фабрика объектов
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************

#include "factory.h"
#include <QApplication>
#include <QPluginLoader>
#include <QDir>
#include <QVector>
#include "io/drvplugin.h"
#include "io/dummy.h"
#include "defines.h"

//#warning "DEBUG"
#include <QDebug>
//#include "io/drvio_debug.h"

// ============================================================================
//                          Локальные функции
// ============================================================================

    // Формирование списка драйверов
    static void  buildDrvList (void);


// ============================================================================
//                          Локальные переменные
// ============================================================================

    // Список моделей сессий
    static QVector<CIoSess*>    g_sessions;
    // Список объектов драйверов
    static QVector<CDriver*>    g_drvList;


// ============================================================================
///                 Формирование объекта ядра приложения
// ============================================================================
/// \return     Указатель на объект ядра
// ============================================================================

CCore* CFactory::GetCore (void)
{
    static CCore* pCore = NULL;

    if (!pCore)
    {
        pCore = new CCore();
    }

    return pCore;
}


// ============================================================================
///                     Формирование объекта GUI
// ============================================================================
/// \return     Указатель на объект GUI
// ============================================================================

CGui* CFactory::GetGui (void)
{
    static CGui* pGui = NULL;

    if (!pGui)
    {
        pGui = new CGui();
    }

    return pGui;
}


// ============================================================================
///                 Формирование объекта настроек приложения
// ============================================================================
/// \return     Указатель на объект настроек приложения
// ============================================================================

CSettings* CFactory::GetSettings (void)
{
    static CSettings* pSettings = NULL;

    if (!pSettings)
    {
        pSettings = new CSettings( GetCore() );

        // Формирование настроек по умолчанию
        QHash<QString, QVariant> params;
        QString groupName;

        // Base
        groupName = SETTINGS_GROUP_BASE_NAME;
        pSettings->Get(&groupName, &params);
        if ( !params.contains("font") ) params.insert("font", CFG_GEN_DEFAULT_FONT_FAMILY);
        if ( !params.contains("fontSize") ) params.insert("fontSize", CFG_GEN_DEFAULT_FONT_SIZE);
        if ( !params.contains("colorBg") ) params.insert("colorBg", QColor(CFG_GEN_DEFAULT_COLOR_BG));
        if ( !params.contains("colorIn") ) params.insert("colorIn", QColor(CFG_GEN_DEFAULT_COLOR_TEXT_IN));
        if ( !params.contains("colorOut") ) params.insert("colorOut", QColor(CFG_GEN_DEFAULT_COLOR_TEXT_OUT));
        if ( !params.contains("stayOnTop") ) params.insert("stayOnTop", false);
        if ( !params.contains(SETTINGS_SHORTCUT_PARAM_PREFIX "newtab") ) params.insert(SETTINGS_SHORTCUT_PARAM_PREFIX "newtab", CFG_GEN_DEFAULT_KEY_TABNEW);
        if ( !params.contains(SETTINGS_SHORTCUT_PARAM_PREFIX "closetab") ) params.insert(SETTINGS_SHORTCUT_PARAM_PREFIX "closetab", CFG_GEN_DEFAULT_KEY_TABCLOSE);
        if ( !params.contains(SETTINGS_SHORTCUT_PARAM_PREFIX "iosettings") ) params.insert(SETTINGS_SHORTCUT_PARAM_PREFIX "iosettings", CFG_GEN_DEFAULT_KEY_IO_SETTINGS);
        if ( !params.contains(SETTINGS_SHORTCUT_PARAM_PREFIX "ioopenclose") ) params.insert(SETTINGS_SHORTCUT_PARAM_PREFIX "ioopenclose", CFG_GEN_DEFAULT_KEY_IO_OPENCLOSE);
        pSettings->Save(&groupName, &params);

        // Загрузка параметров плагинов
        CPluginMgr* pMgr = GetPluginMgr();
        QStringList plgList = pMgr->GetPlugins();

        foreach (QString plgName, plgList)
        {
            CPlugin* plg = pMgr->GetPlugin(plgName);
            if (!plg) continue;

            // Формирование настроек по умолчанию
            QWidget dummyWidget;
            params.clear();
            plg->DrawSettings(&dummyWidget, &params);
            plg->ParseSettings(&dummyWidget, &params);

            // Перезапись параметров из существующей конфигурации
            groupName = "plg_";
            groupName.append(plgName);
            pSettings->Get(&groupName, &params);
            pSettings->Save(&groupName, &params);
        }
    }

    return pSettings;
}


// ============================================================================
///                 Формирование списка драйверов
// ============================================================================
/// \return     0 или отрицательный код ошибки
// ============================================================================

int CFactory::GetDrivers (QStringList* pList)
{
    if (!pList) return -1;
    if ( !g_drvList.count() ) buildDrvList();

    pList->clear();

    QVector<CDriver*>::iterator itr;
    for (itr = g_drvList.begin(); itr != g_drvList.end(); ++itr)
    {
        CDriver* ptr = *itr;
        pList->append( ptr->GetName() );
    }

    return 0;
}


// ============================================================================
///                 Получение объекта (базового) по имени
// ============================================================================
/// \return     Указатель на объект или NULL
// ============================================================================

CDriver* CFactory::GetDriver (const QString* drvName)
{
    if ( !drvName || drvName->isEmpty() ) return NULL;

    if ( !g_drvList.count() ) buildDrvList();

    QVector<CDriver*>::iterator itr;
    for (itr = g_drvList.begin(); itr != g_drvList.end(); ++itr)
    {
        CDriver* drv = *itr;
        if (drv->GetName() == *drvName) return drv;
    }

    return NULL;
}


// ============================================================================
///                 Формирование объекта настроек приложения
// ============================================================================
/// \return     Указатель на объект настроек приложения
// ============================================================================

CDriver* CFactory::CreateDriver (const QString* name)
{
    if ( !g_drvList.count() ) buildDrvList();

    CDriver* drv = GetDriver(name);

    return (drv) ? drv->NewInstance() : new CDrvDummy( GetCore() );
}


// ============================================================================
///                     Создание новой сессии (+ драйвер IO)
// ============================================================================
/// \return     Указатель на объект сессии или NULL при ошибке
// ============================================================================

CIoSess* CFactory::SessionCreate (void)
{
    static uint sessId = 0;

    CIoSess* session = new CIoSess(sessId++);
    g_sessions.append(session);

    CSettings* pSettings = GetSettings();
    QString group = CSettings::GetSessGroup( session->GetId() );
    QHash<QString, QVariant> sessParams;

    int result = pSettings->Get(&group, &sessParams);
    if (!result)
    {
        QString drvName = sessParams.value("driver").toString();

        CDriver* pDrv = CreateDriver(&drvName);
        if (pDrv)
        {
            QHash<QString, QVariant> ioParams = sessParams.value(drvName).toHash();
            pDrv->Configure(&ioParams);
            session->SetDriver(pDrv);
        }
    }

    CPluginMgr* pMgr = GetPluginMgr();
    pMgr->Dispatch(PLG_EV_SESS_CREATE, session);

    return session;
}


// ============================================================================
///                     Удаление сессии
// ============================================================================
/// \param  session Удаляемая сессия
/// \return         0 или отрицательный код ошибки
// ============================================================================

int CFactory::SessionDelete (CIoSess* session)
{
    CPluginMgr* pMgr = GetPluginMgr();
    QVector<CIoSess*>::iterator itr;

    for (itr = g_sessions.begin(); itr != g_sessions.end(); ++itr)
    {
        if (*itr != session) continue;

        pMgr->Dispatch(PLG_EV_SESS_DELETE, session);

        CDriver* drv = session->GetDriver();
        if (drv)
        {
            drv->Close();
            delete drv;
        }

        g_sessions.erase(itr);
        delete session;

        return 0;
    }

    return -1;
}


// ============================================================================
///                 Запрос объекта менеджера плагинов
// ============================================================================
/// \param  pList   Указатель на переменную для формирования списка
/// \return         0 или отрицательный код ошибки
// ============================================================================

int CFactory::SessionList (QList<CIoSess*>* pList)
{
    if (!pList) return -1;

    pList->clear();

    foreach (CIoSess* pSess, g_sessions)
    {
        if (!pSess) continue;
        pList->append(pSess);
    }

    return 0;
}


// ============================================================================
///                 Запрос объекта менеджера плагинов
// ============================================================================
/// \param  sessId      ID сессии
/// \param  pParams     Параметры сессии
/// \return             0 или отрицательный код ошибки
// ============================================================================

CPluginMgr* CFactory::GetPluginMgr (void)
{
    static CPluginMgr* pPlgMgr = NULL;

    if (!pPlgMgr)
    {
        pPlgMgr = new CPluginMgr( GetCore() );
    }

    return pPlgMgr;
}



// ============================================================================
///
///                     Формирование списка драйверов IO
///
// ============================================================================

void buildDrvList (void)
{
    CCore* core = CFactory::GetCore();

    if ( g_drvList.count() ) return;


    QDir dir( QApplication::applicationDirPath() );
    if ( !dir.cd("drivers") ) return;

    QPluginLoader loader(core);

    foreach ( QString file, dir.entryList(QDir::Files) )
    {
        file.prepend("drivers/");
        loader.setFileName(file);

        QObject* plugin = loader.instance();
        if (!plugin)
        {
            qDebug() << "[drivers]:" << file << "plugin ERROR";
            continue;
        }

        IPlgDriver* drvWrap = qobject_cast<IPlgDriver*>(plugin);
        if (!drvWrap)
        {
            qDebug() << "[drivers]:" << file << "cast ERROR";
            continue;
        }

        CDriver* plgDrv = drvWrap->GetDriver();
        if (!plgDrv)
        {
            qDebug() << "[drivers]:" << file << "driver ERROR";
            continue;
        }

        qDebug() << "[drivers]: load" << file;

        // Привязка базового объекта плагина к core,
        // т.к. loader при выходе будет удален
        plugin->setParent(core);

        g_drvList.append(plgDrv);
    } // foreach ( QString file, dir.entryList(QDir::Files) )
}
