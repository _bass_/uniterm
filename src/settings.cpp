//*****************************************************************************
//
// Имя файла    : 'settings.cpp'
// Описание     : Управление настройками приложения
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************

#include "settings.h"
#include <QApplication>


// ============================================================================
///
///                             Конструктор
///
// ============================================================================

CSettings::CSettings (QObject* parent) :
    QObject(parent)
{
    m_pSettings = new QSettings(
                        QApplication::applicationDirPath() + "/config.ini",
                        QSettings::IniFormat,
                        this
                    );
}


// ============================================================================
///
///                             Деструктор
///
// ============================================================================

CSettings::~CSettings ()
{
    m_pSettings->sync();
    delete m_pSettings;
}


// ============================================================================
///                         Чтение группы настроек
// ============================================================================
/// \param  group       Идентификатор группы настроек
/// \param  pStorage    Хранилище для копирования параметров
/// \return             0 или отрицательный код ошибки
// ============================================================================

int CSettings::Get (const QString* group, QHash<QString, QVariant>* pStorage)
{
    if (!group || !pStorage) return -1;
    if (!m_pSettings) return -2;

    if (*group == SETTINGS_GROUP_KEYBIND_NAME)
    {
        // Сбор всех сочетаний из всех групп (виртуальная группа)
        QStringList groups = m_pSettings->childGroups();

        foreach (QString name, groups)
        {
            m_pSettings->beginGroup(name);
            {
                QStringList keys = m_pSettings->childKeys();

                foreach (QString key, keys)
                {
                    if ( !key.startsWith(SETTINGS_SHORTCUT_PARAM_PREFIX) ) continue;
                    // Формирование имени параметра с учетом группы
                    QString tmpKey = key;
                    tmpKey.prepend("/");
                    tmpKey.prepend(name);
                    pStorage->insert( tmpKey, m_pSettings->value(key) );
                }
            }
            m_pSettings->endGroup();
        }
    }
    else
    {
        m_pSettings->beginGroup(*group);
        {
            QStringList keys = m_pSettings->childKeys();

            foreach (QString key, keys)
            {
                pStorage->insert( key, m_pSettings->value(key) );
            }
        }
        m_pSettings->endGroup();
    }

    return 0;
}


// ============================================================================
///                     Сохранение группы настроек
// ============================================================================
/// \param  group       Идентификатор группы настроек
/// \param  pStorage    Сохраняемые параметры
/// \return             0 или отрицательный код ошибки
// ============================================================================

int CSettings::Save (const QString* group, QHash<QString, QVariant>* pStorage)
{
    if (!group || !pStorage) return -1;
    if (!m_pSettings) return -2;

    if (*group == SETTINGS_GROUP_KEYBIND_NAME)
    {
        // Сохранение сочетаний в соответствующих группах
        QStringList keys = pStorage->keys();
        QStringList changedGroups;

        foreach (QString key, keys)
        {
            int index = key.indexOf("/");
            QString groupName = key.mid(0, index);
            if ( groupName.isEmpty() ) continue;

            if ( !changedGroups.contains(groupName) ) changedGroups.append(groupName);

            m_pSettings->setValue( key, pStorage->value(key) );
        }

        // Информирование об изменениях в группах
        foreach(QString name, changedGroups)
        {
            QHash<QString, QVariant> params;
            int result = Get(&name, &params);
            if (result < 0) continue;
            emit SigSettingsChanged(&name, &params);
        }
    }
    else
    {
        m_pSettings->beginGroup(*group);
        {
            const QList<QString> keys = pStorage->keys();

            foreach (QString key, keys)
            {
                m_pSettings->setValue( key, pStorage->value(key) );
            }
        }
        m_pSettings->endGroup();
    }
    m_pSettings->sync();

    emit SigSettingsChanged(group, pStorage);

    return 0;
}
