//*****************************************************************************
//
// Имя файла    : 'main.cpp'
// Описание     : Основной файл проекта
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************

#include "factory.h"


int main (int argc, char** argv)
{
    CCore* core = CFactory::GetCore();

    core->Init(argc, argv);
    int ecode = core->Run();

    delete core;

    return ecode;
}
