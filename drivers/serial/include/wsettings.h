//*****************************************************************************
//
// Имя файла    : 'wsettings.h'
// Описание     : Окно настроек параметров драйвера
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************
#pragma once

#include "serial.h"
#include <QWidget>


// ============================================================================
//                              WSettings
// ============================================================================

namespace Ui {
class WSettings;
}

class WSettings : public QWidget
{
    Q_OBJECT

    public:
        explicit WSettings (QWidget* parent, const TCfgSerial* pCfg);
        ~WSettings ();

        // Сохранение параметров в структуру конфигурации
        void    ExtractSettings (TCfgSerial* pCfg);

    private:
        Ui::WSettings* ui;
};
