//*****************************************************************************
//
// Имя файла    : 'serial.h'
// Описание     : Драйвер ввода-вывода
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************
#pragma once

#include <QObject>
#include <QTranslator>
#include <QtSerialPort/QtSerialPort>
#include "io/driver.h"


// ============================================================================
//                                  Типы
// ============================================================================

    // Конфигурационные параметры
    typedef struct
    {
        QString                     device;
        uint32                      baudrate;
        QSerialPort::DataBits       dataBits;
        QSerialPort::FlowControl    flowControl;
        QSerialPort::StopBits       stopBits;
        QSerialPort::Parity         parity;
    } TCfgSerial;


// ============================================================================
//                                  CDrvSerial
// ============================================================================

class CDrvSerial : public CDriver
{
    Q_OBJECT

    public:
        CDrvSerial (QObject* parent);
        ~CDrvSerial ();

        CDriver*  NewInstance (void);
        QString GetName (void);
        QString GetVersion (void);
        QString GetTitle (void);
        QString GetDesc (void);

        void    DrawSettings (QWidget* widget, QHash<QString, QVariant>* pSettings);
        void    SaveSettings (QWidget* widget, QHash<QString, QVariant>* pSettings);

        int     Configure (QHash<QString, QVariant>* params);
        int     Open (void);
        int     Close (void);
        bool    IsOpen (void);

    public slots:
        // Запись данных в устройство
        void    Send (const char* data, uint size);

    private slots:
        void    onSendDone (qint64 size);
        void    onRedyRead (void);

    private:
        TCfgSerial      m_cfg;
        QSerialPort*    m_pDev;
        const char*     m_pDataWr;
        QTranslator     m_translator;

        // Конвертация настроек в формат внутренней конфигурации
        void    settingsToCfg (QHash<QString, QVariant>* pSettings, TCfgSerial* pCfg);
        // Конвертация внутренней конфигурации в системные настройки
        void    cfgToSettings (TCfgSerial* pCfg, QHash<QString, QVariant>* pSettings);
};
