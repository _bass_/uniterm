//*****************************************************************************
//
// Имя файла    : 'wsettings.cpp'
// Описание     : Окно настроек параметров драйвера
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************

#include "wsettings.h"
#include "ui_wsettings.h"
#include <QStringList>
#include <QtSerialPort/QtSerialPort>


// ============================================================================
///                             Конструктор
// ============================================================================
/// \param  parent  Родительский виджет (для отрисовки)
/// \param  pCfg    Активаня конфигурация
// ============================================================================

WSettings::WSettings (QWidget* parent, const TCfgSerial* pCfg) :
    QWidget(parent),
    ui(new Ui::WSettings)
{
    ui->setupUi(parent);

    // Список портов
    QList<QSerialPortInfo> portList = QSerialPortInfo::availablePorts();
    QStringList nameList;

    for (QList<QSerialPortInfo>::iterator itr = portList.begin(); itr != portList.end(); ++itr)
    {
        nameList.append( (*itr).portName() );
    }

    nameList.sort();
    ui->device->addItems(nameList);
    {
        int index = ui->device->findText(pCfg->device);
        ui->device->setCurrentIndex(index);
    }

    // Скорость обмена
    QList<qint32> baudList = QSerialPortInfo::standardBaudRates();
    for (QList<qint32>::iterator itr = baudList.begin(); itr != baudList.end(); ++itr)
    {
        qint32 val = *itr;
        QString str;
        ui->baudrate->addItem(str.number(val), val);

        if ( (uint32)val == pCfg->baudrate )
        {
            int index = baudList.indexOf(val);
            ui->baudrate->setCurrentIndex(index);
        }
    }

    // Data bits
    ui->dataBits->addItem("8", QSerialPort::Data8);
    ui->dataBits->addItem("7", QSerialPort::Data7);
    ui->dataBits->addItem("6", QSerialPort::Data6);
    {
        int index = ui->dataBits->findData(pCfg->dataBits);
        if (index != -1) ui->dataBits->setCurrentIndex(index);
    }

    // Flow control
    ui->flowControl->addItem(tr("No"), QSerialPort::NoFlowControl);
    ui->flowControl->addItem(tr("Hardware"), QSerialPort::HardwareControl);
    ui->flowControl->addItem(tr("Software"), QSerialPort::SoftwareControl);
    {
        int index = ui->flowControl->findData(pCfg->flowControl);
        if (index != -1) ui->flowControl->setCurrentIndex(index);
    }

    // Stop bits
    ui->stopBits->addItem("1", QSerialPort::OneStop);
    ui->stopBits->addItem("2", QSerialPort::TwoStop);
    {
        int index = ui->stopBits->findData(pCfg->stopBits);
        if (index != -1) ui->stopBits->setCurrentIndex(index);
    }

    // Parity
    ui->parity->addItem(tr("No"), QSerialPort::NoParity);
    ui->parity->addItem(tr("Even"), QSerialPort::EvenParity);
    ui->parity->addItem(tr("Odd"), QSerialPort::OddParity);
    {
        int index = ui->parity->findData(pCfg->parity);
        if (index != -1) ui->parity->setCurrentIndex(index);
    }
}


// ============================================================================
///
///                             Деструктор
///
// ============================================================================

WSettings::~WSettings()
{
    delete ui;
}


// ============================================================================
///
///             Сохранение параметров в структуру конфигурации
///
// ============================================================================

void WSettings::ExtractSettings (TCfgSerial* pCfg)
{
    if (!pCfg) return;

    pCfg->device = ui->device->currentText();
    pCfg->baudrate = ui->baudrate->currentData().toUInt();
    pCfg->dataBits = (QSerialPort::DataBits) ui->dataBits->currentData().toInt();
    pCfg->flowControl = (QSerialPort::FlowControl) ui->flowControl->currentData().toInt();
    pCfg->stopBits = (QSerialPort::StopBits) ui->stopBits->currentData().toInt();
    pCfg->parity = (QSerialPort::Parity) ui->parity->currentData().toInt();
}
