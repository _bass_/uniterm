//*****************************************************************************
//
// Имя файла    : 'serial.cpp'
// Описание     : Драйвер ввода-вывода
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************

#include "serial.h"
#include "wsettings.h"
#include <string.h>


// ============================================================================
//                              Константы
// ============================================================================

    #define PLG_NAME                    "usart"
    #define PLG_VERSION                 "0.0.1"


// ============================================================================
//
///                              Конструктор
//
// ============================================================================

CDrvSerial::CDrvSerial (QObject* parent) :
    CDriver(parent),
    m_cfg(),
    m_pDev(NULL),
    m_translator(this)
{
    // Параметры по умолчанию
    m_cfg.device = QString("");
    m_cfg.baudrate = 115200;
    m_cfg.dataBits = QSerialPort::Data8;
    m_cfg.flowControl = QSerialPort::NoFlowControl;
    m_cfg.stopBits = QSerialPort::OneStop;
    m_cfg.parity = QSerialPort::NoParity;

    // Загрузка перевода
    QString fname = ":/drv_usart/lang/" + QLocale::system().name().mid(0, 2);
    m_translator.load(fname);
    qApp->installTranslator(&m_translator);
}


// ============================================================================
//
///                              Деструктор
//
// ============================================================================

CDrvSerial::~CDrvSerial()
{
    if ( IsOpen() )
    {
        Close();
    }

    // Удаление переводчика
    qApp->removeTranslator(&m_translator);
}


// ============================================================================
///                         Создание нового объекта
// ============================================================================
/// \return     Указатель на созданный объект
// ============================================================================

CDriver* CDrvSerial::NewInstance (void)
{
    return new CDrvSerial( parent() );
}


// ============================================================================
///                     Запрос имени плагина (файла)
// ============================================================================
/// \return     Строка с именем плагина
// ============================================================================

QString CDrvSerial::GetName (void)
{
    return QString(PLG_NAME);
}


// ============================================================================
///                     Запрос версии плагина
// ============================================================================
/// \return     Строка с версией
// ============================================================================

QString CDrvSerial::GetVersion (void)
{
    return QString(PLG_VERSION);
}


// ============================================================================
///                     Запрос названия драйвера
// ============================================================================
/// \return     Строка с названием
// ============================================================================

QString CDrvSerial::GetTitle (void)
{
    return ( m_cfg.device.isEmpty() ) ? tr("Usart") : m_cfg.device;
}


// ============================================================================
///                     Запрос описания драйвера
// ============================================================================
/// \return     Строка с описанием
// ============================================================================

QString CDrvSerial::GetDesc ()
{
    return tr("Usart description");
}


// ============================================================================
///                     Отрисовка параметров драйвера
// ============================================================================
/// \param  widget      Виджет для отрисовки настроек
/// \param  pSettings   Актульные значения конфигурации
// ============================================================================

void CDrvSerial::DrawSettings (QWidget* widget, QHash<QString, QVariant>* pSettings)
{
    TCfgSerial cfg;
    settingsToCfg(pSettings, &cfg);

    WSettings* settings = new WSettings(widget, &cfg);
    settings->show();
}


// ============================================================================
///                     Сохранение выбранных параметров
// ============================================================================
/// \param  widget      Виджет с настройками настроек
/// \param  pSettings   Хранилище настроек
// ============================================================================

void CDrvSerial::SaveSettings (QWidget* widget, QHash<QString, QVariant>* pSettings)
{
    Q_UNUSED(widget);

    QObjectList childList = widget->children();
    QObject* pObj = childList.at(0);
    if ( !pObj->isWidgetType() ) return;

    WSettings* wSettings = qobject_cast<WSettings*>(pObj);
    TCfgSerial cfg;
    wSettings->ExtractSettings(&cfg);

    cfgToSettings(&cfg, pSettings);
}


// ============================================================================
///                     Конфигурирование драйвера
// ============================================================================
/// \param  params  Конфигурационные параметры
/// \return         0 или отрицательный код ошибки
// ============================================================================

int CDrvSerial::Configure (QHash<QString, QVariant>* params)
{
    settingsToCfg(params, &m_cfg);

    return 0;
}


// ============================================================================
///                         Открытие устройства
// ============================================================================
/// \return         0 или отрицательный код ошибки
// ============================================================================

int CDrvSerial::Open (void)
{
    if (m_pDev) return -1;

    m_pDev = new QSerialPort(m_cfg.device, this);

    bool result = m_pDev->open(QIODevice::ReadWrite);
    if (!result) return -2;

    m_pDev->setBaudRate(m_cfg.baudrate);
    m_pDev->setDataBits(m_cfg.dataBits);
    m_pDev->setStopBits(m_cfg.stopBits);
    m_pDev->setFlowControl(m_cfg.flowControl);
    m_pDev->setParity(m_cfg.parity);

    m_pDev->flush();

    connect(m_pDev, &QSerialPort::bytesWritten, this, &CDrvSerial::onSendDone);
    connect(m_pDev, &QSerialPort::readyRead, this, &CDrvSerial::onRedyRead);

    return 0;
}


// ============================================================================
///                         Закрытие устройства
// ============================================================================
/// \return         0 или отрицательный код ошибки
// ============================================================================

int CDrvSerial::Close (void)
{
    if (!m_pDev) return -1;

    if ( IsOpen() )
    {
        m_pDev->flush();
        m_pDev->close();
    }

    delete m_pDev;
    m_pDev = NULL;

    return 0;
}


// ============================================================================
///                 Определение состояния устройства
// ============================================================================
/// \return         true - устройство открыто
// ============================================================================

bool CDrvSerial::IsOpen (void)
{
    return ( m_pDev && m_pDev->isOpen() );
}


// ============================================================================
///                         Отправка данных
// ============================================================================
/// \param  data    Данные для записи
/// \param  size    Количество данных
// ============================================================================

void CDrvSerial::Send (const char* data, uint size)
{
    if (!m_pDev) return;

    m_pDataWr = data;
    m_pDev->write(data, size);
}


// ============================================================================
///                 Обработчик завершения отправки данных
// ============================================================================
/// \param  size    Количество отправленных данных
// ============================================================================

void CDrvSerial::onSendDone (qint64 size)
{
    emit Sended(m_pDataWr, size);
}


// ============================================================================
///                 Обработчик готовности чтения данных
// ============================================================================
/// \param  size    Количество отправленных данных
// ============================================================================

void CDrvSerial::onRedyRead (void)
{
    char buff[64];
    qint64 size;

    do
    {
        size = m_pDev->read( buff, sizeof(buff) );
        if (size) emit Received (buff, size);
    } while (size > 0);
}


// ============================================================================
///             Конвертация настроек в формат внутренней конфигурации
// ============================================================================
/// \param  pSettings   Системные настройки
/// \param  pCfg        Внутренняя конфигурация
// ============================================================================

void CDrvSerial::settingsToCfg (QHash<QString, QVariant>* pSettings, TCfgSerial* pCfg)
{
    pCfg->device = pSettings->value( "device", QString("") ).toString();
    pCfg->baudrate = pSettings->value("baudrate", 115200).toUInt();
    pCfg->dataBits = (QSerialPort::DataBits) pSettings->value("dataBits", QSerialPort::Data8).toInt();
    pCfg->flowControl = (QSerialPort::FlowControl) pSettings->value("flowControl", QSerialPort::NoFlowControl).toInt();
    pCfg->stopBits = (QSerialPort::StopBits) pSettings->value("stopBits", QSerialPort::OneStop).toInt();
    pCfg->parity = (QSerialPort::Parity) pSettings->value("parity", QSerialPort::NoParity).toInt();
}


// ============================================================================
///         Конвертация внутренней конфигурации в системные настройки
// ============================================================================
/// \param  pCfg        Внутренняя конфигурация
/// \param  pSettings   Системные настройки
// ============================================================================

void CDrvSerial::cfgToSettings (TCfgSerial* pCfg, QHash<QString, QVariant>* pSettings)
{
    pSettings->clear();
    pSettings->insert("device", pCfg->device);
    pSettings->insert("baudrate", pCfg->baudrate);
    pSettings->insert("dataBits", pCfg->dataBits);
    pSettings->insert("flowControl", pCfg->flowControl);
    pSettings->insert("stopBits", pCfg->stopBits);
    pSettings->insert("parity", pCfg->parity);
}
