<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>CDrvSerial</name>
    <message>
        <location filename="../../src/serial.cpp" line="111"/>
        <source>Usart</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/serial.cpp" line="123"/>
        <source>Usart description</source>
        <translation>Драйвер USART</translation>
    </message>
</context>
<context>
    <name>WSettings</name>
    <message>
        <location filename="../ui/wsettings.ui" line="44"/>
        <source>Device</source>
        <translation>Порт</translation>
    </message>
    <message>
        <location filename="../ui/wsettings.ui" line="61"/>
        <source>Baudrate</source>
        <translation>Скорость обмена</translation>
    </message>
    <message>
        <location filename="../ui/wsettings.ui" line="78"/>
        <source>Data bits</source>
        <translation>Бит данных</translation>
    </message>
    <message>
        <location filename="../ui/wsettings.ui" line="95"/>
        <source>Flow control</source>
        <translation>Управление обменом</translation>
    </message>
    <message>
        <location filename="../ui/wsettings.ui" line="112"/>
        <source>Stop bits</source>
        <translation>Стоп-бит</translation>
    </message>
    <message>
        <location filename="../ui/wsettings.ui" line="129"/>
        <source>Parity</source>
        <translation>Контроль четности</translation>
    </message>
    <message>
        <location filename="../../src/wsettings.cpp" line="69"/>
        <location filename="../../src/wsettings.cpp" line="86"/>
        <source>No</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="../../src/wsettings.cpp" line="70"/>
        <source>Hardware</source>
        <translation>Аппаратное</translation>
    </message>
    <message>
        <location filename="../../src/wsettings.cpp" line="71"/>
        <source>Software</source>
        <translation>Программное</translation>
    </message>
    <message>
        <location filename="../../src/wsettings.cpp" line="87"/>
        <source>Even</source>
        <translation>Четность</translation>
    </message>
    <message>
        <location filename="../../src/wsettings.cpp" line="88"/>
        <source>Odd</source>
        <translation>Нечетность</translation>
    </message>
</context>
</TS>
