
TARGET = loop
TEMPLATE = lib

CONFIG	+= console
CONFIG	+= plugin
CONFIG	+= no_plugin_name_prefix
CONFIG  -= debug_and_release
CONFIG  -= debug_and_release_target

QT		+= core gui widgets

QMAKE_CXXFLAGS	+= -pipe
QMAKE_CFLAGS	+= -pipe


CONFIG(debug, debug|release) {
    BUILD_DIR = build/debug
} else {
    BUILD_DIR = build/release
}

DESTDIR = $$BUILD_DIR/bin
RCC_DIR	= $$BUILD_DIR/rcc
UI_DIR	= $$BUILD_DIR/ui
MOC_DIR	= $$BUILD_DIR/moc
OBJECTS_DIR	= $$BUILD_DIR/obj

target.path = ../../$$DESTDIR/drivers
target.files += $$DESTDIR/$${TARGET}.$${QMAKE_EXTENSION_SHLIB}
target.CONFIG = no_check_exist
INSTALLS += target


INCLUDEPATH += include
INCLUDEPATH += ../../include


SOURCES += \
    src/*.cpp


HEADERS  += \
    ../../include/io/driver.h \
    include/*.h

#FORMS    += res/ui/*.ui
