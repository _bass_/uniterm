//*****************************************************************************
//
// Имя файла    : 'loop.h'
// Описание     : Драйвер ввода-вывода
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************
#pragma once

#include <QObject>
#include <QtSerialPort/QtSerialPort>
#include "io/driver.h"


// ============================================================================
//                                  CDrvDummy
// ============================================================================

class CDrvLoop : public CDriver
{
    Q_OBJECT

    public:
        CDrvLoop (QObject* parent);

        CDriver*  NewInstance (void);
        QString GetName (void);
        QString GetVersion (void);
        QString GetTitle (void);
        QString GetDesc (void);

        void    DrawSettings (QWidget* widget, QHash<QString, QVariant>* pSettings);
        void    SaveSettings (QWidget* widget, QHash<QString, QVariant>* pSettings);

        int     Configure (QHash<QString, QVariant>* params);
        int     Open (void);
        int     Close (void);
        bool    IsOpen (void);

    public slots:
        // Запись данных в устройство
        void    Send (const char* data, uint size);

    private:
        bool    m_isOpen;
};
