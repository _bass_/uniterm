//*****************************************************************************
//
// Имя файла    : 'plugin.h'
// Описание     : Плагин драйвера ввода-вывода
// Автор        : Межлумов В.Ш.
//
//*****************************************************************************
#pragma once

#include <QObject>
#include <QtPlugin>
#include "io/drvplugin.h"
#include "loop.h"


// ============================================================================
//                                  CPlugin
// ============================================================================

class CPlugin : public QObject, public IPlgDriver
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID PLG_DRIVER_IFACE_ID)
    Q_INTERFACES(IPlgDriver)

    public:
        CPlugin ();

        CDriver* GetDriver (void);

    private:
        CDrvLoop  m_drv;
};
